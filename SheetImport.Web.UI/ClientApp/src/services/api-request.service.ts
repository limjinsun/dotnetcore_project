import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiRequestService {
  http: HttpClient;
  apiUrl: string;

  constructor(http: HttpClient) {
    this.http = http;
    this.apiUrl = environment.apiUrl;
  }

  makeGetRequest(sheetName: string, sheetId: string) {
    return this.http.get(this.apiUrl + '/sheet/' + sheetId + '/' + sheetName);
  }

  makeAllSheetGetRequest(sheetId: string) {
    return this.http.get(this.apiUrl + '/sheet/alldata/' + sheetId);
  }

  makeAllSheetPostRequest(sheetId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    var emptyBody: any = null;
    return this.http.post(this.apiUrl + '/sheet/alldata/' + sheetId, emptyBody, httpOptions);
  }

  makePostRequest(sheetName: string, jsonData: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(this.apiUrl + '/sheet/' + sheetName, jsonData, httpOptions);
  }

  makeSheetMetaDataGetRequest(sheetId: string, sheetName: string) {
    return this.http.get(this.apiUrl + '/sheet/metadata/' + sheetId + '/' + sheetName);
  }

  makeDriveGetRequest(sheetId: string) {
    return this.http.get(this.apiUrl + '/drive/' + sheetId);
  }

  makeDbConnectionGetRequest() {
    console.log('apiurl : ' + this.apiUrl);
    return this.http.get(this.apiUrl + '/dbconnection');
  }

  makeDbSelectGetRequest(dbName: string) {
    return this.http.get(this.apiUrl + '/dbconnection/dbselect/' + dbName);
  }

  makeResetGetRequest() {
    return this.http.get(this.apiUrl + '/dbconnection/dbreset');
  }

  makeDbCodeGetRequest(dbName: string) {
    return this.http.get(this.apiUrl + '/dbconnection/dbcode/' + dbName);
  }
}
