import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalSettingService {
  googleSheetId?: any;
  sheetFileName?: any;
  currentSheetObject?: any;
  resultList?: any;
  transactionResult?: any;

  setSheetFileName(fileName) {
    this.sheetFileName = fileName;
  }

  getSheetFileName() {
    return this.sheetFileName;
  }

  setGoogleSheetId(sheetId) {
    this.googleSheetId = sheetId;
  }

  getGoogleSheetId() {
    return this.googleSheetId;
  }

  setCurrentSheetObject(currentSheetObject) {
    this.currentSheetObject = currentSheetObject;
  }

  getCurrentSheetObject() {
    return this.currentSheetObject;
  }

  setResultList(resultList) {
    this.resultList = resultList;
  }

  getResultList() {
    return this.resultList;
  }

  setTransactionResult(transactionResult) {
    this.transactionResult = transactionResult;
  }

  getTransactionResult() {
    return this.transactionResult;
  }

  constructor() { }
}
