import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';


import 'hammerjs'; // https://material.angular.io/guide/getting-started#step-5-gesture-support

const providers = [
  { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] },
  // { provide: 'API_URL', useFactory: getApiUrl, deps: [] } // added
];

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}

// export function getApiUrl(): string {
//   return AppConsts.Url;
// }

// export class AppConsts {
//   static Url = "https://localhost:44311/api";   // https://localhost:44357/api
// }

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic(providers).bootstrapModule(AppModule)
  .catch(err => console.log(err));


