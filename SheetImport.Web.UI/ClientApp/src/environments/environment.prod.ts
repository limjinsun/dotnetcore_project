export const environment = {
  production: true,
  apiUrl: 'https://fixhub-onboardingtool-api-dev.azurewebsites.net/api'
};
