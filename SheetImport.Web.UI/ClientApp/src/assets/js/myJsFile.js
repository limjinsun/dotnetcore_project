function recursive(previous, current) {
    if (Array.isArray(current)) {
      return current.reduce(recursive, previous); // return array to previous position.
    }
    previous.push(current);
    return previous;
  }
  