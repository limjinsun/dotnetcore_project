import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dtoPresentnameFilter'
})
export class DtoPresentnameFilterPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    let result = value.match(/\w.+(?=Dto)/);
    return result + " tab in google sheet";
  }
}
