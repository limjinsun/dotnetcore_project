import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'classNameFilter'
})
export class ClassNameFilterPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    let result = value.match(/\w+$/);
    return result;
  }
}
