import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/preselection/preselection.component';
import { DropdownComponent } from '../../pages/postselection/dropdown.component';
import { ResultComponent } from '../../pages/result/result.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'dropdown',       component: DropdownComponent, data: {reuseRoute: false }},
    { path: 'result',         component: ResultComponent }
];
