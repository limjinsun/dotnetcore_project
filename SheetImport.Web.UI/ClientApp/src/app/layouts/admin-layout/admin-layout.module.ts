import { NgModule }                             from '@angular/core';
import { RouterModule, RouteReuseStrategy }                         from '@angular/router';
import { CommonModule }                         from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';

import { AdminLayoutRoutes }                    from './admin-layout.routing';

import { DashboardComponent }                   from '../../pages/preselection/preselection.component';

import { DropdownComponent }                    from '../../pages/postselection/dropdown.component';
import { TableComponent }                       from '../../pages/table/table.component';
import { ResultComponent }                      from '../../pages/result/result.component';

import { NgbModule }                            from '@ng-bootstrap/ng-bootstrap';

import { FormsModule, ReactiveFormsModule }     from '@angular/forms';
import { NgSelectModule }                       from '@ng-select/ng-select';
import { MaterialModule }                       from './material-module';
import { MatProgressButtonsModule }             from 'mat-progress-buttons';

import { ApiRequestService }                    from '../../../services/api-request.service';

import { ClassNameFilterPipe }                  from '../../class-name-filter.pipe';
import { DtoPresentnameFilterPipe }             from '../../dto-presentname-filter.pipe';
import { ButtonsandwarningsComponent }          from '../../pages/buttonsandwarnings/buttonsandwarnings.component';

import { CustomReuseStrategy }                  from 'app/reuse-strategy';

import { NgxSpinnerModule } from "ngx-spinner";


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(AdminLayoutRoutes),
    NgbModule,
    FormsModule, ReactiveFormsModule,
    NgSelectModule,
    MaterialModule,
    MatProgressButtonsModule.forRoot(),
    NgxSpinnerModule
  ],
  declarations: [
    DashboardComponent,
    DropdownComponent,
    TableComponent,
    ClassNameFilterPipe,
    DtoPresentnameFilterPipe,
    ResultComponent,
    ButtonsandwarningsComponent
  ],
  providers: [
    ApiRequestService,
    {provide: RouteReuseStrategy, useClass: CustomReuseStrategy }
  ]
})
export class AdminLayoutModule {}
