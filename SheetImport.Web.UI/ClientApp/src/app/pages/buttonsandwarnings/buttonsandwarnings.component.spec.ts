import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsandwarningsComponent } from './buttonsandwarnings.component';

describe('ButtonsandwarningsComponent', () => {
  let component: ButtonsandwarningsComponent;
  let fixture: ComponentFixture<ButtonsandwarningsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonsandwarningsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsandwarningsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
