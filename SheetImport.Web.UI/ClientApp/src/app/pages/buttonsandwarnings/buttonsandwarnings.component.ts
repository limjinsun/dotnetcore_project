import { Component, OnInit, ViewChild} from '@angular/core';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { ApiRequestService } from '../../../services/api-request.service';
import { GlobalSettingService } from 'services/global-setting.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

class resultData{}

@Component({
  selector: 'app-buttonsandwarnings',
  templateUrl: './buttonsandwarnings.component.html',
  styleUrls: ['./buttonsandwarnings.component.scss']
})
export class ButtonsandwarningsComponent implements OnInit {
  @ViewChild('content', {static: false}) private content;

  dataValidationStatus: any;
  allSheetValidationList: any;
  sheetNameForValidation: any;
  dataSafeIndication: any;
  hasValidationError: boolean;

  validationButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: 'VALIDATE ALL DATA',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    buttonColor: 'primary',
    spinnerColor: 'primary',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  }

  allSheetImportButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: 'IMPORT ALL DATA',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    buttonColor: 'primary',
    spinnerColor: 'primary',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  }

  dataForResult: any = {
    resultList: {}, transactionResult: {}
  };

  constructor(
    private apiService: ApiRequestService,
    private globalSettingService: GlobalSettingService,
    private router: Router,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.allSheetImportButtonOptions.disabled = true;
  }

  validationButtonClick(): void {
    this.allSheetValidationList = null;
    this.dataSafeIndication = null;
    this.validationButtonOptions.active = true;

    this.apiService.makeAllSheetGetRequest(this.globalSettingService.getGoogleSheetId()).subscribe(result => {
      const hasSheetError = this.processAllSheetData(result);
      if (!hasSheetError) {
        this.dataSafeIndication = {
          message: 'All Data are safe to import.'
        }
        this.validationButtonOptions.active = false;
        this.dataValidationStatus = { status: 'configured' };
        this.allSheetImportButtonOptions.disabled = false;
      } else {
        this.validationButtonOptions.active = false;
        this.dataValidationStatus = { status: 'not-configured' };
        this.allSheetImportButtonOptions.disabled = true;
      }
    })
  }

  processAllSheetData(data: any): any {
    this.hasValidationError = false;
    this.allSheetValidationList = [];
    data.forEach((item) => {
      this.sheetNameForValidation = item['sheetName'];
      const dtoObj = item['dto'];
      const wrongData = dtoObj.filter((element) => element['isValid'] === false);
      if (wrongData.length > 0) {
        wrongData.forEach((current) => {
          if (current['isValid'] === false) {
            const validation = {
              sheetName: this.sheetNameForValidation,
              id: current['rowNumber'],
              details: current['validationMessage']
            };
            this.allSheetValidationList.push(validation);
            this.hasValidationError = true;
          }
        });
      }
    });
    console.log(this.allSheetValidationList);
    return this.hasValidationError;
  }

  allSheetImportButtonClick(): void {
    this.allSheetImportButtonOptions.active = true;
    this.spinner.show();

    this.apiService.makeAllSheetPostRequest(this.globalSettingService.getGoogleSheetId()).subscribe(result => {
      console.log(result);
      const resultArray = result as resultData[];
      this.dataForResult.resultList = resultArray.reduce(this.makeArrayflat, []);
      this.dataForResult.transactionResult.numOfAdded = this.dataForResult.resultList.filter(c => c.status === 1).length;
      this.dataForResult.transactionResult.numOfUpdated = this.dataForResult.resultList.filter(c => c.status === 2).length;
      this.dataForResult.transactionResult.numOfWarning = this.dataForResult.resultList.filter(c => c.status === 3).length;
      this.dataForResult.transactionResult.numOfError = this.dataForResult.resultList.filter(c => c.status === 4).length;

      this.allSheetImportButtonOptions.active = false;

      this.globalSettingService.setResultList(this.dataForResult.resultList);
      this.globalSettingService.setTransactionResult(this.dataForResult.transactionResult);

      this.spinner.hide();
      this.dataValidationStatus = null // reset status.

      // redirect to Result Seciton
      this.router.navigate(['/result']);
    });
  }

  makeArrayflat = (previous, current) => {
    if (Array.isArray(current)) {
      return current.reduce(this.makeArrayflat, previous); // return array to previous position. recursive
    }
    previous.push(current);
    return previous;
  }
}
