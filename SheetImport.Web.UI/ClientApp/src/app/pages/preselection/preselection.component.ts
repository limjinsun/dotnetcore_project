import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiRequestService } from '../../../services/api-request.service';
import { Router } from '@angular/router'
import { GlobalSettingService } from 'services/global-setting.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'preselection.component.html'
})
export class DashboardComponent implements OnInit{
  dataForDropdown: any = { sheetId: '', fileName: '' };

  selectedDb: any = null;
  dataConfigured?: Boolean = false;
  dbs = null;
  errorMsg?: any;
  dbCode: any;
  dbcodeError?: any;

  sheetUrl = new FormControl();

  constructor(
    private apiService: ApiRequestService,
    private globalSettingService: GlobalSettingService,
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
    // apiService will be injected from Constructor.
    console.log('## Preselection component constructor ##');
    this.apiService.makeDbConnectionGetRequest().subscribe(result => {
      this.dbs = result;
    });
    this.matIconRegistry.addSvgIcon(
      `database-svg`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../assets/img/database-001.svg')
    );
  }

  onSheetUrlChange() {
    console.log(this.sheetUrl.value);
    this.handleButtonClick(null);
  }

  onChange(db: any) {
    console.log(db);
    this.selectedDb = db;
  }

  handleButtonClick(event: Event) {
    this.errorMsg = null;

    if (this.selectedDb === null) {
      this.errorMsg = 'Error occured - Please Select DB connection';
      return;
    } else if (this.sheetUrl.value === null) {
      this.errorMsg = 'Error occured - Please type url of the GoogleSheet';
      return;
    }

    if (this.selectedDb != null && this.sheetUrl.value != null) {
      this.dataConfigured = false;
      console.log('SheetUrl : ', this.sheetUrl.value);
      const s = this.sheetUrl.value;
      const result = s.match(/\/spreadsheets\/d\/([a-zA-Z0-9-_]+)/g); // https://developers.google.com/sheets/api/guides/concepts
      if (result == null) {
        this.errorMsg = 'Error occured - GoogleSheet URL format is not recognized.';
        return;
      }
      const condensedId = result.toString().substring(16);
      this.globalSettingService.setGoogleSheetId(condensedId);
      this.globalSettingService.setCurrentSheetObject(null);
      this.globalSettingService.setSheetFileName(null);

      this.apiService.makeDbCodeGetRequest(this.selectedDb).subscribe(result => {
        this.dbCode = result['dbCode'];
      });

      this.dataForDropdown.sheetId = condensedId;
      this.apiService.makeDriveGetRequest(condensedId).subscribe(result => {
        console.log(result);
        this.dataForDropdown.fileName = result['fileName'];
        console.log(this.dataForDropdown.fileName);
        // if file is null - out.
        // if code is not match - out.
        // then - ok

        if (this.dataForDropdown.fileName == null) {
          this.errorMsg = 'Error occured - ' + result['error'];
          return;
        } else if (!this.dataForDropdown.fileName.includes(this.dbCode)) {
          this.errorMsg = 'Error occured - Db and Sheet Info was not matched';
          return;
        } else {
          this.dataConfigured = true;
          this.globalSettingService.setSheetFileName(this.dataForDropdown['fileName']);
          this.router.navigate(['/dropdown']);
        }
      });

      // To give application Db name.
      this.apiService.makeDbSelectGetRequest(this.selectedDb).subscribe(result => {
        console.log(result);
      });

      console.log('## DashBoard Button handler ##');
    }
  }

  ngOnInit() {
    console.log('## DashBoard component ngOnInit ##');
  }
}
