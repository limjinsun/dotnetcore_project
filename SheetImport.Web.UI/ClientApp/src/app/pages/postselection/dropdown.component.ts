import { Component, OnInit, ViewEncapsulation, Inject, Input, ChangeDetectionStrategy } from '@angular/core';
import { ApiRequestService } from '../../../services/api-request.service';
import { GlobalSettingService } from 'services/global-setting.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MatProgressButtonOptions } from 'mat-progress-buttons';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  combinedDataForTable: any = null;
  selectedSheet: any = null;
  resultJson: any = [];
  validationArray: any = [];
  sheetnameFromApi: any = '';
  sheets = [
    { id: 1, name: 'OEM' },
    { id: 2, name: 'Asset Categories' },
    { id: 3, name: 'Models'},
    { id: 4, name: 'Location' },
    { id: 5, name: 'Site Users' },
    { id: 6, name: 'Sites' },
    { id: 7, name: 'Assets' },
    { id: 8, name: 'Service Providers' },
    { id: 9, name: 'Service Provider Users' }
  ];


  sheetFileName?: any;
  currentSheetObject?: any;
  googleSheetId?: any;

  sheetOpenButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: 'OPEN THE SHEET',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    buttonColor: 'warn',
    spinnerColor: 'warn',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  }

  onNavigate() {
    window.open('https://docs.google.com/spreadsheets/d/' + this.googleSheetId , '_blank');
  }

  constructor(
    private apiService: ApiRequestService,
    private globalSettingService: GlobalSettingService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
    // apiService will be injected from Constructor.
    console.log('## Dropdown component constructor ##');

    if (this.globalSettingService.getSheetFileName()){
      this.sheetFileName = this.globalSettingService.getSheetFileName();
      this.googleSheetId = this.globalSettingService.getGoogleSheetId();
    }

    if (this.globalSettingService.getCurrentSheetObject()) {
      this.currentSheetObject = this.globalSettingService.getCurrentSheetObject();
      this.onChange(this.currentSheetObject);
    }

    this.matIconRegistry.addSvgIcon(
      `cloud-svg`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../assets/img/cloud-001.svg')
    );
  }

  onChange(sheet: any) {
    this.selectedSheet = sheet;  // don't forget to update the model here
    this.globalSettingService.setCurrentSheetObject(sheet);

    this.apiService.makeSheetMetaDataGetRequest(this.googleSheetId, this.selectedSheet.name).subscribe(result => {
      this.selectedSheet['id'] = result[0]['properties']['sheetId'];
      console.log(this.selectedSheet);
    });

    this.apiService.makeGetRequest(this.selectedSheet.name, this.googleSheetId).subscribe(result => {
      console.log(result);
      this.resultJson = result['dto'];
      this.combinedDataForTable = {
        resultJson: this.resultJson,
        sheetNameFromApi: result['sheetName'],
        selectedSheet: this.selectedSheet,
        sheetId: this.googleSheetId }
    });
  }

  ngOnInit() {
    console.log('## Dropdown component ngOnit() ##');
  }
}
