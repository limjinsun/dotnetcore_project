import {
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  OnInit,
  DebugElement,
  ViewEncapsulation,
  ViewChild
} from "@angular/core";
import Tabulator from "tabulator-tables";
import { ApiRequestService } from "../../../services/api-request.service";
import { MatProgressButtonOptions } from "mat-progress-buttons";
import { Router } from "@angular/router";
import { GlobalSettingService } from "services/global-setting.service";
import { NgxSpinnerService } from "ngx-spinner";

class resultData { }

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnChanges, OnInit {
  @Input() combinedDataInput: any;
  @ViewChild("content", { static: false }) private content;

  table: any;
  tableData: any;
  validationData: any = [];
  sheetName: any;
  validationList?: any;
  processedData: any = [];
  dataForResult: any = {
    resultList: {},
    transactionResult: {}
  };
  sheetNameForValidation: any;
  currentSheetObj: any;
  googleSheetId: any;

  singleImportSpinnerButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: "IMPORT CURRENT DATA",
    spinnerSize: 18,
    raised: true,
    stroked: false,
    buttonColor: "accent",
    spinnerColor: "accent",
    fullWidth: false,
    disabled: false,
    mode: "indeterminate"
  };

  constructor(
    private apiService: ApiRequestService,
    private globalSettingService: GlobalSettingService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    // apiService will be injected from Constructor.
    console.log("## Table component constructor called ");
  }

  addColumn(array, column): any {
    array.unshift(column);
    return array;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("## Table - ngOnChange() called ##");

    this.validationList = null; // clear validation output when user select other sheet.
    this.singleImportSpinnerButtonOptions.disabled = false; // set import button to normal state.
    if (this.combinedDataInput.resultJson != null && this.table != null) {
      console.log(this.combinedDataInput.resultJson);
      this.tableData = this.combinedDataInput.resultJson;
      this.sheetName = this.combinedDataInput.selectedSheet.name;

      this.table.setColumns(
        Object.keys(this.tableData[0])
          .filter(e => e !== "validationMessage")
          .map(obj => {
            if (obj === "rowNumber") {
              return {
                title: "Row",
                field: "rowNumber",
                sorter: "number",
                align: "center"
              };
            } else if (obj === "isValid") {
              return {
                title: "DataCheck",
                field: "isValid",
                sorter: "string",
                align: "center",
                formatter: "tickCross",
                formatterParams: {
                  allowEmpty: true,
                  allowTruthy: true,
                  tickElement:
                    '<i class="fa fa-check" style="color : green;"></i>',
                  crossElement:
                    '<i class="fa fa-times" style="color : red;"></i>'
                }
              };
            } else {
              return {
                title: obj,
                field: obj,
                sorter: "string",
                align: "left"
              };
            }
          })
      );

      this.validationList = [];
      const wrongData = this.tableData.filter(e => e['isValid'] === false);
      if (wrongData.length > 0) {
        this.singleImportSpinnerButtonOptions.disabled = true;
        wrongData.forEach((current) => {
          console.log(current);
          const validation = {
            sheetName: this.sheetName,
            id: current['rowNumber'],
            details: current['validationMessage']
          };
          this.validationList.push(validation);
        });
      };
      this.table.setData(this.tableData);
    }
  }

  openNewWindow = (e, row) => {
    console.log(row);
    this.googleSheetId = this.globalSettingService.getGoogleSheetId();
    this.currentSheetObj = this.globalSettingService.getCurrentSheetObject();
    const win = window.open(
      'https://docs.google.com/spreadsheets/d/' + this.googleSheetId + '/edit#gid='
      + this.currentSheetObj['id'] + '&range=' + row['_row']['data']['rowNumber'] + ':' + row['_row']['data']['rowNumber'], '_blank');
    win.focus();
  }

  private drawTable(): void {
    console.log("## drawTable called ##");

    this.singleImportSpinnerButtonOptions.disabled = false; // set import button to normal state.
    this.tableData = this.combinedDataInput.resultJson;
    this.sheetName = this.combinedDataInput.selectedSheet.name;

    this.table = new Tabulator("#my-tabular-table", {
      columns: Object.keys(this.tableData[0])
        .filter(e => e !== "validationMessage")
        .map(obj => {
          if (obj === "rowNumber") {
            return {
              title: "Row",
              field: "rowNumber",
              sorter: "number",
              align: "center"
            };
          } else if (obj === "isValid") {
            return {
              title: "DataCheck",
              field: "isValid",
              sorter: "string",
              align: "center",
              formatter: "tickCross",
              formatterParams: {
                allowEmpty: true,
                allowTruthy: true,
                tickElement:
                  '<i class="fa fa-check" style="color : green;"></i>',
                crossElement: '<i class="fa fa-times" style="color : red;"></i>'
              }
            };
          } else {
            return {
              title: obj,
              field: obj,
              sorter: "string",
              align: "left"
            };
          }
        }),
      layout: "fitDataFill",
      reactiveData: true,
      pagination: "local",
      paginationSize: 10,
      rowClick : this.openNewWindow
    });

    this.validationList = [];
    const wrongData = this.tableData.filter(e => e['isValid'] === false);
    if (wrongData.length > 0) {
      this.singleImportSpinnerButtonOptions.disabled = true;
      wrongData.forEach((current) => {
        console.log(current);
        const validation = {
          sheetName: this.sheetName,
          id: current['rowNumber'],
          details: current['validationMessage']
        };
        this.validationList.push(validation);
      });
    }

    this.table.setData(this.tableData);
  }

  singleImportBtnClick(): void {
    console.log("import button click");

    console.log("sheetname : " + this.sheetName);
    console.log("tabledata : " + this.tableData);

    this.singleImportSpinnerButtonOptions.active = true;
    /** spinner starts on init */
    this.spinner.show();
    this.apiService
      .makePostRequest(this.sheetName, this.tableData)
      .subscribe(result => {
        console.log(result);
        const resultArray = result as resultData[];
        this.dataForResult.resultList = resultArray.reduce(this.makeArrayflat, []);
        this.dataForResult.transactionResult.numOfAdded = this.dataForResult.resultList.filter(
          c => c.status === 1
        ).length;
        this.dataForResult.transactionResult.numOfUpdated = this.dataForResult.resultList.filter(
          c => c.status === 2
        ).length;
        this.dataForResult.transactionResult.numOfWarning = this.dataForResult.resultList.filter(
          c => c.status === 3
        ).length;
        this.dataForResult.transactionResult.numOfError = this.dataForResult.resultList.filter(
          c => c.status === 4
        ).length;

        this.singleImportSpinnerButtonOptions.active = false;
        this.globalSettingService.setResultList(this.dataForResult.resultList);
        this.globalSettingService.setTransactionResult(this.dataForResult.transactionResult);
        this.spinner.hide();
        // redirect to Result Seciton
        this.router.navigate(["/result"]);
      });
  }

  makeArrayflat = (previous, current) => {
    if (Array.isArray(current)) {
      return current.reduce(this.makeArrayflat, previous); // return array to previous position. recursive
    }
    previous.push(current);
    return previous;
  }

  ngOnInit() {
    console.log("## Table component ngOnInit called ");
    if (this.combinedDataInput.resultJson != null) {
      this.drawTable();
    }
  }
}

/**   var win = window.open(
  'https://docs.google.com/spreadsheets/d/' + this.googleSheetId + '/edit#gid=' + this.currentSheetObj['id'], '_blank');
  win.focus();
**/
