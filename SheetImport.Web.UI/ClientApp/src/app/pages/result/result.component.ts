import { Component, OnInit } from '@angular/core';
import { GlobalSettingService } from 'services/global-setting.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  resultList?: any;
  transactionResult?: any;

  constructor(
    private globalSettingService: GlobalSettingService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
    console.log('## Result constructor ##');

    if (this.globalSettingService.getResultList()) {
      this.resultList = this.globalSettingService.getResultList();
      console.log(this.resultList);
    }

    if (this.globalSettingService.getTransactionResult()) {
      this.transactionResult = this.globalSettingService.getTransactionResult();
      console.log(this.transactionResult);
    }

    this.matIconRegistry.addSvgIcon(
      `file-svg`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../assets/img/file-001.svg')
    );
  }

  addFilterClick(event){
    const globalSingleTonResultList = this.globalSettingService.getResultList();
    this.resultList = globalSingleTonResultList.filter(e => e['status'] === 1);
  }

  updateFilterClick(){
    const globalSingleTonResultList = this.globalSettingService.getResultList();
    this.resultList = globalSingleTonResultList.filter(e => e['status'] === 2);
  }

  warningFilterClick(){
    const globalSingleTonResultList = this.globalSettingService.getResultList();
    this.resultList = globalSingleTonResultList.filter(e => e['status'] === 3);
  }

  errorFilterClick(){
    const globalSingleTonResultList = this.globalSettingService.getResultList();
    this.resultList = globalSingleTonResultList.filter(e => e['status'] === 4);
  }

  onValChange(e) {
    console.log(e);
  }

  ngOnInit() {
    console.log('## Result ngOnInit() ##');
  }
}
