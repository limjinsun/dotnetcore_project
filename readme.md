ASP .NET Core 2.2 + Mysql + Angular 8.0 Client
----

This project was built to transport data from Google sheet to MySQL database. Angular framework used for the frontend along with Angular Material and Tabulator open source libaray. .NET Core 2.2 Framework used for making JSON API Backend and Business logics.
© Jin Lim


- Google Sheet/Drive API
- Entity Framework


## Application layout

![screenshot1](./pics/s1.png "s1")

