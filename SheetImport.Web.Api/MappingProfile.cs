﻿using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Models;

namespace DataHubSheetImport.Web.Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<StoreDto, Store>()
                .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                .ForMember(destination => destination.PhoneNumber, option => option.MapFrom(source => source.PhoneNumber))
                .ForMember(destination => destination.McDonaldsStoreId, option => option.MapFrom(source => source.McDonaldsStoreId))
                .ForMember(destination => destination.Name, option => option.MapFrom(source => source.Name))
                .ForMember(destination => destination.RequiresServiceRequestApproval, option => option.MapFrom(source => source.RequiresServiceRequestApproval))
                .ForMember(destination => destination.AddressType, option => option.MapFrom(source => source.AddressType))
                .ForMember(destination => destination.Country, option => option.MapFrom(source => source.Country))
                .ForMember(destination => destination.MajorMunicipality, option => option.MapFrom(source => source.MajorMunicipality))
                .ForMember(destination => destination.MinorMunicipality, option => option.MapFrom(source => source.MinorMunicipality))
                .ForMember(destination => destination.PostalArea, option => option.MapFrom(source => source.PostalArea))
                .ForMember(destination => destination.StreetName, option => option.MapFrom(source => source.StreetName))
                .ForMember(destination => destination.StoreNumber, option => option.MapFrom(source => source.StoreNumber))
                .ForMember(destination => destination.OwnerId, option => option.MapFrom(source => source.OwnerId))
                .ForMember(destination => destination.Password, option => option.MapFrom(source => source.Password))
                .ForMember(destination => destination.RequiresServiceRequestPmApproval, option => option.MapFrom(source => source.RequiresServiceRequestPmApproval));

            CreateMap<SiteUsersDto, Users>()
                .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(destination => destination.Username, option => option.MapFrom(source => source.Username))
                .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                .ForMember(destination => destination.MobileNumber, option => option.MapFrom(source => source.Mobile));

            CreateMap<SiteUsersDto, Store>()
                .ForMember(destination => destination.Name, option => option.MapFrom(source => source.Site))
                .ForMember(destination => destination.Email, opt => opt.Ignore());
            
            CreateMap<UsersDto, Users>()
                .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                .ForMember(destination => destination.MobileNumber, option => option.MapFrom(source => source.MobileNumber))
                .ForMember(destination => destination.Username, option => option.MapFrom(source => source.Username))
                .ForMember(destination => destination.ProviderId, option => option.MapFrom(source => source.ProviderId))
                .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName));

            CreateMap<AuthoritiesDto, Authorities>()
                .ForMember(destination => destination.Authority, option => option.MapFrom(source => source.Authority))
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId));

            CreateMap<UserStoreRelationshipDto, Userstorerelationship>()
                .ForMember(destination => destination.StoreId, option => option.MapFrom(source => source.StoreId))
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId));

            CreateMap<OemDto, Oemdetails>()
                .ForMember(destination => destination.Name, option => option.MapFrom(source => source.Manufacture));

            CreateMap<PartMetadataDto, Partmetadata>()
                .BeforeMap((source, destination) => destination.Dtype = "MachineMetadata") // PartMetadata table. DTYPE should be 'NOT NULL'
                .ForMember(destination => destination.OemDetailsId,
                    option => option.MapFrom(source => source.OemDetailsId))
                .ForMember(destination => destination.PartNumber,
                    option => option.MapFrom(source => source.ModelNumber))
                .ForMember(destination => destination.Description,
                    option => option.MapFrom(source => source.Description))
                .ForMember(destination => destination.MachineCategoryId,
                    option => option.MapFrom(source => source.MachineCategoryId));

            CreateMap<MachineDto, Machine>()
                .ForMember(destination => destination.SerialNumber,
                    option => option.MapFrom(source => source.SerialNumber))
                .ForMember(destination => destination.ExpiryDate,
                    option => option.MapFrom(source => source.ExpiryDate))
                .ForMember(destination => destination.MachineMetaDataId,
                    option => option.MapFrom(source => source.MachineMetaDataId))
                .ForMember(destination => destination.StationId,
                    option => option.MapFrom(source => source.StationId))
                .ForMember(destination => destination.StoreId,
                    option => option.MapFrom(source => source.StoreId))
                .ForMember(destination => destination.FriendlyName,
                    option => option.MapFrom(source => source.FriendlyName))
                .ForMember(destination => destination.PurchasePrice,
                    option => option.MapFrom(source => source.PurchasePrice))
                .ForMember(destination => destination.InstallationDate,
                    option => option.MapFrom(source => source.InstallationDate))
                .ForMember(destination => destination.WarrantyStartDate,
                    option => option.MapFrom(source => source.WarrantyStartDate));

            // Not working any more - Machinecategoty.Name doesn't exist any more. It has been changed to NameID. 
            // In order to get NameId. you need to join the tables. so simply mapping two objects doesn't work. 

            //CreateMap<AssetCategoriesDto, Machinecategory>()
            //    .BeforeMap((source, destination) =>
            //        destination.CommissioningPeriod = 14) // Default value has been set in DB.
            //    .ForMember(destination => destination.CategoryCode,
            //        option => option.MapFrom(source => source.CategoryCode))
            //    .ForMember(destination => destination.Name,
            //        option => option.MapFrom(source => source.AssetCategory));

            //CreateMap<LocationDto, Station>()
            //    .ForMember(destination => destination.NameId,
            //        option => option.MapFrom(source => source.Location));

            CreateMap<ServiceProvidersDto, Serviceprovider>()
                .ForMember(destination => destination.Email,
                    option => option.MapFrom(source => source.Email))
                .ForMember(destination => destination.MobilePhoneNumber,
                    option => option.MapFrom(source => source.Mobile))
                .ForMember(destination => destination.PhoneNumber,
                    option => option.MapFrom(source => source.Phone))
                .ForMember(destination => destination.Name,
                    option => option.MapFrom(source => source.ServiceCompanyName))
                .ForMember(destination => destination.AddressType,
                    option => option.MapFrom(source => source.AddressLine1))
                .ForMember(destination => destination.MajorMunicipality,
                    option => option.MapFrom(source => source.County))
                .ForMember(destination => destination.MinorMunicipality,
                    option => option.MapFrom(source => source.City))
                .ForMember(destination => destination.PostalArea,
                    option => option.MapFrom(source => source.PostCode))
                .ForMember(destination => destination.StreetName,
                    option => option.MapFrom(source => source.AddressLine2))
                .ForMember(destination => destination.PersonalIndemnityInsuranceExpireDate,
                    option => option.MapFrom(source => source.IdemnityInsuranceExpireDate))
                .ForMember(destination => destination.PublicLiabilityInsuranceExpireDate,
                    option => option.MapFrom(source => source.PublicLiabilityInsuranceExpireDate))
                .ForMember(destination => destination.ServiceProviderAbnNumber,
                    option => option.MapFrom(source => source.CompanyNumber));

            CreateMap<EngineerDto, Engineer>()
                .BeforeMap((source, destination) => destination.PassportExpiryWarningSent = false) // Default Value
                .ForMember(destination => destination.ServiceProviderId,
                    option => option.MapFrom(source => source.ServiceProviderId))
                .ForMember(destination => destination.UserId,
                    option => option.MapFrom(source => source.UserId));

        }
    }
}