﻿using FixHubSheetImport.DatabaseConnection;
using Microsoft.AspNetCore.Mvc;

namespace FixHubSheetImport.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class DbConnectionController : Controller
    {
        [HttpGet]
        public JsonResult Get()
        {
            var allDbNames = FixHubConnectionManager.GetAllDbNames();
            return Json(allDbNames);
        }

        [HttpGet]
        [Route("dbselect/{dbName}")]
        public JsonResult DbSelect(string dbName)
        {
            DbManager.DbName = dbName;
            DbSetting dbSetting = new DbSetting {dbName = dbName};
            return Json(dbSetting);
        }

        [HttpGet]
        [Route("dbreset/")]
        public JsonResult DbReset()
        {
            DbManager.DbName = null;
            DbSetting dbSetting = new DbSetting { dbName = null };
            return Json(dbSetting);
        }

        [HttpGet]
        [Route("dbcode/{dbName}")]
        public JsonResult DbCodeSecret(string dbName)
        {
            DbSetting dbSetting = new DbSetting { dbName = dbName, dbCode = FixHubConnectionManager.GetDbCode(dbName) };
            return Json(dbSetting);
        }
    }

    public class DbSetting
    {
        public string dbName { get; set; }
        public string dbCode { get; set; }
    }
}