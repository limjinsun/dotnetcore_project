﻿using FixHubSheetImport.ServiceManagement;
using FixHubSheetImport.Services.GoogleSheetServices;
using Microsoft.AspNetCore.Mvc;

namespace FixHubSheetImport.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public partial class SheetController : Controller
    {
        private readonly IServiceInitializer _serviceInitializer;
        private readonly IAllSheetImportService _allSheetImportService;
        private readonly IAllSheetValidateService _allSheetValidateService;
        private readonly GoogleSheetMetadataService _googleSheetMetadataService;

        public SheetController(IServiceInitializer serviceInitializer, IAllSheetImportService allSheetImportService, IAllSheetValidateService allSheetValidateService, GoogleSheetMetadataService googleSheetMetadataService)
        {
            _serviceInitializer = serviceInitializer;
            _allSheetImportService = allSheetImportService;
            _allSheetValidateService = allSheetValidateService;
            _googleSheetMetadataService = googleSheetMetadataService;
        }

        [HttpGet]
        [Route("{spreadsheetId}/{sheetName}")]
        public JsonResult Get(string spreadsheetId, string sheetName)
        {
            var service = _serviceInitializer.CreateGoogleSheetService(sheetName);
            var dtos = service.GetData(spreadsheetId);
            var sheetData = new GoogleSheetData
            {
                Dto = dtos,
                SheetName = sheetName
            };
            return Json(sheetData);
        }

        [HttpPost]
        [Route("{sheetName}")]
        public JsonResult Post([FromBody] object dtoJson, [FromRoute] string sheetName)
        {
            var service = _serviceInitializer.CreateDatabaseService(sheetName);
            var transactionResult = service.InsertDto(dtoJson);
            return Json(transactionResult);
        }

        [HttpGet]
        [Route("metadata/{spreadsheetId}")]
        public JsonResult GetMetaData(string spreadsheetId)
        {
            var transactionResult = _googleSheetMetadataService.GetMetaInformation(spreadsheetId);
            return Json(transactionResult);
        }

        [HttpGet]
        [Route("metadata/{spreadsheetId}/{sheetName}")]
        public JsonResult GetMetaDataWithSheetName(string spreadsheetId, string sheetName)
        {
            var transactionResult = _googleSheetMetadataService.GetMetaInformationWithSheetName(spreadsheetId, sheetName);
            return Json(transactionResult);
        }

        [HttpGet]
        [Route("alldata/{spreadsheetId}")]
        public JsonResult ValidateAllData(string spreadsheetId)
        {
            var transactionResult = _allSheetValidateService.ValidateAllDataFromSheet(spreadsheetId);
            return Json(transactionResult);
        }

        [HttpPost]
        [Route("alldata/{spreadsheetId}")]
        public JsonResult ProcessAllData(string spreadsheetId)
        {
            var transactionResult = _allSheetImportService.ImportAllDataFromSheet(spreadsheetId);
            return Json(transactionResult);
        }
    }
}