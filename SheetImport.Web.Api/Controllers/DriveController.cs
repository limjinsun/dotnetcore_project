﻿using System;
using FixHubSheetImport.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FixHubSheetImport.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class DriveController : Controller
    {
        private readonly IGoogleDriveService _googleDriveService;

        public DriveController(IGoogleDriveService googleDriveService)
        {
            _googleDriveService = googleDriveService;
        }

        [HttpGet("{spreadsheetId}")]
        [ProducesResponseType(typeof(GoogleFileInfo), StatusCodes.Status200OK)]
        public JsonResult Get(string spreadsheetId)
        {
            GoogleFileInfo g;
            try
            {
                var file = _googleDriveService.GetFileData(spreadsheetId);
                g = new GoogleFileInfo {FileName = file.Title};
            }
            catch (Exception ex)
            {
                g = new GoogleFileInfo {Error = ex.Message, Debug = ToString()};
            }
            return Json(g);
        }
    }

    public class GoogleFileInfo
    {
        public string FileName { get; set; }
        public string Error { get; set; }
        public string Debug { get; set; }
    }
}