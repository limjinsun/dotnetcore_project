﻿using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;
using DataHubSheetImport.ServiceManagement;
using DataHubSheetImport.Services.DatabaseServices;
using DataHubSheetImport.Services.GoogleDriveServices;
using DataHubSheetImport.Services.GoogleSheetServices;
using Microsoft.Extensions.DependencyInjection;

namespace DataHubSheetImport.Web.Api
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            // AutoMapper configuration. 
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
                cfg.AllowNullCollections = true;
            });
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // For dependency injection.
            services.AddTransient<GoogleSheetMetadataService>();

            services.AddTransient<IGoogleSheetService<SiteUsersDto>, SiteUsersSheetService>();
            services.AddTransient<IGoogleSheetService<ModelsDto>, ModelsSheetService>();
            services.AddTransient<IGoogleSheetService<OemDto>, OemSheetService>();
            services.AddTransient<IGoogleSheetService<AssetsDto>, AssetsSheetService>();
            services.AddTransient<IGoogleSheetService<AssetCategoriesDto>, AssetCategoriesSheetService>();
            services.AddTransient<IGoogleSheetService<LocationDto>, LocationSheetService>();
            services.AddTransient<IGoogleSheetService<ServiceProvidersDto>, ServiceProvidersSheetService>();
            services.AddTransient<IGoogleSheetService<SitesDto>, SitesSheetService>();
            services.AddTransient<IGoogleSheetService<ServiceProviderUsersDto>, ServiceProviderUsersSheetService>();

            services.AddTransient<IDatabaseService<SiteUsersDto>, SiteUsersDbService>();
            services.AddTransient<IDatabaseService<OemDto>, OemDbService>();
            services.AddTransient<IDatabaseService<ModelsDto>, ModelsDbService>();
            services.AddTransient<IDatabaseService<AssetsDto>, AssetsDbService>();
            services.AddTransient<IDatabaseService<AssetCategoriesDto>, AssetCategoriesDbService>();
            services.AddTransient<IDatabaseService<LocationDto>, LocationDbService>();
            services.AddTransient<IDatabaseService<ServiceProvidersDto>, ServiceProvidersDbService>();
            services.AddTransient<IDatabaseService<SitesDto>, SitesDbService>();
            services.AddTransient<IDatabaseService<ServiceProviderUsersDto>, ServiceProviderUsersDbService>();

            services.AddTransient<BaseRepository<Machinecategory>, MachineCategoryRepository>();
            services.AddTransient<BaseRepository<Users>, UsersRepository>();
            services.AddTransient<BaseRepository<Oemdetails>, OemDetailsRepository>();
            services.AddTransient<BaseRepository<Partmetadata>, PartMetadataRepository>();
            services.AddTransient<BaseRepository<Machine>, MachineRepository>();
            services.AddTransient<BaseRepository<Station>, StationRepository>();
            services.AddTransient<BaseRepository<Serviceprovider>, ServiceProviderRepository>();
            services.AddTransient<BaseRepository<Store>, StoreRepository>();
            services.AddTransient<BaseRepository<Authorities>, AuthoritiesRepository>();
            services.AddTransient<BaseRepository<Userstorerelationship>, UserStoreRelationshipRepository>();
            services.AddTransient<BaseRepository<Engineer>, EngineerRepository>();
            services.AddTransient<BaseRepository<Userstore>, UserStoreRepository>();

            services.AddTransient<IGoogleDriveService, GoogleDriveService>();
            services.AddDbContext<DataHubContext>();

            services.AddTransient<IServiceInitializer, ServiceInitializer>(); // Custom service initialise. 
            services.AddTransient<IAllSheetImportService, AllSheetImportService>();
            services.AddTransient<IAllSheetValidateService, AllSheetValidateService>();

            return services;
        }


    }
}

