﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class StationRepository : BaseRepository<Station>
    {
        public StationRepository(DataHubContext context, ILogger<StationRepository> logger) : base(context, logger)
        {
        }

        // In order to find unique item in table. following SQL query used. 
        // SELECT * FROM station s LEFT JOIN translation_strings ts ON s.name_id = ts.translation_id WHERE ts.STRINGS = "translation_string";
        public override bool DoesEntityExist(string myString)
        {
            var result =
                        from s in MyContext.Station
                        join ts in MyContext.TranslationStrings on s.NameId equals ts.TranslationId
                        where ts.Strings == myString
                        select s;

            return result.Any();
        }

        public override void UpdateAction(Station t, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            Logger.LogWarning("Updating Method not existed : No need to update into table");
            processingResult.Status = ProcessStatus.Warning;

            errorDetails.Msg = "No need to update into table";
            errorDetails.DebugClassName = this.ToString();
        }

        public override long GetIdByString(string s)
        {
            var translationString = MyContext.TranslationStrings.SingleOrDefault(o => o.Strings == s);
            var station = MyContext.Station.SingleOrDefault(o => o.NameId == translationString.TranslationId);
            return station != null ? station.Id : -1;
        }
    }
}
