﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class OemDetailsRepository : BaseRepository<Oemdetails>
    {
        public OemDetailsRepository(DataHubContext context, ILogger<OemDetailsRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Oemdetails oemDetails)
        {
            return MyContext.Oemdetails.Any(o => o.Name == oemDetails.Name);
        }

        public override void UpdateAction(Oemdetails oemDetails, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            Logger.LogWarning("Update Method not existed (No need to update into table - Table has only one column)");
            processingResult.Status = ProcessStatus.Warning;

            errorDetails.Msg = "No need to update into table - Table has only one column";
            errorDetails.DebugClassName = this.ToString();
        }

        public override long GetIdByString(string s)
        {
            var oemdetails = MyContext.Oemdetails.FirstOrDefault(o => o.Name == s);
            return oemdetails != null ? oemdetails.Id : -1;
        }

    }
}
