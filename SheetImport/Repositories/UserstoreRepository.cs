﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class UserStoreRepository : BaseRepository<Userstore>
    {
        public UserStoreRepository(DataHubContext context, ILogger<UserStoreRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Userstore userstore)
        {
            return MyContext.Userstore.Any(e => e == userstore);
        }

        public override void UpdateAction(Userstore userstore, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            var existedUserstore = MyContext.Userstore.SingleOrDefault(o => o.UserId == userstore.UserId && o.StoreId == userstore.StoreId);
            if (existedUserstore != null)
            {
                if (userstore.UserId != null) existedUserstore.UserId = userstore.UserId;
                if (userstore.StoreId != null) existedUserstore.StoreId = userstore.StoreId;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + userstore);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = userstore.ToString();
                commonDetails.UpdatedItem = userstore;
            }
        }
    }
}
