﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class EngineerRepository : BaseRepository<Engineer>
    {
        public EngineerRepository(DataHubContext context, ILogger<EngineerRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Engineer engineer)
        {
            return MyContext.Engineer.Any(o => o.UserId == engineer.UserId);
        }

        public override void UpdateAction(Engineer engineer, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails, DbProcessingErrorWarningDetail errorDetails)
        {
            var existedEngineer = MyContext.Engineer.SingleOrDefault(o => o.UserId == engineer.UserId);
            if (existedEngineer != null)
            {
                if (engineer.ServiceProviderId != null) existedEngineer.ServiceProviderId = engineer.ServiceProviderId;
                if (engineer.UserId != null) existedEngineer.UserId = engineer.UserId;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + engineer);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = engineer.ToString();
                commonDetails.UpdatedItem = engineer;
            }
        }
    }
}
