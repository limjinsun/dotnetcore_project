﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class UsersRepository : BaseRepository<Users>
    {
        public UsersRepository(DataHubContext context, ILogger<UsersRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Users user)
        {
            return MyContext.Users.Any(o => o.Username == user.Username);
        }

        public override void UpdateAction(Users user, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            Users existedUser = MyContext.Users.SingleOrDefault(o => o.Username == user.Username);
            if (existedUser != null)
            {
                // 4 Data from Google sheets.
                if (user.FirstName != null) existedUser.FirstName = user.FirstName;
                if (user.LastName != null) existedUser.LastName = user.LastName;
                if (user.Email != null) existedUser.Email = user.Email;
                if (user.MobileNumber != null) existedUser.MobileNumber = user.MobileNumber;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + user);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = user.ToString();
                commonDetails.UpdatedItem = user;
            }
        }

        public override long GetIdByString(string s)
        {
            var user = MyContext.Users.SingleOrDefault(o => o.Username == s);
            return user != null ? user.Id : -1;
        }
    }
}