﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class AuthoritiesRepository : BaseRepository<Authorities>
    {
        public AuthoritiesRepository(DataHubContext context, ILogger<AuthoritiesRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Authorities authorities)
        {
            return MyContext.Authorities.Any(o => o.UserId == authorities.UserId);
        }

        public override void UpdateAction(Authorities authorities, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails, DbProcessingErrorWarningDetail errorDetails)
        {
            var existedAuthorities = MyContext.Authorities.SingleOrDefault(o => o.UserId == authorities.UserId);
            if (existedAuthorities != null)
            {
                if (authorities.Authority != null) existedAuthorities.Authority = authorities.Authority;
                if (authorities.UserId != null) existedAuthorities.UserId = authorities.UserId;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + authorities);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = authorities.ToString();
                commonDetails.UpdatedItem = authorities;
            }
        }
    }
}