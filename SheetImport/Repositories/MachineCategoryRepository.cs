﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class MachineCategoryRepository : BaseRepository<Machinecategory>
    {
        public MachineCategoryRepository(DataHubContext context, ILogger<MachineCategoryRepository> logger) : base(context, logger)
        {
        }

        // 'categoryCode' was not 'unique' key. but It was used to test if entity exist. 
        public override bool DoesEntityExist(string categoryCode)
        {
            return MyContext.Machinecategory.Any(o => o.CategoryCode == categoryCode);
        }

        public override void UpdateAction(Machinecategory t, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails, DbProcessingErrorWarningDetail errorDetails)
        {
            Logger.LogWarning("Update Method not existed (No need to update into table)"); ;
            processingResult.Status = ProcessStatus.Warning;

            errorDetails.Msg = "No need to update into table";
            errorDetails.DebugClassName = this.ToString();
        }

        // In order to find unique item in table. following SQL query used. 
        // SELECT * FROM machinecategory mc LEFT JOIN translation_strings ts ON mc.name_id = ts.translation_id WHERE ts.STRINGS = "some string";
        public override long GetIdByString(string s)
        {
            var result =
                from mc in MyContext.Machinecategory
                join ts in MyContext.TranslationStrings on mc.NameId equals ts.TranslationId
                where ts.Strings == s
                select mc;

            var machineCategory = result.SingleOrDefault();
            return machineCategory != null? machineCategory.Id : -1;
        }
    }
}
