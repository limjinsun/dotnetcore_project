﻿using System;
using System.Collections;
using System.Globalization;
using DataHubSheetImport.Services.DatabaseServices;
using Google.Apis.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public abstract class BaseRepository<TEntity> where TEntity : class
    {
        public readonly DataHubContext MyContext;
        public readonly ILogger Logger;

        protected BaseRepository(DataHubContext myContext, ILogger<BaseRepository<TEntity>> logger)
        {
            MyContext = myContext;
            this.Logger = logger;
        }

        public virtual DbProcessingResult AddEntity(TEntity t)
        {
            Logger.LogDebug("Trying adding Entity : " + t + "Connection : " + MyContext.Database.GetDbConnection().ConnectionString);

            var processingResult = new DbProcessingResult();
            var commonDetails = new DbProcessingCommonDetail();
            var errorDetails = new DbProcessingErrorWarningDetail();

            commonDetails.Dto = DtoTracker.CurrentDto;
            commonDetails.DtoPresentName = DtoTracker.DtoPresentName;
            commonDetails.ClassName = t.GetType().Name;

            try
            {
                MyContext.Add(t);
                MyContext.SaveChanges();
                Logger.LogDebug("Adding Success : " + t);
                processingResult.Status = ProcessStatus.Added;
                commonDetails.DbModelName = t.ToString();
            }
            catch (Exception e)
            {
                Logger.LogError("Adding Fail : " + t + e.ToString());
                processingResult.Status = ProcessStatus.Error;

                if (e.InnerException != null) errorDetails.Msg = e.InnerException.ToString();
                errorDetails.DebugClassName = this.ToString();
                errorDetails.Data = e.Data;
            }

            processingResult.Time = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-IE"));
            processingResult.Details = commonDetails;
            processingResult.ErrorOrWarningDetails = errorDetails;

            return processingResult;
        }

        public virtual DbProcessingResult UpdateEntity(TEntity t)
        {
            Logger.LogDebug("Trying updating Entity : " + t + " Connection : " + MyContext.Database.GetDbConnection().ConnectionString);

            var processingResult = new DbProcessingResult();
            var commonDetails = new DbProcessingCommonDetail();
            var errorDetails = new DbProcessingErrorWarningDetail();

            commonDetails.ClassName = t.GetType().Name;
            commonDetails.Dto = DtoTracker.CurrentDto;
            commonDetails.DtoPresentName = DtoTracker.DtoPresentName;

            try
            {
                UpdateAction(t, processingResult, commonDetails, errorDetails);
            }
            catch (Exception e)
            {
                Logger.LogError("Updating fail : " + e.ToString());
                processingResult.Status = ProcessStatus.Error;
                if (e.InnerException != null) errorDetails.Msg = e.InnerException.ToString();
                errorDetails.DebugClassName = this.ToString();
                errorDetails.Data = e.Data;
            }

            processingResult.Details = commonDetails;
            processingResult.ErrorOrWarningDetails = errorDetails;
            processingResult.Time = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-IE"));

            return processingResult;
        }

        public abstract void UpdateAction(TEntity t, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails, DbProcessingErrorWarningDetail errorDetails);

        public virtual long GetIdByString(string s)
        {
            return -1; // when error happen '-1' value return.
        }

        public virtual bool DoesEntityExist(TEntity t)
        {
            return false;
        }

        public virtual bool DoesEntityExist(string s)
        {
            return false;
        }
    }

    public class DbProcessingResult
    {
        public ProcessStatus Status;
        public DbProcessingCommonDetail Details;
        public DbProcessingErrorWarningDetail ErrorOrWarningDetails;
        public string Time;
    }

    public enum ProcessStatus
    {
        [StringValue("added")]
        Added = 1,
        [StringValue("updated")]
        Updated = 2,
        [StringValue("warning")]
        Warning = 3,
        [StringValue("error")]
        Error = 4, 
    }

    public class DbProcessingCommonDetail
    {
        public string DbModelName;
        public string ClassName;
        public object Dto;
        public string DtoPresentName;
        public object UpdatedItem;
    }

    public class DbProcessingErrorWarningDetail
    {
        public string Msg;
        public string DebugClassName;
        public IDictionary Data;
    }
}