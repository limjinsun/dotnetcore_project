﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class UserStoreRelationshipRepository : BaseRepository<Userstorerelationship>
    {
        public UserStoreRelationshipRepository(DataHubContext context, ILogger<UserStoreRelationshipRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Userstorerelationship userStoreRelationship)
        {
            return MyContext.Userstorerelationship.Any(o => o.UserId == userStoreRelationship.UserId && o.StoreId == userStoreRelationship.StoreId);
        }

        public override void UpdateAction(Userstorerelationship userStoreRelationship, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            var existedUserStoreRelationship = MyContext.Userstorerelationship.SingleOrDefault(o =>
                o.UserId == userStoreRelationship.UserId &&
                o.StoreId == userStoreRelationship.StoreId);
            if (existedUserStoreRelationship != null)
            {
                existedUserStoreRelationship.UserId = userStoreRelationship.UserId;
                existedUserStoreRelationship.StoreId = userStoreRelationship.StoreId;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + userStoreRelationship);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = userStoreRelationship.ToString();
                commonDetails.UpdatedItem = userStoreRelationship;
            }
        }
    }
}
