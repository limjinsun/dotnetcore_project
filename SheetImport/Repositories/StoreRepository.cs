﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class StoreRepository : BaseRepository<Store>
    {
        public StoreRepository(DataHubContext context, ILogger<StoreRepository> logger) : base(context, logger)
        {
        }
        // `Name` is not a unique key in a table. but It was assumed as unique value. (Unique key is `Store Number`)
        public override bool DoesEntityExist(Store store)
        {
            return MyContext.Store.Any(o => o.Name == store.Name);
        }

        public override void UpdateAction(Store store, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            if (store.StoreNumber == null)
            {
                Logger.LogWarning("Found store `name` in the table, but `storenumber` is null.");
                processingResult.Status = ProcessStatus.Warning;

                errorDetails.Msg = "Found store `name` in the table, but `storenumber` is null.";
                errorDetails.DebugClassName = this.ToString();

                return;
            }

            var existedStore = MyContext.Store.SingleOrDefault(o => o.Name == store.Name);
            if (existedStore != null)
            {
                if (store.Email != null) existedStore.Email = store.Email;
                if (store.PhoneNumber != null) existedStore.PhoneNumber = store.PhoneNumber;
                if (store.McDonaldsStoreId != null) existedStore.McDonaldsStoreId = store.McDonaldsStoreId;
                if (store.Name != null) existedStore.Name = store.Name;
                if (store.RequiresServiceRequestApproval != null) existedStore.RequiresServiceRequestApproval = store.RequiresServiceRequestApproval;
                if (store.AddressType != null) existedStore.AddressType = store.AddressType;
                if (store.Country != null) existedStore.Country = store.Country;
                if (store.MajorMunicipality != null) existedStore.MajorMunicipality = store.MajorMunicipality;
                if (store.MinorMunicipality != null) existedStore.MinorMunicipality = store.MinorMunicipality;
                if (store.PostalArea != null) existedStore.PostalArea = store.PostalArea;
                if (store.StreetName != null) existedStore.StreetName = store.StreetName;
                if (store.StoreNumber != null) existedStore.StoreNumber = store.StoreNumber;
                if (store.OwnerId != null) existedStore.OwnerId = store.OwnerId;
                if (store.Password != null) existedStore.Password = store.Password;
                if (store.RequiresServiceRequestPmApproval != null) existedStore.RequiresServiceRequestPmApproval = store.RequiresServiceRequestPmApproval;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + store);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = store.ToString();
                commonDetails.UpdatedItem = store;
            }
        }

        public override long GetIdByString(string s)
        {
            var store = MyContext.Store.SingleOrDefault(o => o.Name == s);
            return store != null? store.Id : -1;
        }
    }
}