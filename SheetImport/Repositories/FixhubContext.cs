﻿using DataHubSheetImport.DatabaseConnection;
using DataHubSheetImport.Models;
using Microsoft.EntityFrameworkCore;

namespace DataHubSheetImport.Repositories
{
    public partial class DataHubContext : DbContext
    {

        public DataHubContext()
        {
        }

        public DataHubContext(DbContextOptions<DataHubContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (DbManager.DbName != null && !optionsBuilder.IsConfigured)
            {
                var dbName = DbManager.DbName;
                var dbConnectionString = DbManager.GetDbConnectionString(dbName);
                optionsBuilder.UseMySql(dbConnectionString);
            }
        }

        public virtual DbSet<Assetchangelog> Assetchangelog { get; set; }
        public virtual DbSet<Attachment> Attachment { get; set; }
        public virtual DbSet<Auditlog> Auditlog { get; set; }
        public virtual DbSet<Auditloglocalisation> Auditloglocalisation { get; set; }
        public virtual DbSet<Authenticationtokens> Authenticationtokens { get; set; }
        public virtual DbSet<Authorisation> Authorisation { get; set; }
        public virtual DbSet<Authorities> Authorities { get; set; }
        public virtual DbSet<Availability> Availability { get; set; }
        public virtual DbSet<Avoidablecalltype> Avoidablecalltype { get; set; }
        public virtual DbSet<Batchplannedmaintenancesettings> Batchplannedmaintenancesettings { get; set; }
        public virtual DbSet<Cancellationreason> Cancellationreason { get; set; }
        public virtual DbSet<Causeappliesto> Causeappliesto { get; set; }
        public virtual DbSet<Certification> Certification { get; set; }
        public virtual DbSet<Changepasswordtoken> Changepasswordtoken { get; set; }
        public virtual DbSet<Claim> Claim { get; set; }
        public virtual DbSet<Claimattachment> Claimattachment { get; set; }
        public virtual DbSet<Claimlineitem> Claimlineitem { get; set; }
        public virtual DbSet<Claimnote> Claimnote { get; set; }
        public virtual DbSet<Commoncause> Commoncause { get; set; }
        public virtual DbSet<Commonfault> Commonfault { get; set; }
        public virtual DbSet<Configuredwarrantyrule> Configuredwarrantyrule { get; set; }
        public virtual DbSet<Controlmeasure> Controlmeasure { get; set; }
        public virtual DbSet<Costbreakdown> Costbreakdown { get; set; }
        public virtual DbSet<Costcategory> Costcategory { get; set; }
        public virtual DbSet<Costcategorygroup> Costcategorygroup { get; set; }
        public virtual DbSet<Costcategorygrouprelationship> Costcategorygrouprelationship { get; set; }
        public virtual DbSet<Costlineitem> Costlineitem { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<DashboardLog> DashboardLog { get; set; }
        public virtual DbSet<DataSynchronisationLog> DataSynchronisationLog { get; set; }
        public virtual DbSet<DbCostByCategory> DbCostByCategory { get; set; }
        public virtual DbSet<DbCostByOem> DbCostByOem { get; set; }
        public virtual DbSet<DbCostByProvider> DbCostByProvider { get; set; }
        public virtual DbSet<DbCostByStore> DbCostByStore { get; set; }
        public virtual DbSet<DbCountByCategory> DbCountByCategory { get; set; }
        public virtual DbSet<DbPlannedMaintenanceSummary> DbPlannedMaintenanceSummary { get; set; }
        public virtual DbSet<DbResponseTimes> DbResponseTimes { get; set; }
        public virtual DbSet<DbServiceEventByType> DbServiceEventByType { get; set; }
        public virtual DbSet<DbServiceEventByTypeCombinePm> DbServiceEventByTypeCombinePm { get; set; }
        public virtual DbSet<DbServiceRequestCost> DbServiceRequestCost { get; set; }
        public virtual DbSet<DbServiceRequestCost30d> DbServiceRequestCost30d { get; set; }
        public virtual DbSet<DbServiceRequestCostYtd> DbServiceRequestCostYtd { get; set; }
        public virtual DbSet<DbServiceRequestCount> DbServiceRequestCount { get; set; }
        public virtual DbSet<DbServiceRequestCount30d> DbServiceRequestCount30d { get; set; }
        public virtual DbSet<DbServiceRequestCountYtd> DbServiceRequestCountYtd { get; set; }
        public virtual DbSet<DbServiceRequestFirstTimeFix> DbServiceRequestFirstTimeFix { get; set; }
        public virtual DbSet<DbServiceRequestOutcome> DbServiceRequestOutcome { get; set; }
        public virtual DbSet<DbServiceRequestVisits> DbServiceRequestVisits { get; set; }
        public virtual DbSet<DbTotalInitialArrivalTime> DbTotalInitialArrivalTime { get; set; }
        public virtual DbSet<DbTotalInitialResponseTime> DbTotalInitialResponseTime { get; set; }
        public virtual DbSet<DbTotalWarranty> DbTotalWarranty { get; set; }
        public virtual DbSet<DbTurnaroundInMinute> DbTurnaroundInMinute { get; set; }
        public virtual DbSet<Defaultcost> Defaultcost { get; set; }
        public virtual DbSet<Deferralreason> Deferralreason { get; set; }
        public virtual DbSet<Deliverymethod> Deliverymethod { get; set; }
        public virtual DbSet<Engineer> Engineer { get; set; }
        public virtual DbSet<Eventtype> Eventtype { get; set; }
        public virtual DbSet<Eventtypechangelog> Eventtypechangelog { get; set; }
        public virtual DbSet<Eventtypegroup> Eventtypegroup { get; set; }
        public virtual DbSet<Eventtypegrouprelationship> Eventtypegrouprelationship { get; set; }
        public virtual DbSet<Eventtypeserviceprovider> Eventtypeserviceprovider { get; set; }
        public virtual DbSet<Extendedcontractorapproval> Extendedcontractorapproval { get; set; }
        public virtual DbSet<Faultappliesto> Faultappliesto { get; set; }
        public virtual DbSet<Governingdistrict> Governingdistrict { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<Groupstores> Groupstores { get; set; }
        public virtual DbSet<Grouptype> Grouptype { get; set; }
        public virtual DbSet<Groupusers> Groupusers { get; set; }
        public virtual DbSet<Hazardassessment> Hazardassessment { get; set; }
        public virtual DbSet<HazardassessmentControlmeasure> HazardassessmentControlmeasure { get; set; }
        public virtual DbSet<HazardassessmentVisit> HazardassessmentVisit { get; set; }
        public virtual DbSet<Kit> Kit { get; set; }
        public virtual DbSet<KitPart> KitPart { get; set; }
        public virtual DbSet<Kpi> Kpi { get; set; }
        public virtual DbSet<Machine> Machine { get; set; }
        public virtual DbSet<Machinecategory> Machinecategory { get; set; }
        public virtual DbSet<Machinecheck> Machinecheck { get; set; }
        public virtual DbSet<Machinefile> Machinefile { get; set; }
        public virtual DbSet<Machinetocomponentmetadata> Machinetocomponentmetadata { get; set; }
        public virtual DbSet<MachineWarrantySynchronisationLog> MachineWarrantySynchronisationLog { get; set; }
        public virtual DbSet<Modelproperty> Modelproperty { get; set; }
        public virtual DbSet<Modelpropertyvalue> Modelpropertyvalue { get; set; }
        public virtual DbSet<Mycalendar> Mycalendar { get; set; }
        public virtual DbSet<Mycalendarevent> Mycalendarevent { get; set; }
        public virtual DbSet<Notificationmessage> Notificationmessage { get; set; }
        public virtual DbSet<Notificationsubscription> Notificationsubscription { get; set; }
        public virtual DbSet<Oemdetails> Oemdetails { get; set; }
        public virtual DbSet<Oemgroup> Oemgroup { get; set; }
        public virtual DbSet<Oemgroupmember> Oemgroupmember { get; set; }
        public virtual DbSet<Partmetadata> Partmetadata { get; set; }
        public virtual DbSet<Partnumbermapping> Partnumbermapping { get; set; }
        public virtual DbSet<Pickslip> Pickslip { get; set; }
        public virtual DbSet<Pickslipitem> Pickslipitem { get; set; }
        public virtual DbSet<Plannedmaintenancecycle> Plannedmaintenancecycle { get; set; }
        public virtual DbSet<Plannedmaintenanceoverride> Plannedmaintenanceoverride { get; set; }
        public virtual DbSet<Plannedmaintenancestep> Plannedmaintenancestep { get; set; }
        public virtual DbSet<Plannedmaintenancetype> Plannedmaintenancetype { get; set; }
        public virtual DbSet<Potentialhazard> Potentialhazard { get; set; }
        public virtual DbSet<Providershortlistentry> Providershortlistentry { get; set; }
        public virtual DbSet<QrtzBlobTriggers> QrtzBlobTriggers { get; set; }
        public virtual DbSet<QrtzCalendars> QrtzCalendars { get; set; }
        public virtual DbSet<QrtzCronTriggers> QrtzCronTriggers { get; set; }
        public virtual DbSet<QrtzFiredTriggers> QrtzFiredTriggers { get; set; }
        public virtual DbSet<QrtzJobDetails> QrtzJobDetails { get; set; }
        public virtual DbSet<QrtzJobListeners> QrtzJobListeners { get; set; }
        public virtual DbSet<QrtzLocks> QrtzLocks { get; set; }
        public virtual DbSet<QrtzPausedTriggerGrps> QrtzPausedTriggerGrps { get; set; }
        public virtual DbSet<QrtzSchedulerState> QrtzSchedulerState { get; set; }
        public virtual DbSet<QrtzSimpleTriggers> QrtzSimpleTriggers { get; set; }
        public virtual DbSet<QrtzSimpropTriggers> QrtzSimpropTriggers { get; set; }
        public virtual DbSet<QrtzTriggerListeners> QrtzTriggerListeners { get; set; }
        public virtual DbSet<QrtzTriggers> QrtzTriggers { get; set; }
        public virtual DbSet<Recommendedsalesprice> Recommendedsalesprice { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<Rejectionreason> Rejectionreason { get; set; }
        public virtual DbSet<Reminderscheduler> Reminderscheduler { get; set; }
        public virtual DbSet<Reportperiod> Reportperiod { get; set; }
        public virtual DbSet<Ruledefinition> Ruledefinition { get; set; }
        public virtual DbSet<RuledefinitionCostcategoryids> RuledefinitionCostcategoryids { get; set; }
        public virtual DbSet<Sampletestquestion> Sampletestquestion { get; set; }
        public virtual DbSet<Sampletestresult> Sampletestresult { get; set; }
        public virtual DbSet<Scheduledreport> Scheduledreport { get; set; }
        public virtual DbSet<Scheduledreportparameter> Scheduledreportparameter { get; set; }
        public virtual DbSet<Schedulestep> Schedulestep { get; set; }
        public virtual DbSet<SchemaVersion> SchemaVersion { get; set; }
        public virtual DbSet<Servicecontrollernotification> Servicecontrollernotification { get; set; }
        public virtual DbSet<Serviceevent> Serviceevent { get; set; }
        public virtual DbSet<ServiceEventPartsList> ServiceEventPartsList { get; set; }
        public virtual DbSet<Serviceeventtocomponentmetadata> Serviceeventtocomponentmetadata { get; set; }
        public virtual DbSet<Servicenote> Servicenote { get; set; }
        public virtual DbSet<Serviceprovider> Serviceprovider { get; set; }
        public virtual DbSet<Serviceprovidernotification> Serviceprovidernotification { get; set; }
        public virtual DbSet<Servicerelationship> Servicerelationship { get; set; }
        public virtual DbSet<Servicerequest> Servicerequest { get; set; }
        public virtual DbSet<Station> Station { get; set; }
        public virtual DbSet<Store> Store { get; set; }
        public virtual DbSet<Storenotification> Storenotification { get; set; }
        public virtual DbSet<Supportresource> Supportresource { get; set; }
        public virtual DbSet<Surveyresponse> Surveyresponse { get; set; }
        public virtual DbSet<Systemlabel> Systemlabel { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<Taskinstance> Taskinstance { get; set; }
        public virtual DbSet<Ticketnumberoffsetreferencedata> Ticketnumberoffsetreferencedata { get; set; }
        public virtual DbSet<Timeperiod> Timeperiod { get; set; }
        public virtual DbSet<Translation> Translation { get; set; }
        public virtual DbSet<TranslationStrings> TranslationStrings { get; set; }
        public virtual DbSet<Usernotification> Usernotification { get; set; }
        public virtual DbSet<Useroemrelationship> Useroemrelationship { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Userstore> Userstore { get; set; }
        public virtual DbSet<Userstorerelationship> Userstorerelationship { get; set; }
        public virtual DbSet<Visit> Visit { get; set; }
        public virtual DbSet<Visitapprover> Visitapprover { get; set; }
        public virtual DbSet<Visitserviceevent> Visitserviceevent { get; set; }
        public virtual DbSet<Visitserviceeventpart> Visitserviceeventpart { get; set; }
        public virtual DbSet<Warrantyauditlog> Warrantyauditlog { get; set; }
        public virtual DbSet<Warrantyclassification> Warrantyclassification { get; set; }
        public virtual DbSet<Warrantyclassificationequipmentassociation> Warrantyclassificationequipmentassociation { get; set; }
        public virtual DbSet<Warrantyclassificationreviewers> Warrantyclassificationreviewers { get; set; }
        public virtual DbSet<Warrantyclassificationruledefinition> Warrantyclassificationruledefinition { get; set; }
        public virtual DbSet<Warrantyprovider> Warrantyprovider { get; set; }
        public virtual DbSet<Warrantysupport> Warrantysupport { get; set; }
        public virtual DbSet<Workcoverdetail> Workcoverdetail { get; set; }

        // Unable to generate entity type for table 'DataHub.oauth_access_token'. Please see the warning messages.
        // Unable to generate entity type for table 'DataHub.oauth_code'. Please see the warning messages.
        // Unable to generate entity type for table 'DataHub.oauth_refresh_token'. Please see the warning messages.
        // Unable to generate entity type for table 'DataHub.partmetadata_partmetadata'. Please see the warning messages.
        // Unable to generate entity type for table 'DataHub.refresh_report_tables'. Please see the warning messages.
        // Unable to generate entity type for table 'DataHub.serviceevent_modelpropertyvalue'. Please see the warning messages.



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Assetchangelog>(entity =>
            {
                entity.ToTable("assetchangelog");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineId)
                    .HasName("fk_assetchangelog_machine_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ChangeDate).HasColumnName("changeDate");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.NewValue)
                    .HasColumnName("newValue")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Property)
                    .HasColumnName("property")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.User)
                    .HasColumnName("user")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Assetchangelog)
                    .HasForeignKey(d => d.MachineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("assetchangelog_ibfk_1");
            });

            modelBuilder.Entity<Attachment>(entity =>
            {
                entity.ToTable("attachment");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineId)
                    .HasName("fk_attachment_machine_id");

                entity.HasIndex(e => e.ServiceRequestId)
                    .HasName("FK1C93543B13C638D");

                entity.HasIndex(e => e.UserId)
                    .HasName("FK_users_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasColumnName("fileName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize)
                    .HasColumnName("file_size")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MarkDeleted)
                    .HasColumnName("mark_deleted")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UploadDate).HasColumnName("upload_date");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Attachment)
                    .HasForeignKey(d => d.MachineId)
                    .HasConstraintName("fk_attachment_machine_id");

                entity.HasOne(d => d.ServiceRequest)
                    .WithMany(p => p.Attachment)
                    .HasForeignKey(d => d.ServiceRequestId)
                    .HasConstraintName("FK1C93543B13C638D");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Attachment)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_users_id");
            });

            modelBuilder.Entity<Auditlog>(entity =>
            {
                entity.ToTable("auditlog");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => new { e.RefersTo, e.LogType })
                    .HasName("auditlog_refers_to_type");

                entity.HasIndex(e => new { e.RefersToObjectType, e.RefersTo, e.NewState })
                    .HasName("auditlog_refers_to");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EventDateTime).HasColumnName("event_date_time");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LogDateTime).HasColumnName("log_date_time");

                entity.Property(e => e.LogType)
                    .HasColumnName("log_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NewState)
                    .HasColumnName("new_state")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PreviousState)
                    .HasColumnName("previous_state")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.RefersTo)
                    .HasColumnName("refers_to")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RefersToObjectType)
                    .HasColumnName("refers_to_object_type")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.User)
                    .HasColumnName("user")
                    .HasMaxLength(48)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Auditloglocalisation>(entity =>
            {
                entity.ToTable("auditloglocalisation");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => new { e.AuditlogId, e.Locale })
                    .HasName("auditlog_id_locale_unique_index")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AuditlogId)
                    .HasColumnName("auditlog_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasColumnType("longtext");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Locale)
                    .IsRequired()
                    .HasColumnName("locale")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.Auditlog)
                    .WithMany(p => p.Auditloglocalisation)
                    .HasForeignKey(d => d.AuditlogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("auditloglocalisation_ibfk_1");
            });

            modelBuilder.Entity<Authenticationtokens>(entity =>
            {
                entity.HasKey(e => e.Token);

                entity.ToTable("authenticationtokens");

                entity.HasIndex(e => e.UserId)
                    .HasName("FKD8E2AE12D914A9CD");

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExpirationTime).HasColumnName("expiration_time");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Authenticationtokens)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FKD8E2AE12D914A9CD");
            });

            modelBuilder.Entity<Authorisation>(entity =>
            {
                entity.ToTable("authorisation");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => new { e.RefId, e.Role, e.Functionality })
                    .HasName("authorisation_unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Authorised)
                    .HasColumnName("authorised")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Functionality)
                    .IsRequired()
                    .HasColumnName("functionality")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RefId)
                    .HasColumnName("ref_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Updated).HasColumnName("updated");
            });

            modelBuilder.Entity<Authorities>(entity =>
            {
                entity.ToTable("authorities");

                entity.HasIndex(e => e.Authority)
                    .HasName("idx_authorities_authority");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.UserId)
                    .HasName("FK2B0F1321D914A9CD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Authority)
                    .HasColumnName("authority")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Authorities)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK2B0F1321D914A9CD");
            });

            modelBuilder.Entity<Availability>(entity =>
            {
                entity.ToTable("availability");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK80DB697B8963B647");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FK80DB697BBF9BE887");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DayOfWeek)
                    .HasColumnName("dayOfWeek")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Availability)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK80DB697B8963B647");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Availability)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK80DB697BBF9BE887");
            });

            modelBuilder.Entity<Avoidablecalltype>(entity =>
            {
                entity.ToTable("avoidablecalltype");

                entity.HasIndex(e => e.CodeId)
                    .HasName("FK_avoidablecalltype_code_Id");

                entity.HasIndex(e => e.DescriptionId)
                    .HasName("FK_avoidablecalltype_description_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CodeId)
                    .HasColumnName("code_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DescriptionId)
                    .HasColumnName("description_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Code)
                    .WithMany(p => p.AvoidablecalltypeCode)
                    .HasForeignKey(d => d.CodeId)
                    .HasConstraintName("FK_avoidablecalltype_code_Id");

                entity.HasOne(d => d.Description)
                    .WithMany(p => p.AvoidablecalltypeDescription)
                    .HasForeignKey(d => d.DescriptionId)
                    .HasConstraintName("FK_avoidablecalltype_description_Id");
            });

            modelBuilder.Entity<Batchplannedmaintenancesettings>(entity =>
            {
                entity.ToTable("batchplannedmaintenancesettings");

                entity.HasIndex(e => e.RegionId)
                    .HasName("FK9E122EACD0E53C6D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AdvanceScheduleDays)
                    .HasColumnName("advance_schedule_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CronExpression)
                    .HasColumnName("cron_expression")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.WindowSizeDays)
                    .HasColumnName("window_size_days")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Batchplannedmaintenancesettings)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK9E122EACD0E53C6D");
            });

            modelBuilder.Entity<Cancellationreason>(entity =>
            {
                entity.ToTable("cancellationreason");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ReasonId)
                    .HasName("FK_cancellationreason_reason_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ReasonId)
                    .HasColumnName("reason_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Visibility)
                    .HasColumnName("visibility")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Reason)
                    .WithMany(p => p.Cancellationreason)
                    .HasForeignKey(d => d.ReasonId)
                    .HasConstraintName("FK_cancellationreason_reason_Id");
            });

            modelBuilder.Entity<Causeappliesto>(entity =>
            {
                entity.ToTable("causeappliesto");

                entity.HasIndex(e => e.CommonCauseId)
                    .HasName("FK3F7825DEB42B0587");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FK3F7825DE5F04BDE7");

                entity.HasIndex(e => e.MachineMetaDataId)
                    .HasName("FK3F7825DE8D2BB867");

                entity.HasIndex(e => e.OemDetailsId)
                    .HasName("FK3F7825DE1EEB76D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CommonCauseId)
                    .HasColumnName("commonCause_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MachineMetaDataId)
                    .HasColumnName("machineMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemDetailsId)
                    .HasColumnName("oemDetails_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemFaultCode)
                    .HasColumnName("oem_fault_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CommonCause)
                    .WithMany(p => p.Causeappliesto)
                    .HasForeignKey(d => d.CommonCauseId)
                    .HasConstraintName("FK3F7825DEB42B0587");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Causeappliesto)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FK3F7825DE5F04BDE7");

                entity.HasOne(d => d.MachineMetaData)
                    .WithMany(p => p.Causeappliesto)
                    .HasForeignKey(d => d.MachineMetaDataId)
                    .HasConstraintName("FK3F7825DE8D2BB867");

                entity.HasOne(d => d.OemDetails)
                    .WithMany(p => p.Causeappliesto)
                    .HasForeignKey(d => d.OemDetailsId)
                    .HasConstraintName("FK3F7825DE1EEB76D");
            });

            modelBuilder.Entity<Certification>(entity =>
            {
                entity.ToTable("certification");

                entity.HasIndex(e => e.EngineerId)
                    .HasName("FK934849A853EC5CD");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FK934849A5F04BDE7");

                entity.HasIndex(e => e.MachineMetaDataId)
                    .HasName("FK934849A8D2BB867");

                entity.HasIndex(e => e.OemDetailsId)
                    .HasName("FK934849A1EEB76D");

                entity.HasIndex(e => new { e.MachineCategoryId, e.EngineerId, e.OemDetailsId })
                    .HasName("idx_eng_cat_mapping")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EngineerId)
                    .HasColumnName("engineer_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MachineMetaDataId)
                    .HasColumnName("machineMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemDetailsId)
                    .HasColumnName("oemDetails_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Engineer)
                    .WithMany(p => p.Certification)
                    .HasForeignKey(d => d.EngineerId)
                    .HasConstraintName("FK934849A853EC5CD");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Certification)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FK934849A5F04BDE7");

                entity.HasOne(d => d.MachineMetaData)
                    .WithMany(p => p.Certification)
                    .HasForeignKey(d => d.MachineMetaDataId)
                    .HasConstraintName("FK934849A8D2BB867");

                entity.HasOne(d => d.OemDetails)
                    .WithMany(p => p.Certification)
                    .HasForeignKey(d => d.OemDetailsId)
                    .HasConstraintName("FK934849A1EEB76D");
            });

            modelBuilder.Entity<Changepasswordtoken>(entity =>
            {
                entity.ToTable("changepasswordtoken");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ExpirationTime).HasColumnName("expirationTime");

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Used)
                    .HasColumnName("used")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Claim>(entity =>
            {
                entity.ToTable("claim");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AdminFeeType)
                    .HasColumnName("adminFeeType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("FIXED");

                entity.Property(e => e.AdminFeeValue)
                    .HasColumnName("adminFeeValue")
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.CashReceived)
                    .HasColumnName("cashReceived")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ClaimNumber)
                    .HasColumnName("claimNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimStatus)
                    .IsRequired()
                    .HasColumnName("claimStatus")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreditMemo)
                    .HasColumnName("creditMemo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreditNote)
                    .HasColumnName("creditNote")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CurrencyRate)
                    .HasColumnName("currencyRate")
                    .HasColumnType("decimal(8,6)")
                    .HasDefaultValueSql("1.000000");

                entity.Property(e => e.CurrencyType)
                    .HasColumnName("currencyType")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyValue)
                    .HasColumnName("currencyValue")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.DateApproved).HasColumnName("dateApproved");

                entity.Property(e => e.DateReceived).HasColumnName("dateReceived");

                entity.Property(e => e.Dtype)
                    .HasColumnName("DTYPE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FocParts)
                    .HasColumnName("focParts")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Freight)
                    .HasColumnName("freight")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.HkInvoiceNo)
                    .HasColumnName("hkInvoiceNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategoryId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemId)
                    .HasColumnName("oemId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PartsReceived)
                    .HasColumnName("partsReceived")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RejectionReason)
                    .HasColumnName("rejectionReason")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RejectionSource)
                    .HasColumnName("rejectionSource")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceproviderId)
                    .HasColumnName("serviceprovider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.TicketNumber)
                    .HasColumnName("ticketNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Claimattachment>(entity =>
            {
                entity.ToTable("claimattachment");

                entity.HasIndex(e => e.ClaimId)
                    .HasName("fk_claimattachment_claim");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ClaimId)
                    .HasColumnName("claim_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileLocation)
                    .HasColumnName("fileLocation")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasColumnName("fileName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Claim)
                    .WithMany(p => p.Claimattachment)
                    .HasForeignKey(d => d.ClaimId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("claimattachment_ibfk_1");
            });

            modelBuilder.Entity<Claimlineitem>(entity =>
            {
                entity.ToTable("claimlineitem");

                entity.HasIndex(e => e.ClaimId)
                    .HasName("fk_claimlineitem_claim");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.BatchReference)
                    .HasColumnName("batchReference")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimId)
                    .HasColumnName("claim_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ClaimLineItemStatus)
                    .HasColumnName("claimLineItemStatus")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimPrice)
                    .HasColumnName("claimPrice")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.CostCategoryId)
                    .HasColumnName("costCategoryId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreditNumber)
                    .HasColumnName("creditNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNumber)
                    .HasColumnName("invoiceNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PartNumber)
                    .HasColumnName("partNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.ReceivedBy)
                    .HasColumnName("receivedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReceivedOn).HasColumnName("receivedOn");

                entity.Property(e => e.RecommendedPrice)
                    .HasColumnName("recommendedPrice")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.ReturnQuantity)
                    .HasColumnName("returnQuantity")
                    .HasColumnType("decimal(8,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ReturnRequired)
                    .HasColumnName("returnRequired")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Claim)
                    .WithMany(p => p.Claimlineitem)
                    .HasForeignKey(d => d.ClaimId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("claimlineitem_ibfk_1");
            });

            modelBuilder.Entity<Claimnote>(entity =>
            {
                entity.ToTable("claimnote");

                entity.HasIndex(e => e.ClaimId)
                    .HasName("fk_claimnote_claim");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ClaimId)
                    .HasColumnName("claim_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .IsUnicode(false);

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Claim)
                    .WithMany(p => p.Claimnote)
                    .HasForeignKey(d => d.ClaimId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("claimnote_ibfk_1");
            });

            modelBuilder.Entity<Commoncause>(entity =>
            {
                entity.ToTable("commoncause");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ParentCauseId)
                    .HasName("FKEF1CFD1E25E7B546");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("longtext");

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasColumnType("longtext");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ParentCauseId)
                    .HasColumnName("parentCause_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ParentCause)
                    .WithMany(p => p.InverseParentCause)
                    .HasForeignKey(d => d.ParentCauseId)
                    .HasConstraintName("FKEF1CFD1E25E7B546");
            });

            modelBuilder.Entity<Commonfault>(entity =>
            {
                entity.ToTable("commonfault");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("longtext");

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasColumnType("longtext");

                entity.Property(e => e.FaultDescriptionFile)
                    .HasColumnName("fault_description_file")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SelfFixAvailable)
                    .HasColumnName("self_fix_available")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Solution)
                    .HasColumnName("solution")
                    .HasColumnType("longtext");
            });

            modelBuilder.Entity<Configuredwarrantyrule>(entity =>
            {
                entity.ToTable("configuredwarrantyrule");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Configuredparameter)
                    .HasColumnName("configuredparameter")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RuledefinitionId)
                    .HasColumnName("ruledefinition_id")
                    .HasColumnType("mediumtext");
            });

            modelBuilder.Entity<Controlmeasure>(entity =>
            {
                entity.ToTable("controlmeasure");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.PotentialHazardId)
                    .HasName("FKCE575161FBB38A07");

                entity.HasIndex(e => e.TextId)
                    .HasName("FK_controlmeasure_text_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PotentialHazardId)
                    .HasColumnName("potentialHazard_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.TextId)
                    .HasColumnName("text_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.PotentialHazard)
                    .WithMany(p => p.Controlmeasure)
                    .HasForeignKey(d => d.PotentialHazardId)
                    .HasConstraintName("FKCE575161FBB38A07");

                entity.HasOne(d => d.Text)
                    .WithMany(p => p.Controlmeasure)
                    .HasForeignKey(d => d.TextId)
                    .HasConstraintName("FK_controlmeasure_text_Id");
            });

            modelBuilder.Entity<Costbreakdown>(entity =>
            {
                entity.ToTable("costbreakdown");

                entity.HasIndex(e => e.CurrencyId)
                    .HasName("FK_costbreakdown_currency_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceEventId)
                    .HasName("FK5C9F70D49C75D46D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("currency_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Exported)
                    .HasColumnName("exported")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ImportDate).HasColumnName("import_date");

                entity.Property(e => e.InvoicedDate).HasColumnName("invoicedDate");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NetTotal)
                    .HasColumnName("net_total")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.NetTotalHK)
                    .HasColumnName("net_total_h_k")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.NetTotalStore)
                    .HasColumnName("net_total_store")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Rejected)
                    .HasColumnName("rejected")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ServiceEventId)
                    .HasColumnName("serviceEvent_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SubmittedBy)
                    .HasColumnName("submittedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SubmittedDate)
                    .HasColumnName("submittedDate")
                    .HasColumnType("date");

                entity.Property(e => e.Total)
                    .HasColumnName("total")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.TotalHK)
                    .HasColumnName("total_h_k")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.TotalStore)
                    .HasColumnName("total_store")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Updated).HasColumnName("updated");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.Costbreakdown)
                    .HasForeignKey(d => d.CurrencyId)
                    .HasConstraintName("FK_costbreakdown_currency_Id");

                entity.HasOne(d => d.ServiceEvent)
                    .WithMany(p => p.Costbreakdown)
                    .HasForeignKey(d => d.ServiceEventId)
                    .HasConstraintName("FK5C9F70D49C75D46D");
            });

            modelBuilder.Entity<Costcategory>(entity =>
            {
                entity.ToTable("costcategory");

                entity.HasIndex(e => e.CategoryCode)
                    .HasName("category_code")
                    .IsUnique();

                entity.HasIndex(e => e.DescriptionId)
                    .HasName("FK_costcategory_description_Id");

                entity.HasIndex(e => e.LabelId)
                    .HasName("FK_costcategory_label_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CategoryCode)
                    .HasColumnName("category_code")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionId)
                    .HasColumnName("description_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DisplayOrder)
                    .HasColumnName("display_order")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("10");

                entity.Property(e => e.Enabled)
                    .HasColumnName("enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.FixedQuantity)
                    .HasColumnName("fixed_quantity")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LabelId)
                    .HasColumnName("label_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PriceInputMask)
                    .HasColumnName("price_input_mask")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PriceReferenceValue)
                    .HasColumnName("price_reference_value")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.PrimaryCategory)
                    .HasColumnName("primaryCategory")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.QuantityInputMask)
                    .HasColumnName("quantity_input_mask")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Description)
                    .WithMany(p => p.CostcategoryDescription)
                    .HasForeignKey(d => d.DescriptionId)
                    .HasConstraintName("FK_costcategory_description_Id");

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.CostcategoryLabel)
                    .HasForeignKey(d => d.LabelId)
                    .HasConstraintName("FK_costcategory_label_Id");
            });

            modelBuilder.Entity<Costcategorygroup>(entity =>
            {
                entity.ToTable("costcategorygroup");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Costcategorygrouprelationship>(entity =>
            {
                entity.HasKey(e => new { e.CostcategoryId, e.CostcategorygroupId });

                entity.ToTable("costcategorygrouprelationship");

                entity.HasIndex(e => e.CostcategoryId)
                    .HasName("costcategory_id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.CostcategorygroupId)
                    .HasName("fk_costcategorygroup");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.CostcategoryId)
                    .HasColumnName("costcategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostcategorygroupId)
                    .HasColumnName("costcategorygroup_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Costcategory)
                    .WithOne(p => p.Costcategorygrouprelationship)
                    .HasForeignKey<Costcategorygrouprelationship>(d => d.CostcategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_costcategory");

                entity.HasOne(d => d.Costcategorygroup)
                    .WithMany(p => p.Costcategorygrouprelationship)
                    .HasForeignKey(d => d.CostcategorygroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_costcategorygroup");
            });

            modelBuilder.Entity<Costlineitem>(entity =>
            {
                entity.ToTable("costlineitem");

                entity.HasIndex(e => e.ComponentMetaDataId)
                    .HasName("FKC956D0348FE55D67");

                entity.HasIndex(e => e.CostBreakDownId)
                    .HasName("FKC956D034A950E2A7");

                entity.HasIndex(e => e.CostCategoryId)
                    .HasName("FKC956D0345A1AEAED");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.VisitServiceEventId)
                    .HasName("fk_costlineitem_visitserviceevent_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ChargeToStore)
                    .HasColumnName("charge_to_store")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ComponentMetaDataId)
                    .HasColumnName("componentMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostBreakDownId)
                    .HasColumnName("costBreakDown_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostCategoryId)
                    .HasColumnName("costCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNumber)
                    .HasColumnName("invoice_number")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PartDescription)
                    .HasColumnName("part_description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartMetaDataId)
                    .HasColumnName("partMetaDataId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PartNumber)
                    .HasColumnName("partNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Tax)
                    .HasColumnName("tax")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Total)
                    .HasColumnName("total")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.VisitServiceEventId)
                    .HasColumnName("visitServiceEvent_Id")
                    .HasColumnType("bigint(20)");

                //entity.Property(e => e.WarrantyCover)
                //    .HasColumnName("warrantyCover")
                //    .HasColumnType("bit(1)")
                //    .HasDefaultValueSql("b'0'");

                entity.HasOne(d => d.ComponentMetaData)
                    .WithMany(p => p.Costlineitem)
                    .HasForeignKey(d => d.ComponentMetaDataId)
                    .HasConstraintName("FKC956D0348FE55D67");

                entity.HasOne(d => d.CostBreakDown)
                    .WithMany(p => p.Costlineitem)
                    .HasForeignKey(d => d.CostBreakDownId)
                    .HasConstraintName("FKC956D034A950E2A7");

                entity.HasOne(d => d.CostCategory)
                    .WithMany(p => p.Costlineitem)
                    .HasForeignKey(d => d.CostCategoryId)
                    .HasConstraintName("FKC956D0345A1AEAED");

                entity.HasOne(d => d.VisitServiceEvent)
                    .WithMany(p => p.Costlineitem)
                    .HasForeignKey(d => d.VisitServiceEventId)
                    .HasConstraintName("fk_costlineitem_visitserviceevent_id");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("country");

                entity.HasIndex(e => e.CurrencyId)
                    .HasName("FK_country_currency_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("currency_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CurrencySymbol)
                    .HasColumnName("currency_symbol")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DefaultTimeZone)
                    .HasColumnName("default_time_zone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InternalCountryCode)
                    .HasColumnName("internal_country_code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsoCountryCode)
                    .HasColumnName("iso_country_code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.Country)
                    .HasForeignKey(d => d.CurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_country_currency_Id");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("currency");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ExchangeRate)
                    .HasColumnName("exchangeRate")
                    .HasColumnType("decimal(8,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Symbol)
                    .IsRequired()
                    .HasColumnName("symbol")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<DashboardLog>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("dashboard_log");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecutionTime)
                    .HasColumnName("execution_time")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FinishTime)
                    .HasColumnName("finish_time")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Msg)
                    .HasColumnName("msg")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DataSynchronisationLog>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("data_synchronisation_log");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecutionTime)
                    .HasColumnName("execution_time")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FinishTime)
                    .HasColumnName("finish_time")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Msg)
                    .HasColumnName("msg")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DbCostByCategory>(entity =>
            {
                entity.ToTable("db_cost_by_category");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_cost_by_category");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_cost_by_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbCostByOem>(entity =>
            {
                entity.ToTable("db_cost_by_oem");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_cost_by_oem");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_cost_by_oem");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.GroupName)
                    .HasColumnName("groupName")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbCostByProvider>(entity =>
            {
                entity.ToTable("db_cost_by_provider");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_cost_by_provider");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_cost_by_provider");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbCostByStore>(entity =>
            {
                entity.ToTable("db_cost_by_store");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_cost_by_store");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_cost_by_store");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbCountByCategory>(entity =>
            {
                entity.ToTable("db_count_by_category");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_count_by_category");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_count_by_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.DbCountByCategory1)
                    .HasColumnName("db_count_by_category")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SrCount)
                    .HasColumnName("sr_count")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbPlannedMaintenanceSummary>(entity =>
            {
                entity.ToTable("db_planned_maintenance_summary");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_planned_maintenace_summary");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_planned_maintenance_summary");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Label)
                    .HasColumnName("label")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LabelDescription)
                    .HasColumnName("label_description")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SrCount)
                    .HasColumnName("sr_count")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbResponseTimes>(entity =>
            {
                entity.ToTable("db_response_times");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_response_times");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_response_times");

                entity.HasIndex(e => e.VisitId)
                    .HasName("unique_visit");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Eta).HasColumnName("eta");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ResponseType)
                    .HasColumnName("response_type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceEventId)
                    .HasColumnName("service_event_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SignInTime).HasColumnName("sign_in_time");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.VisitId)
                    .HasColumnName("visit_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceEventByType>(entity =>
            {
                entity.ToTable("db_service_event_by_type");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_event_by_type");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_event_by_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.EventCode)
                    .HasColumnName("event_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EventCount)
                    .HasColumnName("event_count")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EventDescription)
                    .HasColumnName("event_description")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceEventByTypeCombinePm>(entity =>
            {
                entity.ToTable("db_service_event_by_type_combine_pm");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_event_by_type");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_event_by_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.EventCode)
                    .HasColumnName("event_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EventCount)
                    .HasColumnName("event_count")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EventDescription)
                    .HasColumnName("event_description")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SrId)
                    .HasColumnName("sr_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestCost>(entity =>
            {
                entity.ToTable("db_service_request_cost");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_cost");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_cost");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostSr)
                    .HasColumnName("cost_sr")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestCost30d>(entity =>
            {
                entity.ToTable("db_service_request_cost_30d");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_cost_30d");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_cost_30d");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostSr)
                    .HasColumnName("cost_sr")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestCostYtd>(entity =>
            {
                entity.ToTable("db_service_request_cost_ytd");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_cost_ytd");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_cost_ytd");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostSr)
                    .HasColumnName("cost_sr")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestCount>(entity =>
            {
                entity.ToTable("db_service_request_count");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_count");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_count");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountSr)
                    .HasColumnName("count_sr")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestCount30d>(entity =>
            {
                entity.ToTable("db_service_request_count_30d");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_count_30d");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_count_30d");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountSr)
                    .HasColumnName("count_sr")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestCountYtd>(entity =>
            {
                entity.ToTable("db_service_request_count_ytd");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_count_ytd");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_count_ytd");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountSr)
                    .HasColumnName("count_sr")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PeriodInDays)
                    .HasColumnName("period_in_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestFirstTimeFix>(entity =>
            {
                entity.ToTable("db_service_request_first_time_fix");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_first_time_fix");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_first_time_fix");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountSr)
                    .HasColumnName("count_sr")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PeriodInDays)
                    .HasColumnName("period_in_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestOutcome>(entity =>
            {
                entity.ToTable("db_service_request_outcome");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_outcome");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_outcome");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountSr)
                    .HasColumnName("count_sr")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Resolution)
                    .HasColumnName("resolution")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbServiceRequestVisits>(entity =>
            {
                entity.ToTable("db_service_request_visits");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_service_request_visits");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_service_request_visits");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountVisits)
                    .HasColumnName("count_visits")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateOfOccurrence)
                    .HasColumnName("date_of_occurrence")
                    .HasColumnType("date");

                entity.Property(e => e.Label)
                    .HasColumnName("label")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SrId)
                    .HasColumnName("sr_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbTotalInitialArrivalTime>(entity =>
            {
                entity.ToTable("db_total_initial_arrival_time");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_total_initial_arrival_time");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_total_initial_arrival_time");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Minutes)
                    .HasColumnName("minutes")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbTotalInitialResponseTime>(entity =>
            {
                entity.ToTable("db_total_initial_response_time");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_total_initial_response_time");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_total_initial_response_time");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Minutes)
                    .HasColumnName("minutes")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbTotalWarranty>(entity =>
            {
                entity.ToTable("db_total_warranty");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_total_warranty");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_total_warranty");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountW)
                    .HasColumnName("count_w")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<DbTurnaroundInMinute>(entity =>
            {
                entity.ToTable("db_turnaround_in_minute");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("idx_service_provider_db_turnaround_in_minute");

                entity.HasIndex(e => e.StoreId)
                    .HasName("idx_store_db_turnaround_in_minute");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Minutes)
                    .HasColumnName("minutes")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PeriodInDays)
                    .HasColumnName("period_in_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SrId)
                    .HasColumnName("sr_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Defaultcost>(entity =>
            {
                entity.ToTable("defaultcost");

                entity.HasIndex(e => e.CostCategoryId)
                    .HasName("FK4956998E5A1AEAED");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK4956998E8963B647");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostCategoryId)
                    .HasColumnName("costCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DefaultItemCost)
                    .HasColumnName("default_item_cost")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.CostCategory)
                    .WithMany(p => p.Defaultcost)
                    .HasForeignKey(d => d.CostCategoryId)
                    .HasConstraintName("FK4956998E5A1AEAED");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Defaultcost)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK4956998E8963B647");
            });

            modelBuilder.Entity<Deferralreason>(entity =>
            {
                entity.ToTable("deferralreason");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Reason)
                    .HasColumnName("reason")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Deliverymethod>(entity =>
            {
                entity.ToTable("deliverymethod");

                entity.HasIndex(e => e.Code)
                    .HasName("code")
                    .IsUnique();

                entity.HasIndex(e => e.DescriptionId)
                    .HasName("fk_deliverymethod_description_id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionId)
                    .HasColumnName("description_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Description)
                    .WithMany(p => p.Deliverymethod)
                    .HasForeignKey(d => d.DescriptionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_deliverymethod_description_id");
            });

            modelBuilder.Entity<Engineer>(entity =>
            {
                entity.ToTable("engineer");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK705CE28F8963B647");

                entity.HasIndex(e => e.UserId)
                    .HasName("FK705CE28FD914A9CD");

                entity.HasIndex(e => new { e.ServiceProviderId, e.FirstName, e.LastName })
                    .HasName("idx_engineer_name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ContractorIdExpiryDate).HasColumnName("contractor_id_expiry_date");

                entity.Property(e => e.ContractorIdNumber)
                    .HasColumnName("contractor_id_number")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractorIdType)
                    .HasColumnName("contractor_id_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Enabled)
                    .HasColumnName("enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MobilePhoneNumber)
                    .HasColumnName("mobile_phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PassportExpiryWarningSent)
                    .HasColumnName("passportExpiryWarningSent")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Engineer)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK705CE28F8963B647");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Engineer)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK705CE28FD914A9CD");
            });

            modelBuilder.Entity<Eventtype>(entity =>
            {
                entity.ToTable("eventtype");

                entity.HasIndex(e => e.CodeId)
                    .HasName("FK_eventtype_code_Id");

                entity.HasIndex(e => e.DescriptionId)
                    .HasName("FK_eventtype_description_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CodeId)
                    .HasColumnName("code_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DescriptionId)
                    .HasColumnName("description_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Enabled)
                    .HasColumnName("enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.EventSequence)
                    .HasColumnName("event_sequence")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EventTypeServiceProviderId)
                    .HasColumnName("eventTypeServiceProvider_Id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PlannedMaintenance)
                    .HasColumnName("plannedMaintenance")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RequiredAssignmentInterval)
                    .HasColumnName("required_assignment_interval")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RequiredResponseTimeInterval)
                    .HasColumnName("required_response_time_interval")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ResolutionTime)
                    .HasColumnName("resolutionTime")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ResponseTimeAndDescriptionVisibility)
                    .HasColumnName("responseTimeAndDescriptionVisibility")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'1'");

                entity.Property(e => e.Summary)
                    .HasColumnName("summary")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.HasOne(d => d.Code)
                    .WithMany(p => p.EventtypeCode)
                    .HasForeignKey(d => d.CodeId)
                    .HasConstraintName("FK_eventtype_code_Id");

                entity.HasOne(d => d.Description)
                    .WithMany(p => p.EventtypeDescription)
                    .HasForeignKey(d => d.DescriptionId)
                    .HasConstraintName("FK_eventtype_description_Id");
            });

            modelBuilder.Entity<Eventtypechangelog>(entity =>
            {
                entity.ToTable("eventtypechangelog");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.NewEventTypeId)
                    .HasName("fk_eventtypechangelog_newEventType");

                entity.HasIndex(e => e.OldEventTypeId)
                    .HasName("fk_eventtypechangelog_oldEventType");

                entity.HasIndex(e => e.ServiceNoteId)
                    .HasName("fk_eventtypechangelog_serviceNote");

                entity.HasIndex(e => e.ServiceRequestId)
                    .HasName("fk_eventtypechangelog_serviceRequest");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NewEventTypeId)
                    .HasColumnName("newEventType_id")
                    .HasColumnType("bigint(255)");

                entity.Property(e => e.OldEventTypeId)
                    .HasColumnName("oldEventType_id")
                    .HasColumnType("bigint(255)");

                entity.Property(e => e.ServiceNoteId)
                    .HasColumnName("serviceNote_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasColumnName("user")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.NewEventType)
                    .WithMany(p => p.EventtypechangelogNewEventType)
                    .HasForeignKey(d => d.NewEventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("eventtypechangelog_ibfk_3");

                entity.HasOne(d => d.OldEventType)
                    .WithMany(p => p.EventtypechangelogOldEventType)
                    .HasForeignKey(d => d.OldEventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("eventtypechangelog_ibfk_4");

                entity.HasOne(d => d.ServiceNote)
                    .WithMany(p => p.Eventtypechangelog)
                    .HasForeignKey(d => d.ServiceNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("eventtypechangelog_ibfk_2");

                entity.HasOne(d => d.ServiceRequest)
                    .WithMany(p => p.Eventtypechangelog)
                    .HasForeignKey(d => d.ServiceRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("eventtypechangelog_ibfk_1");
            });

            modelBuilder.Entity<Eventtypegroup>(entity =>
            {
                entity.ToTable("eventtypegroup");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Eventtypegrouprelationship>(entity =>
            {
                entity.HasKey(e => new { e.EventtypeId, e.EventtypegroupId });

                entity.ToTable("eventtypegrouprelationship");

                entity.HasIndex(e => e.EventtypeId)
                    .HasName("eventtype_id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.EventtypegroupId)
                    .HasName("fk_eventtypegroup");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.EventtypeId)
                    .HasColumnName("eventtype_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EventtypegroupId)
                    .HasColumnName("eventtypegroup_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Eventtype)
                    .WithOne(p => p.Eventtypegrouprelationship)
                    .HasForeignKey<Eventtypegrouprelationship>(d => d.EventtypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_eventtype");

                entity.HasOne(d => d.Eventtypegroup)
                    .WithMany(p => p.Eventtypegrouprelationship)
                    .HasForeignKey(d => d.EventtypegroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_eventtypegroup");
            });

            modelBuilder.Entity<Eventtypeserviceprovider>(entity =>
            {
                entity.ToTable("eventtypeserviceprovider");

                entity.HasIndex(e => e.EventTypeId)
                    .HasName("fk_eventtypeserviceprovider_eventType_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("fk_eventtypeserviceprovider_serviceProvider_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DisabledDate).HasColumnName("disabledDate");

                entity.Property(e => e.Enabled)
                    .HasColumnName("enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("eventType_Id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RequiredAssignmentIntervalMins)
                    .HasColumnName("requiredAssignmentIntervalMins")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RequiredResponseTimeIntervalHrs)
                    .HasColumnName("requiredResponseTimeIntervalHrs")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ResolutionTime)
                    .HasColumnName("resolutionTime")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_Id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.Eventtypeserviceprovider)
                    .HasForeignKey(d => d.EventTypeId)
                    .HasConstraintName("eventtypeserviceprovider_ibfk_1");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Eventtypeserviceprovider)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("eventtypeserviceprovider_ibfk_2");
            });

            modelBuilder.Entity<Extendedcontractorapproval>(entity =>
            {
                entity.ToTable("extendedcontractorapproval");

                entity.HasIndex(e => e.EngineerId)
                    .HasName("FK1198F111853EC5CD");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FK1198F111BF9BE887");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CompletedTrainingAndOrientation)
                    .HasColumnName("completed_training_and_orientation")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ContractorSignature)
                    .HasColumnName("contractor_signature")
                    .HasColumnType("longblob");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.EngineerId)
                    .HasColumnName("engineer_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ManagerSignature)
                    .HasColumnName("manager_signature")
                    .HasColumnType("longblob");

                entity.Property(e => e.RecordedInRegularContractorList)
                    .HasColumnName("recorded_in_regular_contractor_list")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Engineer)
                    .WithMany(p => p.Extendedcontractorapproval)
                    .HasForeignKey(d => d.EngineerId)
                    .HasConstraintName("FK1198F111853EC5CD");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Extendedcontractorapproval)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FK1198F111BF9BE887");
            });

            modelBuilder.Entity<Faultappliesto>(entity =>
            {
                entity.ToTable("faultappliesto");

                entity.HasIndex(e => e.CommonFaultId)
                    .HasName("FK231E4045EB9CFEE7");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FK231E40455F04BDE7");

                entity.HasIndex(e => e.MachineMetaDataId)
                    .HasName("FK231E40458D2BB867");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CommonFaultId)
                    .HasColumnName("commonFault_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MachineMetaDataId)
                    .HasColumnName("machineMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.CommonFault)
                    .WithMany(p => p.Faultappliesto)
                    .HasForeignKey(d => d.CommonFaultId)
                    .HasConstraintName("FK231E4045EB9CFEE7");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Faultappliesto)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FK231E40455F04BDE7");

                entity.HasOne(d => d.MachineMetaData)
                    .WithMany(p => p.Faultappliesto)
                    .HasForeignKey(d => d.MachineMetaDataId)
                    .HasConstraintName("FK231E40458D2BB867");
            });

            modelBuilder.Entity<Governingdistrict>(entity =>
            {
                entity.ToTable("governingdistrict");

                entity.HasIndex(e => e.DistrictName)
                    .HasName("districtName")
                    .IsUnique();

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DistrictName)
                    .HasColumnName("districtName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("group");

                entity.HasIndex(e => e.GroupTypeId)
                    .HasName("fk_group_grouptype_id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Disabled)
                    .HasColumnName("disabled")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.EscalateDays)
                    .HasColumnName("escalateDays")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("7");

                entity.Property(e => e.GroupTypeId)
                    .HasColumnName("groupType_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ParentGroupId)
                    .HasColumnName("parentGroup_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ShouldEscalate)
                    .HasColumnName("shouldEscalate")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.GroupType)
                    .WithMany(p => p.Group)
                    .HasForeignKey(d => d.GroupTypeId)
                    .HasConstraintName("group_ibfk_3");
            });

            modelBuilder.Entity<Groupstores>(entity =>
            {
                entity.ToTable("groupstores");

                entity.HasIndex(e => e.GroupId)
                    .HasName("fk_groupstores_group_id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.StoreId)
                    .HasName("fk_groupstores_store_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Groupstores)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("groupstores_ibfk_2");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Groupstores)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("groupstores_ibfk_1");
            });

            modelBuilder.Entity<Grouptype>(entity =>
            {
                entity.ToTable("grouptype");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Level)
                    .HasColumnName("level")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Groupusers>(entity =>
            {
                entity.ToTable("groupusers");

                entity.HasIndex(e => e.GroupId)
                    .HasName("fk_groupusers_group_id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.UserId)
                    .HasName("fk_groupusers_user_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Groupusers)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("groupusers_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Groupusers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("groupusers_ibfk_1");
            });

            modelBuilder.Entity<Hazardassessment>(entity =>
            {
                entity.ToTable("hazardassessment");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.PotentialHazardId)
                    .HasName("FKC17C094FBB38A07");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ControlMeasureOther)
                    .HasColumnName("control_measure_other")
                    .HasColumnType("longtext");

                entity.Property(e => e.DescriptionOther)
                    .HasColumnName("description_other")
                    .HasColumnType("longtext");

                entity.Property(e => e.HazardRatingBefore)
                    .HasColumnName("hazard_rating_before")
                    .HasColumnType("int(11)");

                entity.Property(e => e.HazardRatingPast)
                    .HasColumnName("hazard_rating_past")
                    .HasColumnType("int(11)");

                entity.Property(e => e.HazardVisitGroup)
                    .HasColumnName("hazardVisitGroup")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PotentialHazardId)
                    .HasColumnName("potentialHazard_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.PotentialHazard)
                    .WithMany(p => p.Hazardassessment)
                    .HasForeignKey(d => d.PotentialHazardId)
                    .HasConstraintName("FKC17C094FBB38A07");
            });

            modelBuilder.Entity<HazardassessmentControlmeasure>(entity =>
            {
                entity.HasKey(e => new { e.HazardAssessmentsId, e.ControlMeasuresId });

                entity.ToTable("hazardassessment_controlmeasure");

                entity.HasIndex(e => e.ControlMeasuresId)
                    .HasName("FK5888446C7F784EBC");

                entity.HasIndex(e => e.HazardAssessmentsId)
                    .HasName("FK5888446C81E06A2");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.HazardAssessmentsId)
                    .HasColumnName("hazardAssessments_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ControlMeasuresId)
                    .HasColumnName("controlMeasures_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.ControlMeasures)
                    .WithMany(p => p.HazardassessmentControlmeasure)
                    .HasForeignKey(d => d.ControlMeasuresId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK5888446C7F784EBC");

                entity.HasOne(d => d.HazardAssessments)
                    .WithMany(p => p.HazardassessmentControlmeasure)
                    .HasForeignKey(d => d.HazardAssessmentsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK5888446C81E06A2");
            });

            modelBuilder.Entity<HazardassessmentVisit>(entity =>
            {
                entity.HasKey(e => new { e.HazardAssessmentsId, e.VisitsId });

                entity.ToTable("hazardassessment_visit");

                entity.HasIndex(e => e.HazardAssessmentsId)
                    .HasName("hazardAssessmentIndex");

                entity.HasIndex(e => e.VisitsId)
                    .HasName("visitIndex");

                entity.Property(e => e.HazardAssessmentsId)
                    .HasColumnName("hazardAssessments_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.VisitsId)
                    .HasColumnName("visits_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.HazardAssessments)
                    .WithMany(p => p.HazardassessmentVisit)
                    .HasForeignKey(d => d.HazardAssessmentsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("hazardAssessmentForeignKey");

                entity.HasOne(d => d.Visits)
                    .WithMany(p => p.HazardassessmentVisit)
                    .HasForeignKey(d => d.VisitsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("visitForeignKey");
            });

            modelBuilder.Entity<Kit>(entity =>
            {
                entity.ToTable("kit");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.PrimaryComponentId)
                    .HasName("FK126B68ED808F8");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PrimaryComponentId)
                    .HasColumnName("primaryComponent_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.PrimaryComponent)
                    .WithMany(p => p.Kit)
                    .HasForeignKey(d => d.PrimaryComponentId)
                    .HasConstraintName("FK126B68ED808F8");
            });

            modelBuilder.Entity<KitPart>(entity =>
            {
                entity.ToTable("kit_part");

                entity.HasIndex(e => e.KitId)
                    .HasName("FKECB92BFCE2FDFA7");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.PartId)
                    .HasName("FKECB92BFC7C36BB40");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.KitId)
                    .HasColumnName("kit_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PartId)
                    .HasColumnName("part_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Kit)
                    .WithMany(p => p.KitPart)
                    .HasForeignKey(d => d.KitId)
                    .HasConstraintName("FKECB92BFCE2FDFA7");

                entity.HasOne(d => d.Part)
                    .WithMany(p => p.KitPart)
                    .HasForeignKey(d => d.PartId)
                    .HasConstraintName("FKECB92BFC7C36BB40");
            });

            modelBuilder.Entity<Kpi>(entity =>
            {
                entity.ToTable("kpi");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("fk_kpi_serviceprovider");

                entity.HasIndex(e => e.ServiceRequestId)
                    .HasName("fk_kpi_serviceRequest");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ActionTime).HasColumnName("actionTime");

                entity.Property(e => e.CreatedTime).HasColumnName("createdTime");

                entity.Property(e => e.ExpectedTime).HasColumnName("expectedTime");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Kpi)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("kpi_ibfk_2");

                entity.HasOne(d => d.ServiceRequest)
                    .WithMany(p => p.Kpi)
                    .HasForeignKey(d => d.ServiceRequestId)
                    .HasConstraintName("kpi_ibfk_1");
            });

            modelBuilder.Entity<Machine>(entity =>
            {
                entity.ToTable("machine");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineMetaDataId)
                    .HasName("machine_machinemetadata");

                entity.HasIndex(e => e.PlannedMaintenanceCycleId)
                    .HasName("FK9469DC274981D44");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK9469DC278963B647");

                entity.HasIndex(e => e.StationId)
                    .HasName("FK9469DC27AB2BC67");

                entity.HasIndex(e => e.StoreId)
                    .HasName("machine_storeid");

                entity.HasIndex(e => e.Uid)
                    .HasName("uid")
                    .IsUnique();

                entity.HasIndex(e => new { e.SerialNumber, e.MachineMetaDataId, e.StoreId })
                    .HasName("UNIQUE_MACHINE")
                    .IsUnique();

                entity.HasIndex(e => new { e.Source, e.SourceId, e.SerialNumber })
                    .HasName("unique_imported_machine")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.BookValue)
                    .HasColumnName("book_value")
                    .HasColumnType("decimal(8,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.CompletedPreviousCycle).HasColumnName("completedPreviousCycle");

                entity.Property(e => e.ContractNumber)
                    .HasColumnName("contract_number")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CycleStart).HasColumnName("cycleStart");

                entity.Property(e => e.Decommissioned)
                    .HasColumnName("decommissioned")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ExpectedNextCycle).HasColumnName("expectedNextCycle");

                entity.Property(e => e.ExpiryDate).HasColumnName("expiry_date");

                entity.Property(e => e.ExpiryNotificationSent)
                    .HasColumnName("expiryNotificationSent")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.FriendlyName)
                    .HasColumnName("friendly_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ImportDate).HasColumnName("import_date");

                entity.Property(e => e.InstallationDate).HasColumnName("installation_date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineMetaDataId)
                    .HasColumnName("machineMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MachineStatus)
                    .HasColumnName("machine_status")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.OptOut)
                    .HasColumnName("opt_out")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.OptOutReason)
                    .HasColumnName("opt_out_reason")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OriginalStoreCode)
                    .HasColumnName("original_store_code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PlannedMaintenanceCycleId)
                    .HasColumnName("plannedMaintenanceCycle_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PurchasePrice)
                    .HasColumnName("purchase_price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.SerialNumber)
                    .HasColumnName("serial_number")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.SourceId)
                    .HasColumnName("source_id")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.StationId)
                    .HasColumnName("station_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Uid)
                    .HasColumnName("uid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UnderWarranty)
                    .HasColumnName("under_warranty")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.WarrantyPeriod)
                    .HasColumnName("warranty_period")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.WarrantyStartDate).HasColumnName("warranty_start_date");

                entity.HasOne(d => d.MachineMetaData)
                    .WithMany(p => p.Machine)
                    .HasForeignKey(d => d.MachineMetaDataId)
                    .HasConstraintName("FK9469DC278D2BB867");

                entity.HasOne(d => d.PlannedMaintenanceCycle)
                    .WithMany(p => p.Machine)
                    .HasForeignKey(d => d.PlannedMaintenanceCycleId)
                    .HasConstraintName("FK9469DC274981D44");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Machine)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK9469DC278963B647");

                entity.HasOne(d => d.Station)
                    .WithMany(p => p.Machine)
                    .HasForeignKey(d => d.StationId)
                    .HasConstraintName("FK9469DC27AB2BC67");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Machine)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FK9469DC27BF9BE887");
            });

            modelBuilder.Entity<Machinecategory>(entity =>
            {
                entity.ToTable("machinecategory");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.NameId)
                    .HasName("FK_machinecategory_name_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CategoryCode)
                    .HasColumnName("category_code")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.CommissioningPeriod)
                    .HasColumnName("commissioning_period")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("14");

                entity.Property(e => e.ExternalReferenceNumber)
                    .HasColumnName("external_reference_number")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NameId)
                    .HasColumnName("name_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Name)
                    .WithMany(p => p.Machinecategory)
                    .HasForeignKey(d => d.NameId)
                    .HasConstraintName("FK_machinecategory_name_Id");
            });

            modelBuilder.Entity<Machinecheck>(entity =>
            {
                entity.ToTable("machinecheck");

                entity.HasIndex(e => e.MachineId)
                    .HasName("FK_machineCheckMachine_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CheckType)
                    .HasColumnName("check_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnName("createdOn");

                entity.Property(e => e.MachineCondition)
                    .HasColumnName("machine_condition")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Machinecheck)
                    .HasForeignKey(d => d.MachineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_machineCheckMachine_id");
            });

            modelBuilder.Entity<Machinefile>(entity =>
            {
                entity.ToTable("machinefile");

                entity.HasIndex(e => e.MachineId)
                    .HasName("FK_machine_id");

                entity.HasIndex(e => e.UploadedById)
                    .HasName("FK_uploadedBy_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.CreatedOn).HasColumnName("createdOn");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Uploaded)
                    .HasColumnName("uploaded")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UploadedById)
                    .HasColumnName("uploadedBy_id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.UploadedOn).HasColumnName("uploadedOn");

                entity.Property(e => e.Uri)
                    .IsRequired()
                    .HasColumnName("uri")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Machinefile)
                    .HasForeignKey(d => d.MachineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_machine_id");

                entity.HasOne(d => d.UploadedBy)
                    .WithMany(p => p.Machinefile)
                    .HasForeignKey(d => d.UploadedById)
                    .HasConstraintName("FK_uploadedBy_id");
            });

            modelBuilder.Entity<Machinetocomponentmetadata>(entity =>
            {
                entity.ToTable("machinetocomponentmetadata");

                entity.HasIndex(e => e.ComponentPartmetadataId)
                    .HasName("fk_machinetocomponentmetadata_component_partmetadata_id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => new { e.MachinePartmetadataId, e.ComponentPartmetadataId })
                    .HasName("machinetocomponentunique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ComponentPartmetadataId)
                    .HasColumnName("component_partmetadata_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CoverPeriodMonths)
                    .HasColumnName("cover_period_months")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachinePartmetadataId)
                    .HasColumnName("machine_partmetadata_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ComponentPartmetadata)
                    .WithMany(p => p.MachinetocomponentmetadataComponentPartmetadata)
                    .HasForeignKey(d => d.ComponentPartmetadataId)
                    .HasConstraintName("machinetocomponentmetadata_ibfk_2");

                entity.HasOne(d => d.MachinePartmetadata)
                    .WithMany(p => p.MachinetocomponentmetadataMachinePartmetadata)
                    .HasForeignKey(d => d.MachinePartmetadataId)
                    .HasConstraintName("machinetocomponentmetadata_ibfk_1");
            });

            modelBuilder.Entity<MachineWarrantySynchronisationLog>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("machine_warranty_synchronisation_log");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExecutionTime)
                    .HasColumnName("execution_time")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FinishTime)
                    .HasColumnName("finish_time")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Msg)
                    .HasColumnName("msg")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Modelproperty>(entity =>
            {
                entity.ToTable("modelproperty");

                entity.HasIndex(e => e.LabelId)
                    .HasName("FK_modelproperty_label_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineMetaDataId)
                    .HasName("FKF34633FE8D2BB867");

                entity.HasIndex(e => e.NameId)
                    .HasName("FK_modelproperty_name_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.LabelId)
                    .HasColumnName("label_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineMetaDataId)
                    .HasColumnName("machineMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.NameId)
                    .HasColumnName("name_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.ModelpropertyLabel)
                    .HasForeignKey(d => d.LabelId)
                    .HasConstraintName("FK_modelproperty_label_Id");

                entity.HasOne(d => d.MachineMetaData)
                    .WithMany(p => p.Modelproperty)
                    .HasForeignKey(d => d.MachineMetaDataId)
                    .HasConstraintName("FKF34633FE8D2BB867");

                entity.HasOne(d => d.Name)
                    .WithMany(p => p.ModelpropertyName)
                    .HasForeignKey(d => d.NameId)
                    .HasConstraintName("FK_modelproperty_name_Id");
            });

            modelBuilder.Entity<Modelpropertyvalue>(entity =>
            {
                entity.ToTable("modelpropertyvalue");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ModelPropertyId)
                    .HasName("FKE5FA3C13C1E35E87");

                entity.HasIndex(e => e.ServiceEventId)
                    .HasName("FKE5FA3C139C75D46D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LongPropertyValue)
                    .HasColumnName("longPropertyValue")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ModelPropertyId)
                    .HasColumnName("modelProperty_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceEventId)
                    .HasColumnName("serviceEvent_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ModelProperty)
                    .WithMany(p => p.Modelpropertyvalue)
                    .HasForeignKey(d => d.ModelPropertyId)
                    .HasConstraintName("FKE5FA3C13C1E35E87");

                entity.HasOne(d => d.ServiceEvent)
                    .WithMany(p => p.Modelpropertyvalue)
                    .HasForeignKey(d => d.ServiceEventId)
                    .HasConstraintName("FKE5FA3C139C75D46D");
            });

            modelBuilder.Entity<Mycalendar>(entity =>
            {
                entity.ToTable("mycalendar");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK87A8DD2A8963B647");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Mycalendar)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK87A8DD2A8963B647");
            });

            modelBuilder.Entity<Mycalendarevent>(entity =>
            {
                entity.ToTable("mycalendarevent");

                entity.HasIndex(e => e.CalendarId)
                    .HasName("FK6C084F9029859479");

                entity.HasIndex(e => e.EngineerId)
                    .HasName("FK6C084F90853EC5CD");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.RegionId)
                    .HasName("FK6C084F9060F8697B");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CalendarId)
                    .HasColumnName("calendar_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EngineerId)
                    .HasColumnName("engineer_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.FinishTime).HasColumnName("finish_time");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StartTime).HasColumnName("start_time");

                entity.HasOne(d => d.Calendar)
                    .WithMany(p => p.Mycalendarevent)
                    .HasForeignKey(d => d.CalendarId)
                    .HasConstraintName("FK6C084F9029859479");

                entity.HasOne(d => d.Engineer)
                    .WithMany(p => p.Mycalendarevent)
                    .HasForeignKey(d => d.EngineerId)
                    .HasConstraintName("FK6C084F90853EC5CD");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Mycalendarevent)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK6C084F9060F8697B");
            });

            modelBuilder.Entity<Notificationmessage>(entity =>
            {
                entity.ToTable("notificationmessage");

                entity.HasIndex(e => e.BodyId)
                    .HasName("FK_notificationmessage_body_Id");

                entity.HasIndex(e => e.SubjectId)
                    .HasName("FK_notificationmessage_subject_Id");

                entity.HasIndex(e => new { e.Name, e.NotificationMedium })
                    .HasName("nametonotificationMediumunique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.BodyId)
                    .HasColumnName("body_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationMedium)
                    .IsRequired()
                    .HasColumnName("notificationMedium")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("EMAIL");

                entity.Property(e => e.SubjectId)
                    .HasColumnName("subject_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Body)
                    .WithMany(p => p.NotificationmessageBody)
                    .HasForeignKey(d => d.BodyId)
                    .HasConstraintName("FK_notificationmessage_body_Id");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.NotificationmessageSubject)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK_notificationmessage_subject_Id");
            });

            modelBuilder.Entity<Notificationsubscription>(entity =>
            {
                entity.ToTable("notificationsubscription");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FKE82551C88963B647");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FKE82551C8BF9BE887");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.NotificationType)
                    .HasColumnName("notification_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OtherNotificationStatus)
                    .HasColumnName("other_notification_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceRequestStatus)
                    .HasColumnName("service_request_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Notificationsubscription)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FKE82551C88963B647");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Notificationsubscription)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FKE82551C8BF9BE887");
            });

            modelBuilder.Entity<Oemdetails>(entity =>
            {
                entity.ToTable("oemdetails");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.OemNumber)
                    .HasName("oem_number")
                    .IsUnique();

                entity.HasIndex(e => e.WarrantySupportId)
                    .HasName("FKDC9B590BE471AEE7");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CustomerCategory)
                    .HasColumnName("customer_category")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.OemNumber)
                    .HasColumnName("oem_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WarrantySupportId)
                    .HasColumnName("warrantySupport_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.WarrantySupport)
                    .WithMany(p => p.Oemdetails)
                    .HasForeignKey(d => d.WarrantySupportId)
                    .HasConstraintName("FKDC9B590BE471AEE7");
            });

            modelBuilder.Entity<Oemgroup>(entity =>
            {
                entity.ToTable("oemgroup");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Oemgroupmember>(entity =>
            {
                entity.ToTable("oemgroupmember");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.OemDetailsId)
                    .HasName("FK995F2C421EEB76D");

                entity.HasIndex(e => e.OemGroupId)
                    .HasName("FK995F2C423B80470D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OemDetailsId)
                    .HasColumnName("oemDetails_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemGroupId)
                    .HasColumnName("oemGroup_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.OemDetails)
                    .WithMany(p => p.Oemgroupmember)
                    .HasForeignKey(d => d.OemDetailsId)
                    .HasConstraintName("FK995F2C421EEB76D");

                entity.HasOne(d => d.OemGroup)
                    .WithMany(p => p.Oemgroupmember)
                    .HasForeignKey(d => d.OemGroupId)
                    .HasConstraintName("FK995F2C423B80470D");
            });

            modelBuilder.Entity<Partmetadata>(entity =>
            {
                entity.ToTable("partmetadata");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FKB2DB68825F04BDE7");

                entity.HasIndex(e => e.OemDetailsId)
                    .HasName("FKB2DB68821EEB76D");

                entity.HasIndex(e => e.PartNumber)
                    .HasName("part_number")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DepreciationTerm)
                    .HasColumnName("depreciationTerm")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("longtext");

                entity.Property(e => e.Dtype)
                    .IsRequired()
                    .HasColumnName("DTYPE")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.EtechDescription)
                    .HasColumnName("etech_description")
                    .HasColumnType("longtext");

                entity.Property(e => e.InternalDescription)
                    .HasColumnName("internal_description")
                    .HasColumnType("longtext");

                entity.Property(e => e.LandedPrice)
                    .HasColumnName("landedPrice")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemDetailsId)
                    .HasColumnName("oemDetails_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemPartCode)
                    .HasColumnName("oem_part_code")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PartClassification)
                    .HasColumnName("part_classification")
                    .HasColumnType("char(1)");

                entity.Property(e => e.PartNumber)
                    .HasColumnName("part_number")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.RecommendedPrice)
                    .HasColumnName("recommended_price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.WarrantyExcluded)
                    .HasColumnName("warrantyExcluded")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.WarrantyMonths)
                    .HasColumnName("warrantyMonths")
                    .HasColumnType("smallint(6)");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Partmetadata)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FKB2DB68825F04BDE7");

                entity.HasOne(d => d.OemDetails)
                    .WithMany(p => p.Partmetadata)
                    .HasForeignKey(d => d.OemDetailsId)
                    .HasConstraintName("FKB2DB68821EEB76D");
            });

            modelBuilder.Entity<Partnumbermapping>(entity =>
            {
                entity.ToTable("partnumbermapping");

                entity.HasIndex(e => e.ComponentMetaDataId)
                    .HasName("FK79938FF28FE55D67");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK79938FF28963B647");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ComponentMetaDataId)
                    .HasColumnName("componentMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("longtext");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SpPartCode)
                    .HasColumnName("sp_part_code")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.HasOne(d => d.ComponentMetaData)
                    .WithMany(p => p.Partnumbermapping)
                    .HasForeignKey(d => d.ComponentMetaDataId)
                    .HasConstraintName("FK79938FF28FE55D67");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Partnumbermapping)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK79938FF28963B647");
            });

            modelBuilder.Entity<Pickslip>(entity =>
            {
                entity.ToTable("pickslip");

                entity.HasIndex(e => e.ClaimId)
                    .HasName("fk_pickslip_claim");

                entity.HasIndex(e => e.DeliverymethodId)
                    .HasName("fk_pickslip_deliverymethod");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AddressType)
                    .HasColumnName("address_type")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.AddressTypeIdentifier)
                    .HasColumnName("address_type_identifier")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.AllocateStock)
                    .HasColumnName("allocateStock")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.ChargeTo)
                    .HasColumnName("chargeTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimId)
                    .HasColumnName("claim_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreditNote)
                    .HasColumnName("creditNote")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCode)
                    .HasColumnName("customerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DeliverTo)
                    .HasColumnName("deliverTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DeliveryName)
                    .HasColumnName("deliveryName")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.DeliveryNote)
                    .HasColumnName("deliveryNote")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DeliverymethodId)
                    .HasColumnName("deliverymethod_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Freight)
                    .HasColumnName("freight")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MajorMunicipality)
                    .HasColumnName("major_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MinorMunicipality)
                    .HasColumnName("minor_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.OriginOfOrder)
                    .HasColumnName("originOfOrder")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.PostalArea)
                    .HasColumnName("postal_area")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Shipped)
                    .HasColumnName("shipped")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ShippedItems)
                    .HasColumnName("shippedItems")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StreetDirection)
                    .HasColumnName("street_direction")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.StreetName)
                    .HasColumnName("street_name")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.StreetNumber)
                    .HasColumnName("street_number")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.WarehouseReference)
                    .HasColumnName("warehouseReference")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Claim)
                    .WithMany(p => p.Pickslip)
                    .HasForeignKey(d => d.ClaimId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pickslip_ibfk_1");

                entity.HasOne(d => d.Deliverymethod)
                    .WithMany(p => p.Pickslip)
                    .HasForeignKey(d => d.DeliverymethodId)
                    .HasConstraintName("fk_pickslip_deliverymethod");
            });

            modelBuilder.Entity<Pickslipitem>(entity =>
            {
                entity.ToTable("pickslipitem");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.PickslipId)
                    .HasName("fk_pickslipitem_pickslip");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FreeOfCharge)
                    .HasColumnName("freeOfCharge")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PartNumber)
                    .HasColumnName("partNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PickslipId)
                    .HasColumnName("pickslip_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.ShippedQuantity)
                    .HasColumnName("shippedQuantity")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Pickslip)
                    .WithMany(p => p.Pickslipitem)
                    .HasForeignKey(d => d.PickslipId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pickslipitem_ibfk_1");
            });

            modelBuilder.Entity<Plannedmaintenancecycle>(entity =>
            {
                entity.ToTable("plannedmaintenancecycle");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ModelId)
                    .HasName("FKDA129F573415806D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CycleName)
                    .HasColumnName("cycle_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DefaultCycle)
                    .HasColumnName("default_cycle")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ModelId)
                    .HasColumnName("model_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Plannedmaintenancecycle)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FKDA129F573415806D");
            });

            modelBuilder.Entity<Plannedmaintenanceoverride>(entity =>
            {
                entity.ToTable("plannedmaintenanceoverride");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.RegionId)
                    .HasName("FKA0BDB3BB2A7B96D0");

                entity.HasIndex(e => e.StepId)
                    .HasName("FKA0BDB3BBE5A233B5");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FKA0BDB3BBBF9BE887");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Days)
                    .HasColumnName("days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StepId)
                    .HasColumnName("step_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Plannedmaintenanceoverride)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FKA0BDB3BB2A7B96D0");

                entity.HasOne(d => d.Step)
                    .WithMany(p => p.Plannedmaintenanceoverride)
                    .HasForeignKey(d => d.StepId)
                    .HasConstraintName("FKA0BDB3BBE5A233B5");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Plannedmaintenanceoverride)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FKA0BDB3BBBF9BE887");
            });

            modelBuilder.Entity<Plannedmaintenancestep>(entity =>
            {
                entity.ToTable("plannedmaintenancestep");

                entity.HasIndex(e => e.CategoryId)
                    .HasName("FKF68BEE3B36A3A48E");

                entity.HasIndex(e => e.KitId)
                    .HasName("FKF68BEE3BE2FDFA7");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ModelId)
                    .HasName("FKF68BEE3B983164D4");

                entity.HasIndex(e => e.PmCycleId)
                    .HasName("FKF68BEE3B981A0E72");

                entity.HasIndex(e => e.PmTypeId)
                    .HasName("FKF68BEE3B3C77E058");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Days)
                    .HasColumnName("days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.KitId)
                    .HasColumnName("kit_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ModelId)
                    .HasColumnName("model_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Overrideable)
                    .HasColumnName("overrideable")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.PmCycleId)
                    .HasColumnName("pmCycle_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PmTypeId)
                    .HasColumnName("pmType_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.QuestionnaireId)
                    .HasColumnName("questionnaireId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RequiresCert)
                    .HasColumnName("requiresCert")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RequiresSample)
                    .HasColumnName("requires_sample")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.SampleRequiresApproval)
                    .HasColumnName("sample_requires_approval")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.StandardPmLabourTimeMinutes)
                    .HasColumnName("standard_pm_labour_time_minutes")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StepOrder)
                    .HasColumnName("step_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitUsage)
                    .HasColumnName("unit_usage")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.UsagePrediction)
                    .HasColumnName("usage_prediction")
                    .HasColumnType("bit(1)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Plannedmaintenancestep)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FKF68BEE3B36A3A48E");

                entity.HasOne(d => d.Kit)
                    .WithMany(p => p.Plannedmaintenancestep)
                    .HasForeignKey(d => d.KitId)
                    .HasConstraintName("FKF68BEE3BE2FDFA7");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Plannedmaintenancestep)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FKF68BEE3B983164D4");

                entity.HasOne(d => d.PmCycle)
                    .WithMany(p => p.Plannedmaintenancestep)
                    .HasForeignKey(d => d.PmCycleId)
                    .HasConstraintName("FKF68BEE3B981A0E72");

                entity.HasOne(d => d.PmType)
                    .WithMany(p => p.Plannedmaintenancestep)
                    .HasForeignKey(d => d.PmTypeId)
                    .HasConstraintName("FKF68BEE3B3C77E058");
            });

            modelBuilder.Entity<Plannedmaintenancetype>(entity =>
            {
                entity.ToTable("plannedmaintenancetype");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TypeName)
                    .HasColumnName("type_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Potentialhazard>(entity =>
            {
                entity.ToTable("potentialhazard");

                entity.HasIndex(e => e.DescriptionId)
                    .HasName("FK_potentialhazard_description_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.NameId)
                    .HasName("FK_potentialhazard_name_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DescriptionId)
                    .HasColumnName("description_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NameId)
                    .HasColumnName("name_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Description)
                    .WithMany(p => p.PotentialhazardDescription)
                    .HasForeignKey(d => d.DescriptionId)
                    .HasConstraintName("FK_potentialhazard_description_Id");

                entity.HasOne(d => d.Name)
                    .WithMany(p => p.PotentialhazardName)
                    .HasForeignKey(d => d.NameId)
                    .HasConstraintName("FK_potentialhazard_name_Id");
            });

            modelBuilder.Entity<Providershortlistentry>(entity =>
            {
                entity.ToTable("providershortlistentry");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ProviderAssignmentState)
                    .HasName("provider_assignment_state");

                entity.HasIndex(e => e.RejectionReasonId)
                    .HasName("FKA862C5C998CB60E7");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FKA862C5C98963B647");

                entity.HasIndex(e => e.ServiceRequestId)
                    .HasName("FKA862C5C9B13C638D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Preference)
                    .HasColumnName("preference")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.ProviderAssignmentState)
                    .HasColumnName("provider_assignment_state")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RejectionReason)
                    .HasColumnName("rejection_reason")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RejectionReasonId)
                    .HasColumnName("rejectionReason_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.HasOne(d => d.RejectionReasonNavigation)
                    .WithMany(p => p.Providershortlistentry)
                    .HasForeignKey(d => d.RejectionReasonId)
                    .HasConstraintName("FKA862C5C998CB60E7");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Providershortlistentry)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FKA862C5C98963B647");

                entity.HasOne(d => d.ServiceRequest)
                    .WithMany(p => p.Providershortlistentry)
                    .HasForeignKey(d => d.ServiceRequestId)
                    .HasConstraintName("FKA862C5C9B13C638D");
            });

            modelBuilder.Entity<QrtzBlobTriggers>(entity =>
            {
                entity.HasKey(e => new { e.TriggerName, e.TriggerGroup });

                entity.ToTable("qrtz_blob_triggers");

                entity.Property(e => e.TriggerName)
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BlobData)
                    .HasColumnName("BLOB_DATA")
                    .HasColumnType("blob");

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzCalendars>(entity =>
            {
                entity.HasKey(e => e.CalendarName);

                entity.ToTable("qrtz_calendars");

                entity.Property(e => e.CalendarName)
                    .HasColumnName("CALENDAR_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Calendar)
                    .IsRequired()
                    .HasColumnName("CALENDAR")
                    .HasColumnType("blob");

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzCronTriggers>(entity =>
            {
                entity.HasKey(e => new { e.TriggerName, e.TriggerGroup });

                entity.ToTable("qrtz_cron_triggers");

                entity.Property(e => e.TriggerName)
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CronExpression)
                    .IsRequired()
                    .HasColumnName("CRON_EXPRESSION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");

                entity.Property(e => e.TimeZoneId)
                    .HasColumnName("TIME_ZONE_ID")
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QrtzFiredTriggers>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.ToTable("qrtz_fired_triggers");

                entity.Property(e => e.EntryId)
                    .HasColumnName("ENTRY_ID")
                    .HasMaxLength(95)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.FiredTime)
                    .HasColumnName("FIRED_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.InstanceName)
                    .IsRequired()
                    .HasColumnName("INSTANCE_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IsNonconcurrent)
                    .HasColumnName("IS_NONCONCURRENT")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUpdateData)
                    .HasColumnName("IS_UPDATE_DATA")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.JobGroup)
                    .HasColumnName("JOB_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.JobName)
                    .HasColumnName("JOB_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Priority)
                    .HasColumnName("PRIORITY")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RequestsRecovery)
                    .HasColumnName("REQUESTS_RECOVERY")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");

                entity.Property(e => e.SchedTime)
                    .HasColumnName("SCHED_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasColumnName("STATE")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .IsRequired()
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerName)
                    .IsRequired()
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QrtzJobDetails>(entity =>
            {
                entity.HasKey(e => new { e.JobName, e.JobGroup });

                entity.ToTable("qrtz_job_details");

                entity.Property(e => e.JobName)
                    .HasColumnName("JOB_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.JobGroup)
                    .HasColumnName("JOB_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IsDurable)
                    .IsRequired()
                    .HasColumnName("IS_DURABLE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsNonconcurrent)
                    .HasColumnName("IS_NONCONCURRENT")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUpdateData)
                    .HasColumnName("IS_UPDATE_DATA")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.JobClassName)
                    .IsRequired()
                    .HasColumnName("JOB_CLASS_NAME")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.JobData)
                    .HasColumnName("JOB_DATA")
                    .HasColumnType("blob");

                entity.Property(e => e.RequestsRecovery)
                    .IsRequired()
                    .HasColumnName("REQUESTS_RECOVERY")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzJobListeners>(entity =>
            {
                entity.HasKey(e => new { e.JobName, e.JobGroup, e.JobListener });

                entity.ToTable("qrtz_job_listeners");

                entity.Property(e => e.JobName)
                    .HasColumnName("JOB_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.JobGroup)
                    .HasColumnName("JOB_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.JobListener)
                    .HasColumnName("JOB_LISTENER")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzLocks>(entity =>
            {
                entity.HasKey(e => e.LockName);

                entity.ToTable("qrtz_locks");

                entity.Property(e => e.LockName)
                    .HasColumnName("LOCK_NAME")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzPausedTriggerGrps>(entity =>
            {
                entity.HasKey(e => new { e.TriggerGroup, e.SchedName });

                entity.ToTable("qrtz_paused_trigger_grps");

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SchedName)
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QrtzSchedulerState>(entity =>
            {
                entity.HasKey(e => e.InstanceName);

                entity.ToTable("qrtz_scheduler_state");

                entity.Property(e => e.InstanceName)
                    .HasColumnName("INSTANCE_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CheckinInterval)
                    .HasColumnName("CHECKIN_INTERVAL")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.LastCheckinTime)
                    .HasColumnName("LAST_CHECKIN_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzSimpleTriggers>(entity =>
            {
                entity.HasKey(e => new { e.TriggerName, e.TriggerGroup });

                entity.ToTable("qrtz_simple_triggers");

                entity.Property(e => e.TriggerName)
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RepeatCount)
                    .HasColumnName("REPEAT_COUNT")
                    .HasColumnType("bigint(7)");

                entity.Property(e => e.RepeatInterval)
                    .HasColumnName("REPEAT_INTERVAL")
                    .HasColumnType("bigint(12)");

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");

                entity.Property(e => e.TimesTriggered)
                    .HasColumnName("TIMES_TRIGGERED")
                    .HasColumnType("bigint(10)");
            });

            modelBuilder.Entity<QrtzSimpropTriggers>(entity =>
            {
                entity.HasKey(e => new { e.SchedName, e.TriggerName, e.TriggerGroup });

                entity.ToTable("qrtz_simprop_triggers");

                entity.Property(e => e.SchedName)
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerName)
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BoolProp1)
                    .HasColumnName("BOOL_PROP_1")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.BoolProp2)
                    .HasColumnName("BOOL_PROP_2")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.DecProp1)
                    .HasColumnName("DEC_PROP_1")
                    .HasColumnType("decimal(13,4)");

                entity.Property(e => e.DecProp2)
                    .HasColumnName("DEC_PROP_2")
                    .HasColumnType("decimal(13,4)");

                entity.Property(e => e.IntProp1)
                    .HasColumnName("INT_PROP_1")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IntProp2)
                    .HasColumnName("INT_PROP_2")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LongProp1)
                    .HasColumnName("LONG_PROP_1")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.LongProp2)
                    .HasColumnName("LONG_PROP_2")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StrProp1)
                    .HasColumnName("STR_PROP_1")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.StrProp2)
                    .HasColumnName("STR_PROP_2")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.StrProp3)
                    .HasColumnName("STR_PROP_3")
                    .HasMaxLength(512)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QrtzTriggerListeners>(entity =>
            {
                entity.HasKey(e => new { e.TriggerName, e.TriggerGroup, e.TriggerListener });

                entity.ToTable("qrtz_trigger_listeners");

                entity.Property(e => e.TriggerName)
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerListener)
                    .HasColumnName("TRIGGER_LISTENER")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");
            });

            modelBuilder.Entity<QrtzTriggers>(entity =>
            {
                entity.HasKey(e => new { e.TriggerName, e.TriggerGroup });

                entity.ToTable("qrtz_triggers");

                entity.HasIndex(e => new { e.JobName, e.JobGroup })
                    .HasName("JOB_NAME");

                entity.Property(e => e.TriggerName)
                    .HasColumnName("TRIGGER_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerGroup)
                    .HasColumnName("TRIGGER_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CalendarName)
                    .HasColumnName("CALENDAR_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.EndTime)
                    .HasColumnName("END_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.JobData)
                    .HasColumnName("JOB_DATA")
                    .HasColumnType("blob");

                entity.Property(e => e.JobGroup)
                    .IsRequired()
                    .HasColumnName("JOB_GROUP")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.JobName)
                    .IsRequired()
                    .HasColumnName("JOB_NAME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MisfireInstr)
                    .HasColumnName("MISFIRE_INSTR")
                    .HasColumnType("smallint(2)");

                entity.Property(e => e.NextFireTime)
                    .HasColumnName("NEXT_FIRE_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.PrevFireTime)
                    .HasColumnName("PREV_FIRE_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.Priority)
                    .HasColumnName("PRIORITY")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SchedName)
                    .IsRequired()
                    .HasColumnName("SCHED_NAME")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("schedulerFactoryBean");

                entity.Property(e => e.StartTime)
                    .HasColumnName("START_TIME")
                    .HasColumnType("bigint(13)");

                entity.Property(e => e.TriggerState)
                    .IsRequired()
                    .HasColumnName("TRIGGER_STATE")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerType)
                    .IsRequired()
                    .HasColumnName("TRIGGER_TYPE")
                    .HasMaxLength(8)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Recommendedsalesprice>(entity =>
            {
                entity.ToTable("recommendedsalesprice");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineMetaDataId)
                    .HasName("fk_partmetadata_recommendedsaleprice_machineMetaData_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineMetaDataId)
                    .HasColumnName("machineMetaData_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RecommendedSalesPrice1)
                    .HasColumnName("recommendedSalesPrice")
                    .HasColumnType("decimal(8,2)");

                entity.HasOne(d => d.MachineMetaData)
                    .WithMany(p => p.Recommendedsalesprice)
                    .HasForeignKey(d => d.MachineMetaDataId)
                    .HasConstraintName("recommendedsalesprice_ibfk_1");
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.ToTable("region");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FK91AD13147E178927");

                entity.HasIndex(e => e.GoverningDistrictId)
                    .HasName("FK91AD13148ADFFBA7");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.GoverningDistrictId)
                    .HasColumnName("governingDistrict_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Region)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK91AD13147E178927");

                entity.HasOne(d => d.GoverningDistrict)
                    .WithMany(p => p.Region)
                    .HasForeignKey(d => d.GoverningDistrictId)
                    .HasConstraintName("FK91AD13148ADFFBA7");
            });

            modelBuilder.Entity<Rejectionreason>(entity =>
            {
                entity.ToTable("rejectionreason");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Reminderscheduler>(entity =>
            {
                entity.ToTable("reminderscheduler");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CronExpression)
                    .HasColumnName("cron_expression")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DeferredUntilAvailable)
                    .HasColumnName("deferredUntilAvailable")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DelayHours)
                    .HasColumnName("delay_hours")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DelayMinutes)
                    .HasColumnName("delay_minutes")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MinutesBeforeEta)
                    .HasColumnName("minutes_before_eta")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NewStatus)
                    .HasColumnName("new_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationType)
                    .HasColumnName("notification_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PreviousStatus)
                    .HasColumnName("previous_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientRole)
                    .HasColumnName("recipient_role")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReminderTemplate)
                    .HasColumnName("reminder_template")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RepeatCount)
                    .HasColumnName("repeat_count")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RepeatIntervalHours)
                    .HasColumnName("repeat_interval_hours")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RepeatIntervalMinutes)
                    .HasColumnName("repeat_interval_minutes")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduleAction)
                    .HasColumnName("schedule_action")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Reportperiod>(entity =>
            {
                entity.ToTable("reportperiod");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FK27D05435F069FDD9");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.RegionId)
                    .HasName("FK27D0543560F8697B");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DateFrom)
                    .HasColumnName("date_from")
                    .HasColumnType("date");

                entity.Property(e => e.DateTo)
                    .HasColumnName("date_to")
                    .HasColumnType("date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Processed)
                    .HasColumnName("processed")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Reportperiod)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK27D05435F069FDD9");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Reportperiod)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK27D0543560F8697B");
            });

            modelBuilder.Entity<Ruledefinition>(entity =>
            {
                entity.ToTable("ruledefinition");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InternalParameter)
                    .IsRequired()
                    .HasColumnName("internalParameter")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsCommon)
                    .HasColumnName("isCommon")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsDefault)
                    .HasColumnName("isDefault")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RuledefinitionCostcategoryids>(entity =>
            {
                entity.ToTable("ruledefinition_costcategoryids");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.RuledefinitionId)
                    .HasName("fk_ruleDefinitionCostCategory");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostCategoryId)
                    .HasColumnName("costCategoryId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RuledefinitionId)
                    .HasColumnName("ruledefinition_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Ruledefinition)
                    .WithMany(p => p.RuledefinitionCostcategoryids)
                    .HasForeignKey(d => d.RuledefinitionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ruledefinition_costcategoryids_ibfk_1");
            });

            modelBuilder.Entity<Sampletestquestion>(entity =>
            {
                entity.ToTable("sampletestquestion");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.MaxValue).HasColumnName("max_value");

                entity.Property(e => e.MinValue).HasColumnName("min_value");

                entity.Property(e => e.Question)
                    .HasColumnName("question")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Sampletestresult>(entity =>
            {
                entity.ToTable("sampletestresult");

                entity.HasIndex(e => e.SampleId)
                    .HasName("FK8BA1CCB9104D38AD");

                entity.HasIndex(e => e.TestQuestionId)
                    .HasName("FK8BA1CCB94E37A857");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SampleId)
                    .HasColumnName("sample_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Score).HasColumnName("score");

                entity.Property(e => e.TestQuestionId)
                    .HasColumnName("testQuestion_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Sample)
                    .WithMany(p => p.Sampletestresult)
                    .HasForeignKey(d => d.SampleId)
                    .HasConstraintName("FK8BA1CCB9104D38AD");

                entity.HasOne(d => d.TestQuestion)
                    .WithMany(p => p.Sampletestresult)
                    .HasForeignKey(d => d.TestQuestionId)
                    .HasConstraintName("FK8BA1CCB94E37A857");
            });

            modelBuilder.Entity<Scheduledreport>(entity =>
            {
                entity.ToTable("scheduledreport");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.CronSchedule)
                    .IsRequired()
                    .HasColumnName("cronSchedule")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.JobName)
                    .HasColumnName("jobName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReportFormat)
                    .IsRequired()
                    .HasColumnName("reportFormat")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReportName)
                    .IsRequired()
                    .HasColumnName("reportName")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ScheduleName)
                    .HasColumnName("scheduleName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TriggerName)
                    .HasColumnName("triggerName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(10)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Scheduledreportparameter>(entity =>
            {
                entity.ToTable("scheduledreportparameter");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.ReportKey)
                    .IsRequired()
                    .HasColumnName("reportKey")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReportValue)
                    .IsRequired()
                    .HasColumnName("reportValue")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduledReportId)
                    .HasColumnName("scheduledReport_id")
                    .HasColumnType("bigint(10)");
            });

            modelBuilder.Entity<Schedulestep>(entity =>
            {
                entity.ToTable("schedulestep");

                entity.HasIndex(e => e.AssetId)
                    .HasName("FK4F103E239C97EC3E");

                entity.HasIndex(e => e.PmStepId)
                    .HasName("FK4F103E23FE72DF18");

                entity.HasIndex(e => e.ServiceRequestId)
                    .HasName("FK4F103E23B13C638D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AssetId)
                    .HasColumnName("asset_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Completed).HasColumnName("completed");

                entity.Property(e => e.CurrentCycle)
                    .HasColumnName("current_cycle")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.DueDate).HasColumnName("dueDate");

                entity.Property(e => e.OriginalDue).HasColumnName("originalDue");

                entity.Property(e => e.OverrideDue).HasColumnName("overrideDue");

                entity.Property(e => e.PmStepId)
                    .HasColumnName("pmStep_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ScheduledDue).HasColumnName("scheduledDue");

                entity.Property(e => e.ScheduledOverrideDue).HasColumnName("scheduledOverrideDue");

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.Schedulestep)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK4F103E239C97EC3E");

                entity.HasOne(d => d.PmStep)
                    .WithMany(p => p.Schedulestep)
                    .HasForeignKey(d => d.PmStepId)
                    .HasConstraintName("FK4F103E23FE72DF18");

                entity.HasOne(d => d.ServiceRequest)
                    .WithMany(p => p.Schedulestep)
                    .HasForeignKey(d => d.ServiceRequestId)
                    .HasConstraintName("FK4F103E23B13C638D");
            });

            modelBuilder.Entity<SchemaVersion>(entity =>
            {
                entity.HasKey(e => e.Version);

                entity.ToTable("schema_version");

                entity.HasIndex(e => e.InstalledRank)
                    .HasName("schema_version_ir_idx");

                entity.HasIndex(e => e.Success)
                    .HasName("schema_version_s_idx");

                entity.HasIndex(e => e.VersionRank)
                    .HasName("schema_version_vr_idx");

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Checksum)
                    .HasColumnName("checksum")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ExecutionTime)
                    .HasColumnName("execution_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InstalledBy)
                    .IsRequired()
                    .HasColumnName("installed_by")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InstalledOn)
                    .HasColumnName("installed_on")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InstalledRank)
                    .HasColumnName("installed_rank")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Script)
                    .IsRequired()
                    .HasColumnName("script")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Success)
                    .HasColumnName("success")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VersionRank)
                    .HasColumnName("version_rank")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Servicecontrollernotification>(entity =>
            {
                entity.ToTable("servicecontrollernotification");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NotificationEventType)
                    .IsRequired()
                    .HasColumnName("notificationEventType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NotificationType)
                    .IsRequired()
                    .HasColumnName("notificationType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(10)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Serviceevent>(entity =>
            {
                entity.ToTable("serviceevent");

                entity.HasIndex(e => e.AvoidableCallTypeId)
                    .HasName("fk_serviceevent_avoidablecalltype");

                entity.HasIndex(e => e.CommonCauseId)
                    .HasName("FK1DD3F905B42B0587");

                entity.HasIndex(e => e.CommonFaultId)
                    .HasName("FK1DD3F905EB9CFEE7");

                entity.HasIndex(e => e.CostBreakDownId)
                    .HasName("FK1DD3F905A950E2A7");

                entity.HasIndex(e => e.EventTypeId)
                    .HasName("FK1DD3F9054F60F547");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineId)
                    .HasName("FK1DD3F90547107F87");

                entity.HasIndex(e => e.ServiceRequestId)
                    .HasName("FK1DD3F905B13C638D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AvoidableCallTypeId)
                    .HasColumnName("avoidableCallType_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ClosedTime).HasColumnName("closed_time");

                entity.Property(e => e.CommonCauseId)
                    .HasColumnName("commonCause_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CommonFaultId)
                    .HasColumnName("commonFault_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CostBreakDownId)
                    .HasColumnName("costBreakDown_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.CreationTime).HasColumnName("creation_time");

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("eventType_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ExtendedPartsWarranty)
                    .HasColumnName("extended_parts_warranty")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.FaultDescription)
                    .HasColumnName("fault_description")
                    .HasColumnType("longtext");

                entity.Property(e => e.FollowUpRequired)
                    .HasColumnName("follow_up_required")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RequiredAssignmentTime)
                    .HasColumnName("required_assignment_time")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RequiredResponseTimeInterval)
                    .HasColumnName("required_response_time_interval")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ResolutionTime)
                    .HasColumnName("resolutionTime")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.SelfFixSuccessful)
                    .HasColumnName("self_fix_successful")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ServiceEventStatus)
                    .HasColumnName("service_event_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("serviceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Warranty)
                    .HasColumnName("warranty")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.HasOne(d => d.AvoidableCallType)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.AvoidableCallTypeId)
                    .HasConstraintName("serviceevent_ibfk_1");

                entity.HasOne(d => d.CommonCause)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.CommonCauseId)
                    .HasConstraintName("FK1DD3F905B42B0587");

                entity.HasOne(d => d.CommonFault)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.CommonFaultId)
                    .HasConstraintName("FK1DD3F905EB9CFEE7");

                entity.HasOne(d => d.CostBreakDown)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.CostBreakDownId)
                    .HasConstraintName("FK1DD3F905A950E2A7");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.EventTypeId)
                    .HasConstraintName("FK1DD3F9054F60F547");

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.MachineId)
                    .HasConstraintName("FK1DD3F90547107F87");

                entity.HasOne(d => d.ServiceRequest)
                    .WithMany(p => p.Serviceevent)
                    .HasForeignKey(d => d.ServiceRequestId)
                    .HasConstraintName("FK1DD3F905B13C638D");
            });

            modelBuilder.Entity<ServiceEventPartsList>(entity =>
            {
                entity.HasKey(e => new { e.Serviceeventid, e.PartOrderNumber });

                entity.ToTable("service_event_parts_list");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Serviceeventid)
                    .HasColumnName("serviceeventid")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PartOrderNumber).HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PartDescription)
                    .HasColumnName("part_description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartNumber)
                    .HasColumnName("part_number")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Total)
                    .HasColumnName("total")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<Serviceeventtocomponentmetadata>(entity =>
            {
                entity.ToTable("serviceeventtocomponentmetadata");

                entity.HasIndex(e => e.ComponentPartmetadataId)
                    .HasName("fk_serviceeventtocomponentmetadata_component_partmetadata_id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceeventId)
                    .HasName("fk_serviceeventtocomponentmetadata_serviceevent_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ComponentPartmetadataId)
                    .HasColumnName("component_partmetadata_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.ServiceeventId)
                    .HasColumnName("serviceevent_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.ComponentPartmetadata)
                    .WithMany(p => p.Serviceeventtocomponentmetadata)
                    .HasForeignKey(d => d.ComponentPartmetadataId)
                    .HasConstraintName("serviceeventtocomponentmetadata_ibfk_2");

                entity.HasOne(d => d.Serviceevent)
                    .WithMany(p => p.Serviceeventtocomponentmetadata)
                    .HasForeignKey(d => d.ServiceeventId)
                    .HasConstraintName("serviceeventtocomponentmetadata_ibfk_1");
            });

            modelBuilder.Entity<Servicenote>(entity =>
            {
                entity.ToTable("servicenote");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineId)
                    .HasName("FKF076302747107F87");

                entity.HasIndex(e => e.ServiceEventId)
                    .HasName("FKF07630279C75D46D");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FKF0763027BF9BE887");

                entity.HasIndex(e => e.VisitServiceEventId)
                    .HasName("FKF07630275E3B6A7");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Author)
                    .HasColumnName("author")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.CreationTime).HasColumnName("creation_time");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Machine)
                    .HasColumnName("machine")
                    .HasColumnType("tinyblob");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Notetype)
                    .HasColumnName("notetype")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrivacyStatus)
                    .HasColumnName("privacy_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceEventId)
                    .HasColumnName("serviceEvent_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasColumnType("longtext");

                entity.Property(e => e.Text)
                    .HasColumnName("text")
                    .HasColumnType("longtext");

                entity.Property(e => e.VisitServiceEventId)
                    .HasColumnName("visitServiceEvent_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.MachineNavigation)
                    .WithMany(p => p.Servicenote)
                    .HasForeignKey(d => d.MachineId)
                    .HasConstraintName("FKF076302747107F87");

                entity.HasOne(d => d.ServiceEvent)
                    .WithMany(p => p.Servicenote)
                    .HasForeignKey(d => d.ServiceEventId)
                    .HasConstraintName("FKF07630279C75D46D");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Servicenote)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FKF0763027BF9BE887");

                entity.HasOne(d => d.VisitServiceEvent)
                    .WithMany(p => p.Servicenote)
                    .HasForeignKey(d => d.VisitServiceEventId)
                    .HasConstraintName("FKF07630275E3B6A7");
            });

            modelBuilder.Entity<Serviceprovider>(entity =>
            {
                entity.ToTable("serviceprovider");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FKD31163A67E178927");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.Name)
                    .HasName("serviceprovider_name")
                    .IsUnique();

                entity.HasIndex(e => e.ParentId)
                    .HasName("FKD31163A6A15B26C3");

                entity.HasIndex(e => e.RegionId)
                    .HasName("FKD31163A6D0E53C6D");

                entity.HasIndex(e => e.ServiceProviderAbnNumber)
                    .HasName("service_provider_abn_number")
                    .IsUnique();

                entity.HasIndex(e => e.ServiceProviderCode)
                    .HasName("service_provider_code")
                    .IsUnique();

                entity.HasIndex(e => e.ShortName)
                    .HasName("short_name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AddressType)
                    .HasColumnName("address_type")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.AddressTypeIdentifier)
                    .HasColumnName("address_type_identifier")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CalendarId)
                    .HasColumnName("calendar_id")
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DaysOnCall)
                    .HasColumnName("days_on_call")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Enabled)
                    .HasColumnName("enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.EscalationRequired)
                    .HasColumnName("escalation_required")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'1'");

                entity.Property(e => e.Exported)
                    .HasColumnName("exported")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.FastSignInEnabled)
                    .HasColumnName("fastSignInEnabled")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.FastSignInHours)
                    .HasColumnName("fastSignInHours")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GoverningDistrict)
                    .HasColumnName("governing_district")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.HkCostSubmission)
                    .HasColumnName("hkCostSubmission")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.InvoiceTemplate)
                    .HasColumnName("invoice_template")
                    .HasColumnType("longblob");

                entity.Property(e => e.InvoicingEnabled)
                    .HasColumnName("invoicing_enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MajorMunicipality)
                    .HasColumnName("major_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MinorMunicipality)
                    .HasColumnName("minor_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhoneNumber)
                    .HasColumnName("mobile_phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationType)
                    .HasColumnName("notification_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OutOfHoursSupport)
                    .HasColumnName("out_of_hours_support")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.PersonalIndemnityInsuranceExpireDate).HasColumnName("personal_indemnity_insurance_expire_date");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PostalArea)
                    .HasColumnName("postal_area")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryContact)
                    .HasColumnName("primary_contact")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PublicLiabilityInsuranceExpireDate).HasColumnName("public_liability_insurance_expire_date");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RequireQuestionnaire)
                    .HasColumnName("require_questionnaire")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ServiceController)
                    .HasColumnName("service_controller")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderAbnNumber)
                    .HasColumnName("service_provider_abn_number")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceProviderCode)
                    .HasColumnName("service_provider_code")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ShortName)
                    .HasColumnName("short_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoleTrader)
                    .HasColumnName("sole_trader")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.StreetDirection)
                    .HasColumnName("street_direction")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.StreetName)
                    .HasColumnName("street_name")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.StreetNumber)
                    .HasColumnName("street_number")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SwitchOverTime)
                    .HasColumnName("switch_over_time")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZone)
                    .HasColumnName("time_zone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TwentyFourHourCoverage)
                    .HasColumnName("twenty_four_hour_coverage")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.UseSettings)
                    .HasColumnName("use_settings")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.UsingJmsNotifications)
                    .HasColumnName("usingJmsNotifications")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.Serviceprovider)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FKD31163A67E178927");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FKD31163A6A15B26C3");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Serviceprovider)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FKD31163A6D0E53C6D");
            });

            modelBuilder.Entity<Serviceprovidernotification>(entity =>
            {
                entity.ToTable("serviceprovidernotification");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NotificationEventType)
                    .IsRequired()
                    .HasColumnName("notificationEventType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NotificationType)
                    .IsRequired()
                    .HasColumnName("notificationType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(10)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Servicerelationship>(entity =>
            {
                entity.ToTable("servicerelationship");

                entity.HasIndex(e => e.EventtypegroupId)
                    .HasName("fk_eventtypegroup");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FK6851F16D5F04BDE7");

                entity.HasIndex(e => e.OemDetailsId)
                    .HasName("FK6851F16D1EEB76D");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK6851F16D8963B647");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FK6851F16DBF9BE887");

                entity.HasIndex(e => new { e.ServiceProviderId, e.StoreId, e.MachineCategoryId, e.OemDetailsId, e.EventtypegroupId })
                    .HasName("unique_provider_store_mapping")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EventtypegroupId)
                    .HasColumnName("eventtypegroup_id")
                    .HasColumnType("int(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemDetailsId)
                    .HasColumnName("oemDetails_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Preference)
                    .HasColumnName("preference")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Eventtypegroup)
                    .WithMany(p => p.Servicerelationship)
                    .HasForeignKey(d => d.EventtypegroupId)
                    .HasConstraintName("servicerelationship_ibfk_1");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Servicerelationship)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FK6851F16D5F04BDE7");

                entity.HasOne(d => d.OemDetails)
                    .WithMany(p => p.Servicerelationship)
                    .HasForeignKey(d => d.OemDetailsId)
                    .HasConstraintName("FK6851F16D1EEB76D");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Servicerelationship)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK6851F16D8963B647");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Servicerelationship)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FK6851F16DBF9BE887");
            });

            modelBuilder.Entity<Servicerequest>(entity =>
            {
                entity.ToTable("servicerequest");

                entity.HasIndex(e => e.CancellationReasonId)
                    .HasName("FK8C148A3AAEE4D72D");

                entity.HasIndex(e => e.Created)
                    .HasName("createdIndex");

                entity.HasIndex(e => e.DeferralReasonId)
                    .HasName("FK8C148A3AD8F729A6");

                entity.HasIndex(e => e.EngineerId)
                    .HasName("FK8C148A3A853EC5CD");

                entity.HasIndex(e => e.KitId)
                    .HasName("FK8C148A3AE2FDFA7");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.LinkedServiceRequestId)
                    .HasName("FK8C148A3A52959294");

                entity.HasIndex(e => e.ScheduleStepId)
                    .HasName("FK8C148A3A8F790826");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK8C148A3A8963B647");

                entity.HasIndex(e => e.ServiceRequestStatus)
                    .HasName("servicerequest_status");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FK8C148A3ABF9BE887");

                entity.HasIndex(e => e.WarrantyProviderId)
                    .HasName("FK8C148A3ABB6ECE2D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Accepted).HasColumnName("accepted");

                entity.Property(e => e.Allocated).HasColumnName("allocated");

                entity.Property(e => e.Approved)
                    .HasColumnName("approved")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("approved_by")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedTime).HasColumnName("approved_time");

                entity.Property(e => e.CancellationReason)
                    .HasColumnName("cancellation_reason")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CancellationReasonId)
                    .HasColumnName("cancellationReason_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CancelledByUser)
                    .HasColumnName("cancelled_by_user")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Closed).HasColumnName("closed");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.DeferDays)
                    .HasColumnName("defer_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DeferralReasonId)
                    .HasColumnName("deferralReason_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DelayDays)
                    .HasColumnName("delay_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EngineerId)
                    .HasColumnName("engineer_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Eta).HasColumnName("eta");

                entity.Property(e => e.ExtendedServiceConfirmed)
                    .HasColumnName("extended_service_confirmed")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.HkAccepted)
                    .HasColumnName("hk_accepted")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.InvoiceNumber)
                    .HasColumnName("invoice_number")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.KitId)
                    .HasColumnName("kit_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.LastEscalatedOn)
                    .HasColumnName("lastEscalatedOn")
                    .HasColumnType("date");

                entity.Property(e => e.LastEscalatedToId)
                    .HasColumnName("lastEscalatedTo_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LinkedServiceRequestId)
                    .HasColumnName("linkedServiceRequest_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.NotificationPhoneNumber)
                    .HasColumnName("notification_phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationType)
                    .HasColumnName("notification_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OldEta).HasColumnName("old_eta");

                entity.Property(e => e.PlannedMaintenanceStep)
                    .HasColumnName("planned_maintenance_step")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionnaireId)
                    .HasColumnName("questionnaireId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RequiresCert)
                    .HasColumnName("requiresCert")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.RequiresSample)
                    .HasColumnName("requires_sample")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ResolvedTimeInMinutes)
                    .HasColumnName("resolvedTimeInMinutes")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Resubmitted)
                    .HasColumnName("resubmitted")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.SampleRequiresApproval)
                    .HasColumnName("sample_requires_approval")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ScheduleStepDue)
                    .HasColumnName("schedule_step_due")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduleStepId)
                    .HasColumnName("scheduleStep_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ServiceRequestStatus)
                    .HasColumnName("service_request_status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusUpdated).HasColumnName("status_updated");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.TicketNumber)
                    .IsRequired()
                    .HasColumnName("ticketNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.WarrantyProviderId)
                    .HasColumnName("warrantyProvider_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.CancellationReasonNavigation)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.CancellationReasonId)
                    .HasConstraintName("FK8C148A3AAEE4D72D");

                entity.HasOne(d => d.DeferralReason)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.DeferralReasonId)
                    .HasConstraintName("FK8C148A3AD8F729A6");

                entity.HasOne(d => d.Engineer)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.EngineerId)
                    .HasConstraintName("FK8C148A3A853EC5CD");

                entity.HasOne(d => d.Kit)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.KitId)
                    .HasConstraintName("FK8C148A3AE2FDFA7");

                entity.HasOne(d => d.LinkedServiceRequest)
                    .WithMany(p => p.InverseLinkedServiceRequest)
                    .HasForeignKey(d => d.LinkedServiceRequestId)
                    .HasConstraintName("FK8C148A3A52959294");

                entity.HasOne(d => d.ScheduleStep)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.ScheduleStepId)
                    .HasConstraintName("FK8C148A3A8F790826");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK8C148A3A8963B647");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("FK8C148A3ABF9BE887");

                entity.HasOne(d => d.WarrantyProvider)
                    .WithMany(p => p.Servicerequest)
                    .HasForeignKey(d => d.WarrantyProviderId)
                    .HasConstraintName("FK8C148A3ABB6ECE2D");
            });

            modelBuilder.Entity<Station>(entity =>
            {
                entity.ToTable("station");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.NameId)
                    .HasName("FK_station_name_Id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NameId)
                    .HasColumnName("name_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Name)
                    .WithMany(p => p.Station)
                    .HasForeignKey(d => d.NameId)
                    .HasConstraintName("FK_station_name_Id");
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.ToTable("store");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FK4C808C17E178927");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.OwnerId)
                    .HasName("FK4C808C144FB59E5");

                entity.HasIndex(e => e.RegionId)
                    .HasName("FK4C808C1D0E53C6D");

                entity.HasIndex(e => e.StoreAbn)
                    .HasName("store_abn")
                    .IsUnique();

                entity.HasIndex(e => new { e.McDonaldsStoreId, e.StoreNumber })
                    .HasName("mc_donalds_store_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.AddressType)
                    .HasColumnName("address_type")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.AddressTypeIdentifier)
                    .HasColumnName("address_type_identifier")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.AllowSpEquipmentAssociations)
                    .HasColumnName("allowSpEquipmentAssociations")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'1'");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.GoverningDistrict)
                    .HasColumnName("governing_district")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MajorMunicipality)
                    .HasColumnName("major_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.McDonaldsStoreId)
                    .HasColumnName("mc_donalds_store_id")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.MinorMunicipality)
                    .HasColumnName("minor_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhoneNumber)
                    .HasColumnName("mobile_phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Open)
                    .HasColumnName("open")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.OwnerId)
                    .HasColumnName("owner_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PostalArea)
                    .HasColumnName("postal_area")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RequiresServiceRequestApproval)
                    .HasColumnName("requires_service_request_approval")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.RequiresServiceRequestPmApproval)
                    .HasColumnName("requires_service_request_pm_approval")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.StoreAbn)
                    .HasColumnName("store_abn")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.StoreNumber)
                    .HasColumnName("store_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StoreType)
                    .HasColumnName("store_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StreetDirection)
                    .HasColumnName("street_direction")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.StreetName)
                    .HasColumnName("street_name")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.StreetNumber)
                    .HasColumnName("street_number")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZone)
                    .HasColumnName("time_zone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UseEtech)
                    .HasColumnName("use_etech")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.UseSettings)
                    .HasColumnName("use_settings")
                    .HasColumnType("bit(1)");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.Store)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK4C808C17E178927");

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Store)
                    .HasForeignKey(d => d.OwnerId)
                    .HasConstraintName("FK4C808C144FB59E5");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Store)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK4C808C1D0E53C6D");
            });

            modelBuilder.Entity<Storenotification>(entity =>
            {
                entity.ToTable("storenotification");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NotificationEventType)
                    .IsRequired()
                    .HasColumnName("notificationEventType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NotificationType)
                    .IsRequired()
                    .HasColumnName("notificationType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(10)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Supportresource>(entity =>
            {
                entity.ToTable("supportresource");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FK89F220DD5F04BDE7");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Payload)
                    .HasColumnName("payload")
                    .HasColumnType("longblob");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Supportresource)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FK89F220DD5F04BDE7");
            });

            modelBuilder.Entity<Surveyresponse>(entity =>
            {
                entity.ToTable("surveyresponse");

                entity.HasIndex(e => e.VisitId)
                    .HasName("FKB033F1DBB7DB9847");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Completed)
                    .HasColumnName("completed")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.ResponseBody)
                    .HasColumnName("responseBody")
                    .HasColumnType("longtext");

                entity.Property(e => e.ResponseReferenceNumber)
                    .HasColumnName("responseReferenceNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ResponseUrl)
                    .HasColumnName("responseUrl")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SurveyBody)
                    .HasColumnName("surveyBody")
                    .HasColumnType("longtext");

                entity.Property(e => e.SurveyReferenceNumber)
                    .HasColumnName("surveyReferenceNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VisitId)
                    .HasColumnName("visit_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.Surveyresponse)
                    .HasForeignKey(d => d.VisitId)
                    .HasConstraintName("FKB033F1DBB7DB9847");
            });

            modelBuilder.Entity<Systemlabel>(entity =>
            {
                entity.ToTable("systemlabel");

                entity.HasIndex(e => e.DescriptionId)
                    .HasName("FK_systemlabel_description_Id");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ParentLabelId)
                    .HasName("FK6D4C9E5C35BE7A2");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DescriptionId)
                    .HasColumnName("description_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OrderIndex)
                    .HasColumnName("orderIndex")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ParentLabelId)
                    .HasColumnName("parentLabel_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Description)
                    .WithMany(p => p.Systemlabel)
                    .HasForeignKey(d => d.DescriptionId)
                    .HasConstraintName("FK_systemlabel_description_Id");

                entity.HasOne(d => d.ParentLabel)
                    .WithMany(p => p.InverseParentLabel)
                    .HasForeignKey(d => d.ParentLabelId)
                    .HasConstraintName("FK6D4C9E5C35BE7A2");
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.ToTable("task");

                entity.HasIndex(e => e.MachineId)
                    .HasName("machine_id_FI");

                entity.HasIndex(e => e.StationId)
                    .HasName("station_id_FI");

                entity.HasIndex(e => e.StoreId)
                    .HasName("store_id_FI");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.DayOfMonth)
                    .HasColumnName("dayOfMonth")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DayOfTheWeek)
                    .HasColumnName("dayOfTheWeek")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("longtext");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Frequency)
                    .HasColumnName("frequency")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machine_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MonthlyInterval)
                    .HasColumnName("monthlyInterval")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.StationId)
                    .HasColumnName("station_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Uuid)
                    .HasColumnName("uuid")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.MachineId)
                    .HasConstraintName("machine_id_FK");

                entity.HasOne(d => d.Station)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.StationId)
                    .HasConstraintName("station_id_FK");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("store_id_FK");
            });

            modelBuilder.Entity<Taskinstance>(entity =>
            {
                entity.ToTable("taskinstance");

                entity.HasIndex(e => e.CompletedById)
                    .HasName("completedby_id_FI");

                entity.HasIndex(e => e.StoreId)
                    .HasName("store_id_FI_TI");

                entity.HasIndex(e => e.TaskId)
                    .HasName("task_id_FI");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Completed)
                    .HasColumnName("completed")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CompletedById)
                    .HasColumnName("completedBy_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CompletedDate).HasColumnName("completed_date");

                entity.Property(e => e.DueDate).HasColumnName("due_date");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Reporter)
                    .HasColumnName("reporter")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Uuid)
                    .HasColumnName("uuid")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CompletedBy)
                    .WithMany(p => p.Taskinstance)
                    .HasForeignKey(d => d.CompletedById)
                    .HasConstraintName("completedby_id_FK");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Taskinstance)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("store_id_FK_TI");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.Taskinstance)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("task_id_FK");
            });

            modelBuilder.Entity<Ticketnumberoffsetreferencedata>(entity =>
            {
                entity.ToTable("ticketnumberoffsetreferencedata");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Offset)
                    .HasColumnName("offset")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Timeperiod>(entity =>
            {
                entity.ToTable("timeperiod");

                entity.HasIndex(e => e.AvailabilityId)
                    .HasName("FKA028830E220FDCCD");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AvailabilityId)
                    .HasColumnName("availability_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.FinishTime)
                    .HasColumnName("finish_time")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StartTime)
                    .HasColumnName("start_time")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TimeCategory)
                    .HasColumnName("time_category")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Availability)
                    .WithMany(p => p.Timeperiod)
                    .HasForeignKey(d => d.AvailabilityId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FKA028830E220FDCCD");
            });

            modelBuilder.Entity<Translation>(entity =>
            {
                entity.ToTable("translation");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<TranslationStrings>(entity =>
            {
                entity.HasKey(e => new { e.TranslationId, e.StringsKey });

                entity.ToTable("translation_strings");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.TranslationId)
                    .HasColumnName("Translation_ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StringsKey)
                    .HasColumnName("STRINGS_KEY")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Strings)
                    .HasColumnName("STRINGS")
                    .HasColumnType("longtext");
            });

            modelBuilder.Entity<Usernotification>(entity =>
            {
                entity.ToTable("usernotification");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(10)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NotificationEventType)
                    .IsRequired()
                    .HasColumnName("notificationEventType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NotificationType)
                    .IsRequired()
                    .HasColumnName("notificationType")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(10)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Useroemrelationship>(entity =>
            {
                entity.ToTable("useroemrelationship");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.OemDetailsId)
                    .HasName("FK3C4451C41EEB76D");

                entity.HasIndex(e => e.UserId)
                    .HasName("FK3C4451C4D914A9CD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OemDetailsId)
                    .HasColumnName("oemDetails_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.OemDetails)
                    .WithMany(p => p.Useroemrelationship)
                    .HasForeignKey(d => d.OemDetailsId)
                    .HasConstraintName("FK3C4451C41EEB76D");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Useroemrelationship)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK3C4451C4D914A9CD");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ProviderId)
                    .HasName("FK6A68E08A105501C");

                entity.HasIndex(e => e.Username)
                    .HasName("username")
                    .IsUnique();

                entity.HasIndex(e => e.WarrantyProviderId)
                    .HasName("FK6A68E08BB6ECE2D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AllowedOnTablet)
                    .HasColumnName("allowedOnTablet")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Enabled)
                    .HasColumnName("enabled")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Language)
                    .HasColumnName("language")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("mobile_number")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderId)
                    .HasColumnName("provider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.RepNumber)
                    .HasColumnName("repNumber")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Timezone)
                    .HasColumnName("timezone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.WarrantyProviderId)
                    .HasColumnName("warrantyProvider_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Provider)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.ProviderId)
                    .HasConstraintName("FK6A68E08A105501C");

                entity.HasOne(d => d.WarrantyProvider)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.WarrantyProviderId)
                    .HasConstraintName("FK6A68E08BB6ECE2D");
            });

            modelBuilder.Entity<Userstore>(entity =>
            {
                entity.ToTable("userstore");

                entity.HasKey(c => new { c.UserId, c.StoreId });

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.StoreId)
                    .HasName("fk_userstore_store_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("fk_userstore_users_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Userstore)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userstore_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userstore)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userstore_ibfk_1");
            });

            modelBuilder.Entity<Userstorerelationship>(entity =>
            {
                entity.ToTable("userstorerelationship");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.StoreId)
                    .HasName("FKE59FCD0EBF9BE887");

                entity.HasIndex(e => e.UserId)
                    .HasName("FKE59FCD0ED914A9CD");

                entity.HasIndex(e => new { e.UserId, e.StoreId })
                    .HasName("user_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StoreId)
                    .HasColumnName("store_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Userstorerelationship)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKE59FCD0EBF9BE887");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userstorerelationship)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKE59FCD0ED914A9CD");
            });

            modelBuilder.Entity<Visit>(entity =>
            {
                entity.ToTable("visit");

                entity.HasIndex(e => e.EngineerId)
                    .HasName("FK4ED5D2B853EC5CD");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.QuestionnaireResponseId)
                    .HasName("FK4ED5D2B4C4F6A44");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CheckListCompleted)
                    .HasColumnName("check_list_completed")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Dtype)
                    .IsRequired()
                    .HasColumnName("DTYPE")
                    .HasMaxLength(31)
                    .IsUnicode(false);

                entity.Property(e => e.EngineerId)
                    .HasColumnName("engineer_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EngineerSignOff)
                    .HasColumnName("engineer_sign_off")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.EngineerSignature)
                    .HasColumnName("engineer_signature")
                    .HasColumnType("longblob");

                entity.Property(e => e.EngineerSignatureOut)
                    .HasColumnName("engineer_signature_out")
                    .HasColumnType("longblob");

                entity.Property(e => e.Eta).HasColumnName("eta");

                entity.Property(e => e.FastSignInUsed)
                    .HasColumnName("fastSignInUsed")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ManagerSignature)
                    .HasColumnName("manager_signature")
                    .HasColumnType("longblob");

                entity.Property(e => e.ManagerSignatureOut)
                    .HasColumnName("manager_signature_out")
                    .HasColumnType("longblob");

                entity.Property(e => e.QuestionnaireResponseId)
                    .HasColumnName("questionnaireResponse_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ScheduleNewVisit)
                    .HasColumnName("schedule_new_visit")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ServiceActionType)
                    .HasColumnName("service_action_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SignInTime).HasColumnName("sign_in_time");

                entity.Property(e => e.SignOutTime).HasColumnName("sign_out_time");

                entity.Property(e => e.SignedInBy)
                    .HasColumnName("signed_in_by")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SignedOutBy)
                    .HasColumnName("signed_out_by")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StoreSignOff)
                    .HasColumnName("store_sign_off")
                    .HasColumnType("bit(1)");

                entity.HasOne(d => d.Engineer)
                    .WithMany(p => p.Visit)
                    .HasForeignKey(d => d.EngineerId)
                    .HasConstraintName("FK4ED5D2B853EC5CD");

                entity.HasOne(d => d.QuestionnaireResponse)
                    .WithMany(p => p.VisitNavigation)
                    .HasForeignKey(d => d.QuestionnaireResponseId)
                    .HasConstraintName("FK4ED5D2B4C4F6A44");
            });

            modelBuilder.Entity<Visitapprover>(entity =>
            {
                entity.ToTable("visitapprover");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.VisitId)
                    .HasName("FKD63ECD10B7DB9847");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VisitId)
                    .HasColumnName("visit_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.Visitapprover)
                    .HasForeignKey(d => d.VisitId)
                    .HasConstraintName("FKD63ECD10B7DB9847");
            });

            modelBuilder.Entity<Visitserviceevent>(entity =>
            {
                entity.ToTable("visitserviceevent");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceEventId)
                    .HasName("FKD09FEDB09C75D46D");

                entity.HasIndex(e => e.VisitId)
                    .HasName("FKD09FEDB0B7DB9847");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineOperational)
                    .HasColumnName("machineOperational")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.ServiceEventId)
                    .HasColumnName("serviceEvent_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.VisitId)
                    .HasColumnName("visit_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.VisitOutcome)
                    .HasColumnName("visit_outcome")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VisitSubOutcome)
                    .HasColumnName("visit_sub_outcome")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.ServiceEvent)
                    .WithMany(p => p.Visitserviceevent)
                    .HasForeignKey(d => d.ServiceEventId)
                    .HasConstraintName("FKD09FEDB09C75D46D");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.Visitserviceevent)
                    .HasForeignKey(d => d.VisitId)
                    .HasConstraintName("FKD09FEDB0B7DB9847");
            });

            modelBuilder.Entity<Visitserviceeventpart>(entity =>
            {
                entity.ToTable("visitserviceeventpart");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.PartId)
                    .HasName("FK7EB76DC3AF825499");

                entity.HasIndex(e => e.VisitServiceEventId)
                    .HasName("FK7EB76DC35E3B6A7");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PartId)
                    .HasColumnName("part_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.VisitPartOutcome)
                    .IsRequired()
                    .HasColumnName("visitPartOutcome")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.VisitServiceEventId)
                    .HasColumnName("visitServiceEvent_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Part)
                    .WithMany(p => p.Visitserviceeventpart)
                    .HasForeignKey(d => d.PartId)
                    .HasConstraintName("FK7EB76DC3AF825499");

                entity.HasOne(d => d.VisitServiceEvent)
                    .WithMany(p => p.Visitserviceeventpart)
                    .HasForeignKey(d => d.VisitServiceEventId)
                    .HasConstraintName("FK7EB76DC35E3B6A7");
            });

            modelBuilder.Entity<Warrantyauditlog>(entity =>
            {
                entity.ToTable("warrantyauditlog");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.EntityId)
                    .HasColumnName("entityId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EntityType)
                    .IsRequired()
                    .HasColumnName("entityType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Warrantyclassification>(entity =>
            {
                entity.ToTable("warrantyclassification");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("currency_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CustomerCode)
                    .HasColumnName("customerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Warrantyclassificationequipmentassociation>(entity =>
            {
                entity.ToTable("warrantyclassificationequipmentassociation");

                entity.HasIndex(e => e.Id)
                    .HasName("id_idx");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("warrantyclassificationequipmentassociation_ibfk_3");

                entity.HasIndex(e => e.OemId)
                    .HasName("oem_id");

                entity.HasIndex(e => e.WarrantyClassificationId)
                    .HasName("warrantyClassification_id_idx");

                entity.HasIndex(e => new { e.WarrantyClassificationId, e.OemId, e.MachineCategoryId })
                    .HasName("uc_EquipmentAssociation")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("createdBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemId)
                    .HasColumnName("oem_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Updated)
                    .HasColumnName("updated")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updatedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.WarrantyClassificationId)
                    .HasColumnName("warrantyClassification_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Warrantyclassificationequipmentassociation)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("warrantyclassificationequipmentassociation_ibfk_3");

                entity.HasOne(d => d.Oem)
                    .WithMany(p => p.Warrantyclassificationequipmentassociation)
                    .HasForeignKey(d => d.OemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("warrantyclassificationequipmentassociation_ibfk_2");

                entity.HasOne(d => d.WarrantyClassification)
                    .WithMany(p => p.Warrantyclassificationequipmentassociation)
                    .HasForeignKey(d => d.WarrantyClassificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("warrantyclassificationequipmentassociation_ibfk_1");
            });

            modelBuilder.Entity<Warrantyclassificationreviewers>(entity =>
            {
                entity.ToTable("warrantyclassificationreviewers");

                entity.HasIndex(e => e.Id)
                    .HasName("id_idx");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id_idx");

                entity.HasIndex(e => e.WarrantyclassificationId)
                    .HasName("warrantyclassification_id_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.WarrantyclassificationId)
                    .HasColumnName("warrantyclassification_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Warrantyclassificationreviewers)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("warrantyclassificationreviewers_ibfk_2");

                entity.HasOne(d => d.Warrantyclassification)
                    .WithMany(p => p.Warrantyclassificationreviewers)
                    .HasForeignKey(d => d.WarrantyclassificationId)
                    .HasConstraintName("warrantyclassificationreviewers_ibfk_1");
            });

            modelBuilder.Entity<Warrantyclassificationruledefinition>(entity =>
            {
                entity.ToTable("warrantyclassificationruledefinition");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.RuledefinitionId)
                    .HasName("ruledefinition_id_idx");

                entity.HasIndex(e => e.WarrantyclassificationId)
                    .HasName("warrantyclassification_id_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RuledefinitionId)
                    .HasColumnName("ruledefinition_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.WarrantyclassificationId)
                    .HasColumnName("warrantyclassification_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Ruledefinition)
                    .WithMany(p => p.Warrantyclassificationruledefinition)
                    .HasForeignKey(d => d.RuledefinitionId)
                    .HasConstraintName("warrantyclassificationruledefinition_ibfk_2");

                entity.HasOne(d => d.Warrantyclassification)
                    .WithMany(p => p.Warrantyclassificationruledefinition)
                    .HasForeignKey(d => d.WarrantyclassificationId)
                    .HasConstraintName("warrantyclassificationruledefinition_ibfk_1");
            });

            modelBuilder.Entity<Warrantyprovider>(entity =>
            {
                entity.ToTable("warrantyprovider");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FKF304F32D7E178927");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.Name)
                    .HasName("name")
                    .IsUnique();

                entity.HasIndex(e => e.RegionId)
                    .HasName("FKF304F32DD0E53C6D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AddressType)
                    .HasColumnName("address_type")
                    .HasMaxLength(48)
                    .IsUnicode(false);

                entity.Property(e => e.AddressTypeIdentifier)
                    .HasColumnName("address_type_identifier")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.GoverningDistrict)
                    .HasColumnName("governing_district")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MajorMunicipality)
                    .HasColumnName("major_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MinorMunicipality)
                    .HasColumnName("minor_municipality")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhoneNumber)
                    .HasColumnName("mobile_phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PostalArea)
                    .HasColumnName("postal_area")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.StreetDirection)
                    .HasColumnName("street_direction")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.StreetName)
                    .HasColumnName("street_name")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.StreetNumber)
                    .HasColumnName("street_number")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZone)
                    .HasColumnName("timeZone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Warrantyprovider)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FKF304F32D7E178927");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Warrantyprovider)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FKF304F32DD0E53C6D");
            });

            modelBuilder.Entity<Warrantysupport>(entity =>
            {
                entity.ToTable("warrantysupport");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.MachineCategoryId)
                    .HasName("FK6160FB135F04BDE7");

                entity.HasIndex(e => e.OemId)
                    .HasName("FK6160FB13298B7BA1");

                entity.HasIndex(e => e.WarrantyProviderId)
                    .HasName("FK6160FB13BB6ECE2D");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MachineCategoryId)
                    .HasColumnName("machineCategory_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.OemId)
                    .HasColumnName("oem_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.WarrantyProviderId)
                    .HasColumnName("warrantyProvider_id")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.MachineCategory)
                    .WithMany(p => p.Warrantysupport)
                    .HasForeignKey(d => d.MachineCategoryId)
                    .HasConstraintName("FK6160FB135F04BDE7");

                entity.HasOne(d => d.Oem)
                    .WithMany(p => p.Warrantysupport)
                    .HasForeignKey(d => d.OemId)
                    .HasConstraintName("FK6160FB13298B7BA1");

                entity.HasOne(d => d.WarrantyProvider)
                    .WithMany(p => p.Warrantysupport)
                    .HasForeignKey(d => d.WarrantyProviderId)
                    .HasConstraintName("FK6160FB13BB6ECE2D");
            });

            modelBuilder.Entity<Workcoverdetail>(entity =>
            {
                entity.ToTable("workcoverdetail");

                entity.HasIndex(e => e.Lastupdatedon)
                    .HasName("lastupdatedon");

                entity.HasIndex(e => e.ServiceProviderId)
                    .HasName("FK25BEA6F78963B647");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ExpiryDate).HasColumnName("expiry_date");

                entity.Property(e => e.Lastupdatedon)
                    .HasColumnName("lastupdatedon")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OperationalInState)
                    .HasColumnName("operational_in_state")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("serviceProvider_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.Workcoverdetail)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .HasConstraintName("FK25BEA6F78963B647");
            });
        }
    }
}

