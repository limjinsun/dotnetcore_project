﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class ServiceProviderRepository : BaseRepository<Serviceprovider>
    {
        public ServiceProviderRepository(DataHubContext context, ILogger<ServiceProviderRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Serviceprovider serviceProvider)
        {
            return MyContext.Serviceprovider.Any(o => o.Name == serviceProvider.Name);
        }

        public override void UpdateAction(Serviceprovider serviceProvider, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            var existedServiceprovider = MyContext.Serviceprovider.SingleOrDefault(o => o.Name == serviceProvider.Name);
            if (existedServiceprovider != null)
            {
                if (serviceProvider.Email != null) existedServiceprovider.Email = serviceProvider.Email;
                if (serviceProvider.MobilePhoneNumber != null) existedServiceprovider.MobilePhoneNumber = serviceProvider.MobilePhoneNumber;
                if (serviceProvider.PhoneNumber != null) existedServiceprovider.PhoneNumber = serviceProvider.PhoneNumber;
                if (serviceProvider.Name != null) existedServiceprovider.Name = serviceProvider.Name;
                if (serviceProvider.AddressType != null) existedServiceprovider.AddressType = serviceProvider.AddressType;
                if (serviceProvider.MajorMunicipality != null) existedServiceprovider.MajorMunicipality = serviceProvider.MajorMunicipality;
                if (serviceProvider.MinorMunicipality != null) existedServiceprovider.MinorMunicipality = serviceProvider.MinorMunicipality;
                if (serviceProvider.PostalArea != null) existedServiceprovider.PostalArea = serviceProvider.PostalArea;
                if (serviceProvider.StreetName != null) existedServiceprovider.StreetName = serviceProvider.StreetName;
                if (serviceProvider.PersonalIndemnityInsuranceExpireDate != null) existedServiceprovider.PersonalIndemnityInsuranceExpireDate = serviceProvider.PersonalIndemnityInsuranceExpireDate;
                if (serviceProvider.PublicLiabilityInsuranceExpireDate != null) existedServiceprovider.PublicLiabilityInsuranceExpireDate = serviceProvider.PublicLiabilityInsuranceExpireDate;
                if (serviceProvider.ServiceProviderAbnNumber != null) existedServiceprovider.ServiceProviderAbnNumber = serviceProvider.ServiceProviderAbnNumber;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + serviceProvider);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = serviceProvider.ToString();
                commonDetails.UpdatedItem = serviceProvider;
            }
        }

        public override long GetIdByString(string s)
        {
            var serviceProvider = MyContext.Serviceprovider.SingleOrDefault(o => o.Name == s);
            return serviceProvider != null ? serviceProvider.Id : -1;
        }

    }
}
