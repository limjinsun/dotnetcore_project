﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class MachineRepository : BaseRepository<Machine>
    {
        public MachineRepository(DataHubContext context, ILogger<MachineRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Machine machine)
        {
            return MyContext.Machine.Any(o =>
                o.SerialNumber == machine.SerialNumber &&
                o.MachineMetaDataId == machine.MachineMetaDataId &&
                o.StoreId == machine.StoreId);
        }

        public override void UpdateAction(Machine machine, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            var existedMachine = MyContext.Machine.SingleOrDefault(o =>
                o.SerialNumber == machine.SerialNumber &&
                o.MachineMetaDataId == machine.MachineMetaDataId &&
                o.StoreId == machine.StoreId);

            if (existedMachine != null)
            {
                if (machine.SerialNumber != null) existedMachine.SerialNumber = machine.SerialNumber;
                if (machine.ExpiryDate != null) existedMachine.ExpiryDate = machine.ExpiryDate;
                if (machine.MachineMetaDataId != null) existedMachine.MachineMetaDataId = machine.MachineMetaDataId;
                if (machine.StationId != null) existedMachine.StationId = machine.StationId;
                if (machine.StoreId != null) existedMachine.StoreId = machine.StoreId;
                if (machine.FriendlyName != null) existedMachine.FriendlyName = machine.FriendlyName;
                if (machine.PurchasePrice != null) existedMachine.PurchasePrice = machine.PurchasePrice;
                if (machine.InstallationDate != null) existedMachine.InstallationDate = machine.InstallationDate;
                if (machine.WarrantyStartDate != null) existedMachine.WarrantyStartDate = machine.WarrantyStartDate;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + machine);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = machine.ToString();
                commonDetails.UpdatedItem = machine;
            }
        }
    }
}
