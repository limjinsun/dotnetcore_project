﻿using DataHubSheetImport.Models;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DataHubSheetImport.Repositories
{
    public class PartMetadataRepository : BaseRepository<Partmetadata>
    {
        public PartMetadataRepository(DataHubContext context, ILogger<PartMetadataRepository> logger) : base(context, logger)
        {
        }

        public override bool DoesEntityExist(Partmetadata partMetadata)
        {
            return MyContext.Partmetadata.Any(o => o.PartNumber == partMetadata.PartNumber);
        }

        public override void UpdateAction(Partmetadata partMetadata, DbProcessingResult processingResult, DbProcessingCommonDetail commonDetails,
            DbProcessingErrorWarningDetail errorDetails)
        {
            var existedPartmetadata = MyContext.Partmetadata.SingleOrDefault(o => o.PartNumber == partMetadata.PartNumber);
            if (existedPartmetadata != null)
            {
                if (partMetadata.OemDetailsId != null) existedPartmetadata.OemDetailsId = partMetadata.OemDetailsId;
                if (partMetadata.PartNumber != null) existedPartmetadata.PartNumber = partMetadata.PartNumber;
                if (partMetadata.Description != null) existedPartmetadata.Description = partMetadata.Description;
                if (partMetadata.MachineCategoryId != null) existedPartmetadata.MachineCategoryId = partMetadata.MachineCategoryId;
                MyContext.SaveChanges();
                Logger.LogDebug("Updating Success : " + partMetadata);
                processingResult.Status = ProcessStatus.Updated;

                commonDetails.DbModelName = partMetadata.ToString();
                commonDetails.UpdatedItem = partMetadata;
            }
        }

        public override long GetIdByString(string s)
        {
            var part = MyContext.Partmetadata.FirstOrDefault(o => o.PartNumber == s);
            return part != null? part.Id : -1;
        }
    }
}