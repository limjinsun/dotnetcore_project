﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataHubSheetImport.DatabaseConnection
{
    public static class DataHubConnectionManager
    {
        public static List<DataHubConnection> GetAllConnections()
        {
            List<DataHubConnection> result;
            using (var r = new StreamReader("dbconnections.json"))
            {
                var json = r.ReadToEnd();
                result = DataHubConnection.FromJson(json);
            }
            return result;
        }

        public static List<string> GetAllDbNames()
        {
            return GetAllConnections().Select(c => c.Name).ToList();
        }

        public static string GetConnectionString(string dbName)
        {
            return GetAllConnections().FirstOrDefault(c => c.Name == dbName)?.Dbconnection;
        }

        public static string GetDbCode(string dbName)
        {
            return GetAllConnections().FirstOrDefault(c => c.Name == dbName)?.Code;
        }
    }
}
