﻿namespace DataHubSheetImport.DatabaseConnection
{
    public static class DbManager
    {
        // static. to give access from Controller.
        public static string DbName;
        
        public static string GetDbConnectionString(string dbName)
        {
            return DataHubConnectionManager.GetConnectionString(dbName);
        }
    }
}
