﻿using System;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace DataHubSheetImport.ServiceManagement
{
    public interface IServiceInitializer
    {
        IDBService CreateDatabaseService(string sheetName);
        ISheetService CreateGoogleSheetService(string sheetName);
    }

    public class ServiceInitializer : IServiceInitializer
    {
        private static IServiceProvider _serviceProvider;

        public ServiceInitializer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ISheetService CreateGoogleSheetService(string sheetName)
        {
            switch (sheetName)
            {
                case "Site Users":
                    return _serviceProvider.GetService<IGoogleSheetService<SiteUsersDto>>(); 

                case "OEM":
                    return _serviceProvider.GetService<IGoogleSheetService<OemDto>>();

                case "Models":
                    return _serviceProvider.GetService<IGoogleSheetService<ModelsDto>>();

                case "Assets":
                    return _serviceProvider.GetService<IGoogleSheetService<AssetsDto>>();

                case "Asset Categories":
                    return _serviceProvider.GetService<IGoogleSheetService<AssetCategoriesDto>>();

                case "Location":
                    return _serviceProvider.GetService<IGoogleSheetService<LocationDto>>();

                case "Service Providers":
                    return _serviceProvider.GetService<IGoogleSheetService<ServiceProvidersDto>>();

                case "Sites":
                    return _serviceProvider.GetService<IGoogleSheetService<SitesDto>>();

                case "Service Provider Users":
                    return _serviceProvider.GetService<IGoogleSheetService<ServiceProviderUsersDto>>();

                default:
                    throw new ArgumentOutOfRangeException(nameof(sheetName));
            }
        }

        public IDBService CreateDatabaseService(string sheetName)
        {
            switch (sheetName)
            {
                case "Sites":
                    return _serviceProvider.GetService<IDatabaseService<SitesDto>>();

                case "Site Users":
                    return _serviceProvider.GetService<IDatabaseService<SiteUsersDto>>();

                case "OEM":
                    return _serviceProvider.GetService<IDatabaseService<OemDto>>();

                case "Models":
                    return _serviceProvider.GetService<IDatabaseService<ModelsDto>>();

                case "Assets":
                    return _serviceProvider.GetService<IDatabaseService<AssetsDto>>();

                case "Asset Categories":
                    return _serviceProvider.GetService<IDatabaseService<AssetCategoriesDto>>();

                case "Location":
                    return _serviceProvider.GetService<IDatabaseService<LocationDto>>();

                case "Service Providers":
                    return _serviceProvider.GetService<IDatabaseService<ServiceProvidersDto>>();

                case "Service Provider Users":
                    return _serviceProvider.GetService<IDatabaseService<ServiceProviderUsersDto>>();

                default:
                    throw new ArgumentOutOfRangeException(nameof(sheetName));
            }
        }
    }
}
