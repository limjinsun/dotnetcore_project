﻿using System.Collections.Generic;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Interfaces
{
    public interface IDatabaseService<TEntity> : IDBService where TEntity : class
    {
        List<DbProcessingResult> InsertDtoToDbTable(TEntity dto);
    }

    public interface IDBService
    {
        List<List<DbProcessingResult>> InsertDto(object dtoJson);
    }
}