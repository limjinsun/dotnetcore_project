﻿namespace DataHubSheetImport.Interfaces
{
    public interface IGoogleSheetService<TEntity> : ISheetService where TEntity : class 
    {
        //List<TEntity> GetDataFromSheet(string spreadsheetId
    }

    public interface ISheetService
    {
        object GetData(string spreadsheetId);
    }
}   
