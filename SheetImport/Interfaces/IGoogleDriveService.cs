﻿using Google.Apis.Drive.v2.Data;

namespace DataHubSheetImport.Interfaces
{
    public interface IGoogleDriveService
    {
        File GetFileData(string fileId);
    }
}
