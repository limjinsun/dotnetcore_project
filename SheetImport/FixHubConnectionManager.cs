﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Google.Apis.Auth.OAuth2;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DataHubSheetImport.Web.Api.Utils
{
    public static class DataHubConnectionManager
    {
        public static List<DataHubConnection> GetAllConnections()
        {
            List<DataHubConnection> result;
            using (StreamReader r = new StreamReader("dbConnections.json"))
            {
                string json = r.ReadToEnd();
                result = DataHubConnection.FromJson(json);
            }
            return result;
        }

        public static List<string> GetAllDbNames()
        {
            return GetAllConnections().Select(c => c.Name).ToList();
        }

        public static string GetConnectionString(string dbName)
        {
            return GetAllConnections().FirstOrDefault(c => c.Name == dbName)?.Dbconnection;
        }
    }
}
