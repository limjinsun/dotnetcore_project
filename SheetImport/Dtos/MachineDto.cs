﻿using System;

namespace DataHubSheetImport.Dtos
{
    public class MachineDto
    {
        public string SerialNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public long? MachineMetaDataId { get; set; }
        public long? StationId { get; set; }
        public long? StoreId { get; set; }
        public string FriendlyName { get; set; }
        public decimal? PurchasePrice { get; set; }
        public DateTime? InstallationDate { get; set; }
        public DateTime? WarrantyStartDate { get; set; }
    }
}
