﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class LocationDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Location { get; set; }

        public override string ToString()
        {
            return $"{nameof(Location)}: {Location}" + " - " + this.GetType().Name;
        }
    }
}
