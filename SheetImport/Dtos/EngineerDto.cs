﻿namespace DataHubSheetImport.Dtos
{
    public class EngineerDto
    {
        public long? ServiceProviderId { get; set; }
        public long? UserId { get; set; }
    }
}