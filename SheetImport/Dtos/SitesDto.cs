﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class SitesDto: BaseDto
    {
        private readonly BaseRepository<Users> _usersRepository;

        public SitesDto(BaseRepository<Users> usersRepository)
        {
            _usersRepository = usersRepository;
        }

        [JsonProperty(Order = 3)]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [Required]
        public string SiteId { get; set; }

        [JsonProperty(Order = 4)]
        [StringLength(128, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [Required]
        public string Name { get; set; }

        [JsonProperty(Order = 5)]
        [StringLength(48, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string AddressLine1 { get; set; }

        [JsonProperty(Order = 6)]
        [StringLength(64, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string AddressLine2 { get; set; }

        [JsonProperty(Order = 7)]
        public string City { get; set; }

        [JsonProperty(Order = 8)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string County { get; set; }

        [JsonProperty(Order = 9)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string PostCode { get; set; }

        [JsonProperty(Order = 10)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Country { get; set; }

        [JsonProperty(Order = 11)]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Telephone { get; set; }

        [JsonProperty(Order = 12)]
        [EmailAddress]
        public string SiteEmail { get; set; }

        [JsonProperty(Order = 13)]
        [Required]
        public string Manager { get; set; }

        [JsonProperty(Order = 14)]
        [StringLength(3, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [Required]
        [RegularExpression("Yes|No",
            ErrorMessage = "The {0} Value has to be 'Yes' or 'No'.")]
        public string SrApproval { get; set; }

        [JsonProperty(Order = 15)]
        [Required]
        [StringLength(3, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [RegularExpression("Yes|No",
            ErrorMessage = "The {0} Value has to be 'Yes' or 'No'.")]
        public string PmApproval { get; set; }

        [JsonProperty(Order = 16)]
        [Required]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string SiteNumber { get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}" + " - " + this.GetType().Name;
        }
    }
}