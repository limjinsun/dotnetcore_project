﻿namespace DataHubSheetImport.Dtos
{
    public class StoreDto : BaseDto
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string McDonaldsStoreId { get; set; }
        public string Name { get; set; }
        public bool RequiresServiceRequestApproval { get; set; }
        public string AddressType { get; set; }
        public string Country { get; set; }
        public string MajorMunicipality { get; set; }
        public string MinorMunicipality { get; set; }
        public string PostalArea { get; set; }
        public string StreetName { get; set; }
        public string StoreNumber { get; set; }
        public long? OwnerId { get; set; }
        public string Password { get; set; }
        public bool RequiresServiceRequestPmApproval { get; set; }
    }
}

