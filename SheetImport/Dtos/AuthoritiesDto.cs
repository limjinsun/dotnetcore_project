﻿namespace DataHubSheetImport.Dtos
{
    public class AuthoritiesDto : BaseDto
    {
        public string Authority { get; set; }
        public long? UserId { get; set; }
    }
}

