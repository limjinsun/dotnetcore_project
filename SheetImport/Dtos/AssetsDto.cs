﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class AssetsDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        public string Site { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        public string Location { get; set; }

        [JsonProperty(Order = 5)]
        public string Manufacturer { get; set; }

        [JsonProperty(Order = 6)]
        [Required]
        public string PartNumber { get; set; }

        [JsonProperty(Order = 7)]
        public string Description { get; set; }

        [JsonProperty(Order = 8)]
        public string AssetType { get; set; }

        [JsonProperty(Order = 9)]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string FriendlyName { get; set; }

        [JsonProperty(Order = 10)]
        [Required]
        public string SerialNumber { get; set; }

        [JsonProperty(Order = 11)]
        public string InstallDate { get; set; }

        [JsonProperty(Order = 12)]
        public string WarrantyStartDate { get; set; }

        [JsonProperty(Order = 13)]
        public string WarrantyExpiryDate { get; set; }

        [JsonProperty(Order = 14)]
        public string PurchaseCost { get; set; }

        [JsonProperty(Order = 15)]
        public string DepTerms { get; set; }

        [JsonProperty(Order = 16)]
        public string NextPmDueDate { get; set; }

        [JsonProperty(Order = 17)]
        public string PMFreque { get; set; }

        [JsonProperty(Order = 18)]
        public string Comments { get; set; }

        public override string ToString()
        {
            return $"{nameof(SerialNumber)}: {SerialNumber}" + " - " + this.GetType().Name;
        }
    }
}