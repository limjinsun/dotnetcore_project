﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class BaseDto
    {
        [JsonProperty(Order = 0)]
        public int RowNumber;
        [JsonProperty(Order = 1)]
        public bool IsValid { get; set; }
        [JsonProperty(Order = 2)]
        public object ValidationMessage { get; set; }

        public BaseDto()
        {
            ValidationMessage = new List<object>();
        }

        public virtual void CheckValidity()
        {
            var context = new ValidationContext(this);
            var results = new List<ValidationResult>();
            IsValid = Validator.TryValidateObject(this, context, results, true);
            ValidationMessage = results;
        }
    }
}