﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class AssetCategoriesDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        [StringLength(16, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string AssetCategory { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [RegularExpression("^[A-Z.\\s_@./#&+]+$",
            ErrorMessage = "The {0} Value has to be certain format")] // Regex expression only allow - Uppercase and Under-Bar
        public string CategoryCode { get; set; }

        public override string ToString()
        {
            return $"{nameof(CategoryCode)}: {CategoryCode}" + " - " + this.GetType().Name;
        }
    }
}
