﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class ModelsDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        public string Manufacturer { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string ModelNumber { get; set; }

        [JsonProperty(Order = 5)]
        [Required]
        public string Description { get; set; }

        [JsonProperty(Order = 6)]
        [Required]
        public string AssetCategory { get; set; }

        public override string ToString()
        {
            return $"{nameof(Manufacturer)}: {Manufacturer}, {nameof(ModelNumber)}: {ModelNumber}" + " - " + this.GetType().Name;
        }
    }
}