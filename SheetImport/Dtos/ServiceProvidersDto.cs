﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class ServiceProvidersDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        [StringLength(64, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string ServiceCompanyName { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        [EmailAddress]
        [StringLength(128, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Email { get; set; }

        [JsonProperty(Order = 5)]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Phone { get; set; }

        [JsonProperty(Order = 6)]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Mobile { get; set; }

        [JsonProperty(Order = 7)]
        [StringLength(48, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string AddressLine1 { get; set; }

        [JsonProperty(Order = 8)]
        [StringLength(64, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string AddressLine2 { get; set; }

        [JsonProperty(Order = 9)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string City { get; set; }

        [JsonProperty(Order = 10)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string County { get; set; }

        [JsonProperty(Order = 11)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string PostCode { get; set; }

        [JsonProperty(Order = 12)]
        [StringLength(32, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Country { get; set; }

        [JsonProperty(Order = 13)]
        [StringLength(64, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string CompanyNumber { get; set; }

        [JsonProperty(Order = 14)]
        public DateTime PublicLiabilityInsuranceExpireDate { get; set; }

        [JsonProperty(Order = 15)]
        public DateTime IdemnityInsuranceExpireDate { get; set; }

        public override string ToString()
        {
            return $"{nameof(Email)}: {Email}" + " - " + this.GetType().Name;
        }
    }
}

