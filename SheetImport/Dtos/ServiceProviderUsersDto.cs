﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class ServiceProviderUsersDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string FirstName { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string LastName { get; set; }

        [JsonProperty(Order = 5)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string UserName { get; set; }

        [JsonProperty(Order = 6)]
        [EmailAddress]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Email { get; set; }

        [JsonProperty(Order = 7)]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Mobile { get; set; }

        [JsonProperty(Order = 8)]
        [Required]
        public string Company { get; set; }

        [JsonProperty(Order = 9)]
        [Required]
        [RegularExpression("Service_Controller|Engineer",
            ErrorMessage = "The {0} Value has to be one of following : Service_Controller / Engineer")]
        public string Role { get; set; }

        public override string ToString()
        {
            return $"{nameof(UserName)}: {UserName}" + " - " + this.GetType().Name;
        }
    }
}