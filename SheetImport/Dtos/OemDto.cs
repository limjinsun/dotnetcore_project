﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class OemDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [StringLength(128, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Manufacture { get; set; }

        public override string ToString()
        {
            return $"{nameof(Manufacture)}: {Manufacture}" + " - " + this.GetType().Name;
        }
    }
}