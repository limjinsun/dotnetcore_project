﻿namespace DataHubSheetImport.Dtos
{
    public class SheetInfo
    {
        public string SpreadsheetId { get; set; }
        public string SheetName { get; set; }
        public string Range { get; set; }
    }
}


