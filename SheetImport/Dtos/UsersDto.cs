﻿namespace DataHubSheetImport.Dtos
{
    public class UsersDto : BaseDto
    {
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string Username { get; set; }
        public long? ProviderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

