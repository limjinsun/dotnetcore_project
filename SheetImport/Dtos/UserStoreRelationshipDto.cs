﻿namespace DataHubSheetImport.Dtos
{
    public class UserStoreRelationshipDto
    {
        public long StoreId { get; set; }
        public long UserId { get; set; }
    }
}

