﻿namespace DataHubSheetImport.Dtos
{
    public class PartMetadataDto
    {
        public long OemDetailsId { get; set; }
        public string ModelNumber { get; set; }
        public string Description { get; set; }
        public long MachineCategoryId { get; set; }
    }
}