﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataHubSheetImport.Dtos
{
    public class SiteUsersDto : BaseDto
    {
        [JsonProperty(Order = 3)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string FirstName { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string LastName { get; set; }

        [JsonProperty(Order = 5)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Username { get; set; }

        [JsonProperty(Order = 6)]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [EmailAddress]
        public string Email { get; set; }

        [JsonProperty(Order = 7)]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Mobile { get; set; }

        [JsonProperty(Order = 8)]
        [Required]
        public string Site { get; set; }

        [JsonProperty(Order = 9)]
        [Required]
        [StringLength(255, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        [RegularExpression("Store Manager|Store Approver|Corporate User|System Admin|System Contract Admin|Cost Approver|Phone Support|Corporate|Oem User", 
            ErrorMessage = "The {0} Value has to be one of following : Store Manager / Store Approver / Corporate User / System Admin / System Contract Admin / Cost Approver / Phone Support / Corporate / Oem User ")]
        public string Role { get; set; }

        public override string ToString()
        {
            return $"{nameof(Username)}: {Username}" + " - " + this.GetType().Name;
        }
    }
}

