﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class AssetsSheetService : BaseGoogleSheetService<AssetsDto>, IGoogleSheetService<AssetsDto>
    {
        public AssetsSheetService()
        {
            Range = "Assets";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<AssetsDto> dtos)
        {
            for (int i = 3; i < values.Count; i++)
            {
                var dataRow = values[i];
                var assetsDto = new AssetsDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Site":
                                assetsDto.Site = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Location":
                                assetsDto.Location = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Manufacturer/OEM":
                                assetsDto.Manufacturer = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Part Number/Model":
                                assetsDto.PartNumber = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Description":
                                assetsDto.Description = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Asset Type(Category)":
                                assetsDto.AssetType = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Unique/Friendly Name":
                                assetsDto.FriendlyName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Serial Number":
                                assetsDto.SerialNumber = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Commission/Install Date":
                                assetsDto.InstallDate = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Warranty Start Date":
                                assetsDto.WarrantyStartDate = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Warranty Expiry Date":
                                assetsDto.WarrantyExpiryDate = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Purchase Cost":
                                assetsDto.PurchaseCost = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Dep. Terms (M)":
                                assetsDto.DepTerms = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Next PM Due Date":
                                assetsDto.NextPmDueDate = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "PM Freque":
                                assetsDto.PMFreque = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Comments":
                                assetsDto.Comments = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;

                        }
                    }
                }

                assetsDto.RowNumber = i + 1;
                assetsDto.CheckValidity();
                dtos.Add(assetsDto);
            }
        }
    }
}
