﻿using System.Collections.Generic;
using DataHubSheetImport.Repositories;
using DataHubSheetImport.ServiceManagement;
using Newtonsoft.Json;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public interface IAllSheetImportService
    {
        List<List<List<DbProcessingResult>>> ImportAllDataFromSheet(string sheetId);
    }

    public class AllSheetImportService : IAllSheetImportService
    {
        private readonly IServiceInitializer _serviceInitializer;
        public AllSheetImportService(IServiceInitializer serviceInitializer)
        {
            _serviceInitializer = serviceInitializer;
        }

        /**
         *
         * Import Order.
         *
         * "OEM","Asset Categories","Models","Location","Site Users","Sites","Assets","Service Providers","Service Provider Users"
         * 
         */
        public List<List<List<DbProcessingResult>>> ImportAllDataFromSheet(string sheetId)
        {
            var result = new List<List<List<DbProcessingResult>>>();
            var sheetList = new List<string>
            {
                "OEM","Asset Categories","Models","Location","Site Users","Sites","Assets","Service Providers","Service Provider Users"
            };
            foreach (var sheet in sheetList)
            {
                var dtos = _serviceInitializer.CreateGoogleSheetService(sheet).GetData(sheetId);
                var serializedDtos = JsonConvert.SerializeObject(dtos);
                result.Add(_serviceInitializer.CreateDatabaseService(sheet).InsertDto(serializedDtos));
            }

            return result;
        }
    }
}
