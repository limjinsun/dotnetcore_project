﻿using System.Collections.Generic;
using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public abstract class BaseGoogleSheetService<TEntity> where TEntity : class
    {
        public static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        public static SheetsService Service;
        protected string Range { get; set; }

        public object GetData(string spreadsheetId)
        {
            return GetDataFromSheet(spreadsheetId);
        }

        public List<TEntity> GetDataFromSheet(string spreadsheetId)
        {
            GoogleCredential credential;
            using (var stream = new FileStream("OnBoarding-Google-Secret.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(Scopes);
            }

            var dtos = ReadEntries(credential, spreadsheetId);
            return dtos;
        }

        protected virtual List<TEntity> ReadEntries(GoogleCredential credential, string spreadsheetId)
        {
            var dtos = new List<TEntity>();
            Service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });

            var request = Service.Spreadsheets.Values.Get(spreadsheetId, Range);
            var response = request.Execute();
            var values = response.Values;

            if (values != null && values.Count > 0)
            {
                var headerRow = values[1];
                ReadAction(values, headerRow, dtos);
            }

            return dtos;
        }

        public abstract void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<TEntity> dtos);

        public static string GetStringValueIfNotEmpty(string s)
        {
            return string.IsNullOrEmpty(s) ? null : s;
        }
    }
}
