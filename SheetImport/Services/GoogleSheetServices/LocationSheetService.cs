﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class LocationSheetService : BaseGoogleSheetService<LocationDto>, IGoogleSheetService<LocationDto>
    {
        public LocationSheetService()
        {
            Range = "Location";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<LocationDto> dtos)
        {
            for (int i = 2; i < values.Count; i++) // real data start from 3rd row.
            {
                var dataRow = values[i];
                var locationDto = new LocationDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Location":
                                locationDto.Location = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                locationDto.RowNumber = i + 1;
                locationDto.CheckValidity();
                dtos.Add(locationDto);
            }
        }
    }
}
