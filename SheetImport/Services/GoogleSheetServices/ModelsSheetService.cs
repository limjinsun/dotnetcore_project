﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class ModelsSheetService : BaseGoogleSheetService<ModelsDto>, IGoogleSheetService<ModelsDto>
    {
        public ModelsSheetService()
        {
            Range = "Models";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<ModelsDto> dtos)
        {
            for (var i = 2; i < values.Count; i++) // real data start from 3rd row.
            {
                var dataRow = values[i];
                var modelsDto = new ModelsDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Manufacturer":
                                modelsDto.Manufacturer = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Model Number":
                                modelsDto.ModelNumber = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Description":
                                modelsDto.Description = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Asset Category":
                                modelsDto.AssetCategory = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                modelsDto.RowNumber = i + 1;
                modelsDto.CheckValidity();
                dtos.Add(modelsDto);
            }
        }
    }
}
