﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class OemSheetService : BaseGoogleSheetService<OemDto>, IGoogleSheetService<OemDto>
    {
        public OemSheetService()
        {
            Range = "OEM";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<OemDto> dtos)
        {
            for (int i = 2; i < values.Count; i++) // real data start from 3rd row.
            {
                var dataRow = values[i];
                var oemDto = new OemDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Manufacturer":
                                oemDto.Manufacture = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                oemDto.RowNumber = i + 1;
                oemDto.CheckValidity();
                dtos.Add(oemDto);
            }
        }
    }
}
