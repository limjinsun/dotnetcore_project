﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class SiteUsersSheetService : BaseGoogleSheetService<SiteUsersDto>, IGoogleSheetService<SiteUsersDto>
    {
        public SiteUsersSheetService ()
        {
            Range = "Site Users";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<SiteUsersDto> dtos)
        {
            for (var i = 2; i < values.Count; i++) // real data start from 3rd row.
            {
                var dataRow = values[i];
                var user = new SiteUsersDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "First Name":
                                user.FirstName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Last Name":
                                user.LastName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Username":
                                user.Username = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Email":
                                user.Email = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Mobile":
                                user.Mobile = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Site":
                                user.Site = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Role":
                                user.Role = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                user.RowNumber = i + 1;
                user.CheckValidity();
                dtos.Add(user);
            }
        }
    }
}
