﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class SitesSheetService : BaseGoogleSheetService<SitesDto>, IGoogleSheetService<SitesDto>
    {

        private readonly BaseRepository<Users> _usersRepository;

        public SitesSheetService(BaseRepository<Users> usersRepository)
        {
            _usersRepository = usersRepository;
            Range = "Sites";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<SitesDto> dtos)
        {
            for (int i = 3; i < values.Count; i++) // real data start from 4th row.
            {
                var dataRow = values[i];
                var sitesDto = new SitesDto(_usersRepository);
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Site ID":
                                sitesDto.SiteId = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Name":
                                sitesDto.Name = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Address line 1":
                                sitesDto.AddressLine1 = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Address line 2":
                                sitesDto.AddressLine2 = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "City":
                                sitesDto.City = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "County":
                                sitesDto.County = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Post Code":
                                sitesDto.PostCode = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Country":
                                sitesDto.Country = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Telephone":
                                sitesDto.Telephone = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Site Email":
                                sitesDto.SiteEmail = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Manager/Owner":
                                sitesDto.Manager = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "SR Approval":
                                sitesDto.SrApproval = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "PM Approval":
                                sitesDto.PmApproval = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Site Number":
                                sitesDto.SiteNumber = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                sitesDto.RowNumber = i + 1;
                sitesDto.CheckValidity();
                dtos.Add(sitesDto);
            }
        }
    }
}