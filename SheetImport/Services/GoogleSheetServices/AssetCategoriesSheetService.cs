﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class AssetCategoriesSheetService : BaseGoogleSheetService<AssetCategoriesDto>, IGoogleSheetService<AssetCategoriesDto>
    {
        public AssetCategoriesSheetService()
        {
            Range = "Asset Categories";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<AssetCategoriesDto> dtos)
        {
            for (int i = 2; i < values.Count; i++) // real data start from 3rd row.
            {
                var dataRow = values[i];
                var assetCategoriesDto = new AssetCategoriesDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Asset Category":
                                assetCategoriesDto.AssetCategory = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Category Code":
                                assetCategoriesDto.CategoryCode = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                assetCategoriesDto.RowNumber = i + 1;
                assetCategoriesDto.CheckValidity();
                dtos.Add(assetCategoriesDto);
            }
        }
    }
}
