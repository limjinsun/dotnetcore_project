﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class ServiceProviderUsersSheetService : BaseGoogleSheetService<ServiceProviderUsersDto>, IGoogleSheetService<ServiceProviderUsersDto>
    {
        public ServiceProviderUsersSheetService()
        {
            Range = "Service Provider Users";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<ServiceProviderUsersDto> dtos)
        {
            for (int i = 2; i < values.Count; i++) // real data start from 3rd row.
            {
                var dataRow = values[i];
                var serviceProviderUsersDto = new ServiceProviderUsersDto();
                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "First Name":
                                serviceProviderUsersDto.FirstName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Last Name":
                                serviceProviderUsersDto.LastName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Username":
                                serviceProviderUsersDto.UserName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Email":
                                serviceProviderUsersDto.Email = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Mobile":
                                serviceProviderUsersDto.Mobile = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Company":
                                serviceProviderUsersDto.Company = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Role":
                                serviceProviderUsersDto.Role = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                        }
                    }
                }

                serviceProviderUsersDto.RowNumber = i + 1;
                serviceProviderUsersDto.CheckValidity();
                dtos.Add(serviceProviderUsersDto);
            }
        }
    }
}
