﻿using System.Collections.Generic;
using DataHubSheetImport.ServiceManagement;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public interface IAllSheetValidateService
    {
        List<GoogleSheetData> ValidateAllDataFromSheet(string sheetId);
    }

    public class AllSheetValidateService : IAllSheetValidateService
    {
        private readonly IServiceInitializer _serviceInitializer;
        public AllSheetValidateService(IServiceInitializer serviceInitializer)
        {
            _serviceInitializer = serviceInitializer;
        }

        /**
         * Importing has to be this order.
         *
         * "OEM","Asset Categories","Models","Location","Site Users","Sites","Assets","Service Providers","Service Provider Users"
         *
         */
        public List<GoogleSheetData> ValidateAllDataFromSheet(string sheetId)
        {
            var sheetList = new List<string>
            {
                "OEM","Asset Categories","Models","Location","Site Users","Sites","Assets","Service Providers","Service Provider Users"
            };
            var gList = new List<GoogleSheetData>();

            foreach (var sheet in sheetList)
            {
                var dtos = _serviceInitializer.CreateGoogleSheetService(sheet).GetData(sheetId);
                gList.Add(new GoogleSheetData()
                {
                    Dto = dtos,
                    SheetName = sheet
                });
            }
            
            return gList;
        }
    }

    public class GoogleSheetData
    {
        public object Dto { get; set; }
        public string SheetName { get; set; }
    }
}