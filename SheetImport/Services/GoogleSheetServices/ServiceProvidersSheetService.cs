﻿using System;
using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class ServiceProvidersSheetService : BaseGoogleSheetService<ServiceProvidersDto>, IGoogleSheetService<ServiceProvidersDto>
    {
        public ServiceProvidersSheetService()
        {
            Range = "Service Providers";
        }

        public override void ReadAction(IList<IList<object>> values, IList<object> headerRow, List<ServiceProvidersDto> dtos)
        {
            for (int i = 3; i < values.Count; i++) // real data start from 4th row.
            {
                var dataRow = values[i];
                var serviceProvidersDto = new ServiceProvidersDto();

                for (var j = 0; j < dataRow.Count; j++)
                {
                    if (headerRow != null && headerRow.Count > j)
                    {
                        switch ((string)headerRow[j])
                        {
                            case "Service Company Name":
                                serviceProvidersDto.ServiceCompanyName = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Email":
                                serviceProvidersDto.Email = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Phone":
                                serviceProvidersDto.Phone = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Mobile":
                                serviceProvidersDto.Mobile = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Address line 1":
                                serviceProvidersDto.AddressLine1 = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Address line 2":
                                serviceProvidersDto.AddressLine2 = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "City":
                                serviceProvidersDto.City = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "County":
                                serviceProvidersDto.County = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Post Code":
                                serviceProvidersDto.PostCode = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Country":
                                serviceProvidersDto.Country = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Company Number":
                                serviceProvidersDto.CompanyNumber = GetStringValueIfNotEmpty(dataRow[j].ToString());
                                break;
                            case "Public Liability Insurance Expiry Date":
                                if (!string.IsNullOrEmpty(dataRow[j].ToString()))
                                {
                                    serviceProvidersDto.PublicLiabilityInsuranceExpireDate = DateTime.ParseExact(dataRow[j].ToString(), "dd/MM/yyyy", null);
                                }
                                break;
                            case "Idemnity Insurance Expiry Date":
                                if (!string.IsNullOrEmpty(dataRow[j].ToString()))
                                {
                                    serviceProvidersDto.IdemnityInsuranceExpireDate = DateTime.ParseExact(dataRow[j].ToString(), "dd/MM/yyyy", null);
                                }
                                break;
                        }
                    }
                }

                serviceProvidersDto.RowNumber = i + 1;
                serviceProvidersDto.CheckValidity();
                dtos.Add(serviceProvidersDto);
            }
        }
    }
}
