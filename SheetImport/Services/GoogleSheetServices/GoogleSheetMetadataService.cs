﻿using System.Collections.Generic;
using Google.Apis.Sheets.v4;
using System.IO;
using System.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4.Data;

namespace DataHubSheetImport.Services.GoogleSheetServices
{
    public class GoogleSheetMetadataService
    {
        protected static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        protected static SheetsService Service;
        protected string Range { get; set; }
        protected GoogleCredential Credential;

        private IList<Sheet> SheetMetaInfoArray(string spreadsheetId)
        {
            using (var stream = new FileStream("OnBoarding-Google-Secret.json", FileMode.Open, FileAccess.Read))
            {
                Credential = GoogleCredential.FromStream(stream).CreateScoped(Scopes);
            }

            Service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Credential
            });

            var request = Service.Spreadsheets.Get(spreadsheetId);
            var response = request.Execute();
            var sheetMetaInfoArray = response.Sheets;
            return sheetMetaInfoArray;
        }

        public object GetMetaInformation(string spreadsheetId)
        {
            var sheetMetaInfoArray = SheetMetaInfoArray(spreadsheetId);
            return sheetMetaInfoArray;
        }

        public object GetMetaInformationWithSheetName(string spreadsheetId, string sheetName)
        {
            var sheetMetaInfoArray = SheetMetaInfoArray(spreadsheetId);
            return sheetMetaInfoArray.Where(item => item.Properties.Title == sheetName).ToArray();
        }
    }
}
