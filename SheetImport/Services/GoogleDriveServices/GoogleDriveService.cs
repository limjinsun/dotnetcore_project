﻿using System;
using System.IO;
using DataHubSheetImport.Interfaces;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using File = Google.Apis.Drive.v2.Data.File;

namespace DataHubSheetImport.Services.GoogleDriveServices
{
    // This class has been made only for getting the file's name. no other purpose.  
    public class GoogleDriveService : IGoogleDriveService
    {
        protected static readonly string[] Scopes = { DriveService.Scope.Drive };
        protected static DriveService Service;

        public File GetFileData(string fileId)
        {
            GoogleCredential credential;
            File file = null;
            using (var stream = new FileStream("OnBoarding-Google-Secret.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped( Scopes );
            }

            // Make Service. 
            Service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });

            try
            {
                file = Service.Files.Get(fileId).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }

            return file;
        }
    }
}
