﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class ModelsDbService : BaseDatabaseService<ModelsDto>, IDatabaseService<ModelsDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Partmetadata> _partMetadataRepository;
        private readonly BaseRepository<Oemdetails> _oemDetailsRepository;
        private readonly BaseRepository<Machinecategory> _machineCategoryRepository;

        public ModelsDbService(IMapper mapper, BaseRepository<Partmetadata> partMetadataRepository, BaseRepository<Oemdetails> oemDetailsRepository, BaseRepository<Machinecategory> machineCategoryRepository)
        {
            _mapper = mapper;
            _partMetadataRepository = partMetadataRepository;
            _oemDetailsRepository = oemDetailsRepository;
            _machineCategoryRepository = machineCategoryRepository;
        }
        public override List<DbProcessingResult> InsertDtoToDbTable(ModelsDto modelsDto)
        {
            var processingResultList = new List<DbProcessingResult>();
            // 1.Change modelsDto -> partMetadataDto
            var partMetadataDto = MapModelsDtoToPartMetadataDto(modelsDto);
            // 2.Then partMetadataDto -> partMetadata Model
            var mappedPartMetadata = _mapper.Map<Partmetadata>(partMetadataDto);
            // 3. Try Add or Update.
            var result = _partMetadataRepository.DoesEntityExist(mappedPartMetadata) ? _partMetadataRepository.UpdateEntity(mappedPartMetadata) : _partMetadataRepository.AddEntity(mappedPartMetadata);

            processingResultList.Add(result);
            return processingResultList;
        }

        // app will crash when id = -1
        private PartMetadataDto MapModelsDtoToPartMetadataDto(ModelsDto modelsDto)
        {
            var partMetadataDto = new PartMetadataDto
            {
                OemDetailsId = _oemDetailsRepository.GetIdByString(modelsDto.Manufacturer),
                ModelNumber = modelsDto.ModelNumber,
                Description = modelsDto.Description,
                MachineCategoryId = _machineCategoryRepository.GetIdByString(modelsDto.AssetCategory)
            };

            return partMetadataDto;
        }
    }
}
