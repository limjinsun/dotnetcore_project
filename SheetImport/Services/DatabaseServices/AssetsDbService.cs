﻿using System;
using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class AssetsDbService : BaseDatabaseService<AssetsDto>,IDatabaseService<AssetsDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Partmetadata> _partMetadataRepository;
        private readonly BaseRepository<Station> _stationRepository;
        private readonly BaseRepository<Machine> _machineRepository;
        private readonly BaseRepository<Store> _storeRepository;

        public AssetsDbService(IMapper mapper, BaseRepository<Partmetadata> partMetadataRepository, BaseRepository<Station> stationRepository, BaseRepository<Machine> machineRepository, BaseRepository<Store> storeRepository)
        {
            _mapper = mapper;
            _partMetadataRepository = partMetadataRepository;
            _stationRepository = stationRepository;
            _machineRepository = machineRepository;
            _storeRepository = storeRepository;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(AssetsDto assetsDto)
        {
            var processingResultList = new List<DbProcessingResult>();
            // 1. change assetsDto -> machineDto
            var machineDto = MapAssetsDtoToMachineDto(assetsDto);
            // 2. then machineDto -> machine Model. 
            var mappedMachine = _mapper.Map<Machine>(machineDto);

            var result = _machineRepository.DoesEntityExist(mappedMachine) ? _machineRepository.UpdateEntity(mappedMachine) : _machineRepository.AddEntity(mappedMachine);

            processingResultList.Add(result);
            return processingResultList;
        }

        // app will crash when Id == -1;
        private MachineDto MapAssetsDtoToMachineDto(AssetsDto assetsDto)
        {
            var machineDto = new MachineDto();

            var oDate = Convert.ToDateTime(assetsDto.WarrantyExpiryDate);
            var oDecimalVal = Convert.ToDecimal(assetsDto.PurchaseCost);
            var oDate2 = Convert.ToDateTime(assetsDto.InstallDate);
            var oDate3 = Convert.ToDateTime(assetsDto.WarrantyExpiryDate);

            machineDto.SerialNumber = assetsDto.SerialNumber;
            machineDto.ExpiryDate = oDate;
            machineDto.MachineMetaDataId = _partMetadataRepository.GetIdByString(assetsDto.PartNumber);
            machineDto.StationId = _stationRepository.GetIdByString(assetsDto.Location);
            machineDto.StoreId = _storeRepository.GetIdByString(assetsDto.Site);
            machineDto.FriendlyName = assetsDto.FriendlyName;
            machineDto.PurchasePrice = oDecimalVal;
            machineDto.InstallationDate = oDate2;
            machineDto.WarrantyStartDate = oDate3;

            return machineDto;
        }
    }
}
