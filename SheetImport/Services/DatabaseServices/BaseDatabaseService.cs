﻿using System.Collections.Generic;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Repositories;
using Newtonsoft.Json;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public abstract class BaseDatabaseService<TEntity> where TEntity : class
    {
        public abstract List<DbProcessingResult> InsertDtoToDbTable(TEntity dto); 
        public List<List<DbProcessingResult>> InsertDto(object dtoJson)
        {
            var dtos = JsonConvert.DeserializeObject<List<TEntity>>(dtoJson.ToString());
            var result = new List<List<DbProcessingResult>>();
            foreach (var dto in dtos)
            {
                DtoTracker.CurrentDto = dto;
                DtoTracker.DtoPresentName = dto.ToString();
                result.Add(InsertDtoToDbTable(dto)); ;
            }

            return result;
        }
    }
}
