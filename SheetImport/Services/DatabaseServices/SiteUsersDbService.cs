﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class SiteUsersDbService : BaseDatabaseService<SiteUsersDto>, IDatabaseService<SiteUsersDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Users> _userRepository;
        private readonly BaseRepository<Authorities> _authoritiesRepository;
        private readonly BaseRepository<Store> _storeRepository;
        private readonly BaseRepository<Userstorerelationship> _userStoreRelationshipRepository;
        private readonly BaseRepository<Userstore> _userStoreRepository;

        public SiteUsersDbService(IMapper mapper, BaseRepository<Users> userRepository, BaseRepository<Authorities> authoritiesRepository, BaseRepository<Userstorerelationship> userStoreRelationshipRepository, BaseRepository<Store> storeRepository, BaseRepository<Userstore> userStoreRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _authoritiesRepository = authoritiesRepository;
            _userStoreRelationshipRepository = userStoreRelationshipRepository;
            _storeRepository = storeRepository;
            _userStoreRepository = userStoreRepository;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(SiteUsersDto siteUsersDto)
        {
            var list = new List<DbProcessingResult>();

            // 1. insert into users table.
            list.Add(InsertDtoToUsersTable(siteUsersDto));
            // 2. insert into authorities table.
            list.Add(InsertDtoToAuthoritiesTable(siteUsersDto));
            // 2-1 insert into store table.
            list.Add(InsertDtoToStoreTable(siteUsersDto));
            // 3. insert into userstorerelationship table.
            list.Add(InsertDtoToUserStoreRelationShipTable(siteUsersDto));
            // 3-1. insert into userstore table only if siteUsers-dto has role as `Store Manager` or `Store Approver` or 'Cost Approver'.
            if (siteUsersDto.Role == "Store Manager" || siteUsersDto.Role == "Store Approver" || siteUsersDto.Role == "Cost Approver")
            {
                list.Add(InsertDtoToUserStoreTable(siteUsersDto));
            }
            return list;
        }

        private DbProcessingResult InsertDtoToStoreTable(SiteUsersDto siteUsersDto)
        {
            var mappedStore = _mapper.Map<Store>(siteUsersDto);
            return _storeRepository.DoesEntityExist(mappedStore) ? _storeRepository.UpdateEntity(mappedStore) : _storeRepository.AddEntity(mappedStore);
        }

        private DbProcessingResult InsertDtoToUsersTable (SiteUsersDto siteUsersDto)
        {
            var mappedUser = _mapper.Map<Users>(siteUsersDto);
            return _userRepository.DoesEntityExist(mappedUser) ? _userRepository.UpdateEntity(mappedUser) : _userRepository.AddEntity(mappedUser);
        }

        private DbProcessingResult InsertDtoToAuthoritiesTable(SiteUsersDto siteUsersDto)
        {
            var authoritiesDto = MapSiteUsersDtoToAuthoritiesDto(siteUsersDto);
            var mappedAuthorities = _mapper.Map<Authorities>(authoritiesDto);
            return _authoritiesRepository.DoesEntityExist(mappedAuthorities) ? _authoritiesRepository.UpdateEntity(mappedAuthorities) : _authoritiesRepository.AddEntity(mappedAuthorities);
        }

        private DbProcessingResult InsertDtoToUserStoreRelationShipTable(SiteUsersDto siteUsersDto)
        {
            var userStoreRelationshipDto = MapSiteUsersDtoToUserStoreRelationshipDto(siteUsersDto);
            var mappedUserstorerelationship = _mapper.Map<Userstorerelationship>(userStoreRelationshipDto);
            return _userStoreRelationshipRepository.DoesEntityExist(mappedUserstorerelationship) ? _userStoreRelationshipRepository.UpdateEntity(mappedUserstorerelationship) : _userStoreRelationshipRepository.AddEntity(mappedUserstorerelationship);
        }

        private AuthoritiesDto MapSiteUsersDtoToAuthoritiesDto(SiteUsersDto siteUsersDto)
        {
            return new AuthoritiesDto()
            {
                Authority = siteUsersDto.Role.ToUpper().Replace(' ', '_'),
                UserId = _userRepository.GetIdByString(siteUsersDto.Username)
            };
        }
        
        private UserStoreRelationshipDto MapSiteUsersDtoToUserStoreRelationshipDto(SiteUsersDto siteUsersDto)
        {
            return new UserStoreRelationshipDto()
            {
                StoreId = _storeRepository.GetIdByString(siteUsersDto.Site),
                UserId = _userRepository.GetIdByString(siteUsersDto.Username)
            };
        }

        private DbProcessingResult InsertDtoToUserStoreTable(SiteUsersDto siteUsersDto)
        {
            var userStore = MapSiteUsersDtoToUserStoreDto(siteUsersDto);
            return _userStoreRepository.DoesEntityExist(userStore) ? _userStoreRepository.UpdateEntity(userStore) : _userStoreRepository.AddEntity(userStore);
        }

        private Userstore MapSiteUsersDtoToUserStoreDto(SiteUsersDto siteUsersDto)
        {
            return new Userstore()
            {
                StoreId = _storeRepository.GetIdByString(siteUsersDto.Site),
                UserId = _userRepository.GetIdByString(siteUsersDto.Username)
            };
        }

    }
}