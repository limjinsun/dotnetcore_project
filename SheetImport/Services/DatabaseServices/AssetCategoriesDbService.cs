﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class AssetCategoriesDbService : BaseDatabaseService<AssetCategoriesDto>, IDatabaseService<AssetCategoriesDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Machinecategory> _machineCategoryRepository;
        private readonly DataHubContext _myContext;

        public AssetCategoriesDbService(IMapper mapper, BaseRepository<Machinecategory> machineCategoryRepository, DataHubContext myContext)
        {
            _mapper = mapper;
            _machineCategoryRepository = machineCategoryRepository;
            _myContext = myContext;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(AssetCategoriesDto assetCategoriesDto)
        {
            var processingResultList = new List<DbProcessingResult>();
            // In order to determine unique item in machinecategory table, you have to pass 'CategoryCode'.
            if (_machineCategoryRepository.DoesEntityExist(assetCategoriesDto.CategoryCode))
            {
                var result = _machineCategoryRepository.UpdateEntity(new Machinecategory());
                processingResultList.Add(result);
                return processingResultList;
            }

            // Adding data 
            // 1. Add ID for translation table. 
            var translation = _myContext.Translation.Add(new Translation());
            _myContext.SaveChanges();

            // 2. Add String And id, local to translation_string table.
            _myContext.TranslationStrings.Add(new TranslationStrings()
            {
                Strings = assetCategoriesDto.AssetCategory,
                StringsKey = "en_GB",
                TranslationId = translation.Entity.Id
            });

            // 3. Add name_id to machineCategory table too.
            var resultObject = _machineCategoryRepository.AddEntity(new Machinecategory()
            {
                CommissioningPeriod = 14, // default value. avoiding 'null'
                CategoryCode = assetCategoriesDto.CategoryCode,
                NameId = translation.Entity.Id
            });
            _myContext.SaveChanges();

            processingResultList.Add(resultObject);
            return processingResultList;
        }
    }
}
