﻿using System.Collections.Generic;
using System.Text;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class SitesDbService : BaseDatabaseService<SitesDto>, IDatabaseService<SitesDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Store> _storeRepository;
        private readonly BaseRepository<Users> _usersRepository;

        public SitesDbService(IMapper mapper, BaseRepository<Users> usersRepository, BaseRepository<Store> storeRepository)
        {
            _mapper = mapper;
            _usersRepository = usersRepository;
            _storeRepository = storeRepository;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(SitesDto sitesDto)
        {
            var processingResultList = new List<DbProcessingResult>();
            // 1. map SitesDto to StoreDto object.
            var storeDto = MapSitesDtoToStoreDto(sitesDto);
            // 2. map StoreDto to Store model object.
            var mappedStore = _mapper.Map<Store>(storeDto);
            // 3. Try Add or Update.
            var result = _storeRepository.DoesEntityExist(mappedStore)
                ? _storeRepository.UpdateEntity(mappedStore)
                : _storeRepository.AddEntity(mappedStore);

            processingResultList.Add(result);
            return processingResultList;
        }

        // app will crash when id = -1
        private StoreDto MapSitesDtoToStoreDto(SitesDto sitesDto)
        {
            long? id = null;
            if (!string.IsNullOrWhiteSpace(sitesDto.Manager))
            {
                id = _usersRepository.GetIdByString(sitesDto.Manager);
            }

            return new StoreDto()
            {
                Email = sitesDto.SiteEmail,
                PhoneNumber = sitesDto.Telephone,
                McDonaldsStoreId = sitesDto.SiteId,
                Name = sitesDto.Name,
                RequiresServiceRequestApproval = ConvertYesNoToBool(sitesDto.SrApproval),
                AddressType = sitesDto.AddressLine1,
                Country = sitesDto.Country,
                MajorMunicipality = sitesDto.City,
                PostalArea = sitesDto.PostCode,
                StreetName = sitesDto.AddressLine2,
                StoreNumber = sitesDto.SiteNumber,
                OwnerId = id,
                Password = CreateMd5(sitesDto.SiteId),
                RequiresServiceRequestPmApproval = ConvertYesNoToBool(sitesDto.PmApproval)
            };
        }

        private bool ConvertYesNoToBool(string yesNo)
        {
            return yesNo.ToLower().Equals("yes");
        }

        public static string CreateMd5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
