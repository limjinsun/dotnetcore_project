﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class ServiceProvidersDbService : BaseDatabaseService<ServiceProvidersDto>, IDatabaseService<ServiceProvidersDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Serviceprovider> _serviceProviderRepository;
          
        public ServiceProvidersDbService(IMapper mapper, BaseRepository<Serviceprovider> serviceProviderRepository)
        {
            _mapper = mapper;
            _serviceProviderRepository = serviceProviderRepository;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(ServiceProvidersDto serviceProvidersDto)
        {
            var processingResult = new List<DbProcessingResult>();
            var mappedServiceProvider = _mapper.Map<Serviceprovider>(serviceProvidersDto);
            var result = _serviceProviderRepository.DoesEntityExist(mappedServiceProvider) ? _serviceProviderRepository.UpdateEntity(mappedServiceProvider) : _serviceProviderRepository.AddEntity(mappedServiceProvider);

            processingResult.Add(result);
            return processingResult;
        }
    }
}