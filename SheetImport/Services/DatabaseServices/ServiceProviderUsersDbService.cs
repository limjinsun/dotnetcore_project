﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class ServiceProviderUsersDbService : BaseDatabaseService<ServiceProviderUsersDto>, IDatabaseService<ServiceProviderUsersDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Serviceprovider> _serviceProviderRepository;
        private readonly BaseRepository<Users> _usersRepository;
        private readonly BaseRepository<Authorities> _authoritiesRepository;
        private readonly BaseRepository<Engineer> _engineerRepository;

        public ServiceProviderUsersDbService(IMapper mapper, BaseRepository<Serviceprovider> serviceProviderRepository, BaseRepository<Users> usersRepository, BaseRepository<Authorities> authoritiesRepository, BaseRepository<Engineer> engineerRepository)
        {
            _mapper = mapper;
            _serviceProviderRepository = serviceProviderRepository;
            _usersRepository = usersRepository;
            _authoritiesRepository = authoritiesRepository;
            _engineerRepository = engineerRepository;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var resultList = new List<DbProcessingResult>();

            // insert into users table.
            resultList.Add(InsertIntoUsersTable(serviceProviderUsersDto));
            // insert into authorities table.
            resultList.Add(InsertIntoAuthoritiesTable(serviceProviderUsersDto));
            // insert into engineer table. 
            resultList.Add(InsertIntoEnginnerTable(serviceProviderUsersDto));

            return resultList;
        }

        private DbProcessingResult InsertIntoEnginnerTable(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var engineerDto = MapServiceProviderUsersDtoToEngineerDto(serviceProviderUsersDto);

            var mappedEngineer = _mapper.Map<Engineer>(engineerDto);
            return _engineerRepository.DoesEntityExist(mappedEngineer) ? _engineerRepository.UpdateEntity(mappedEngineer) : _engineerRepository.AddEntity(mappedEngineer);
        }

        private EngineerDto MapServiceProviderUsersDtoToEngineerDto(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var serviceProviderId = _serviceProviderRepository.GetIdByString(serviceProviderUsersDto.Company);
            var userId = _usersRepository.GetIdByString(serviceProviderUsersDto.UserName);

            return new EngineerDto()
            {
                ServiceProviderId = serviceProviderId,
                UserId = userId
            };
        }

        private DbProcessingResult InsertIntoAuthoritiesTable(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var authoritiesDto = MapServiceProviderUsersDtoToAuthoritiesDto(serviceProviderUsersDto);

            var mappedAuthorities = _mapper.Map<Authorities>(authoritiesDto);
            return _authoritiesRepository.DoesEntityExist(mappedAuthorities) ? _authoritiesRepository.UpdateEntity(mappedAuthorities) : _authoritiesRepository.AddEntity(mappedAuthorities);
        }

        private DbProcessingResult InsertIntoUsersTable(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var usersDto = MapServiceProviderUsersDtoToUsersDto(serviceProviderUsersDto);

            var mappedUsers = _mapper.Map<Users>(usersDto);
            return _usersRepository.DoesEntityExist(mappedUsers) ? _usersRepository.UpdateEntity(mappedUsers) : _usersRepository.AddEntity(mappedUsers);
        }

        private UsersDto MapServiceProviderUsersDtoToUsersDto(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var providerId = _serviceProviderRepository.GetIdByString(serviceProviderUsersDto.Company);
            return new UsersDto()
            {
                Email = serviceProviderUsersDto.Email,
                MobileNumber = serviceProviderUsersDto.Mobile,
                Username = serviceProviderUsersDto.UserName,
                ProviderId = providerId,
                FirstName = serviceProviderUsersDto.FirstName,
                LastName = serviceProviderUsersDto.LastName
            };
        }

        private AuthoritiesDto MapServiceProviderUsersDtoToAuthoritiesDto(ServiceProviderUsersDto serviceProviderUsersDto)
        {
            var userId = _usersRepository.GetIdByString(serviceProviderUsersDto.UserName);
            return new AuthoritiesDto()
            {
                Authority = serviceProviderUsersDto.Role.ToUpper(),
                UserId = userId
            };
        }
    }
}