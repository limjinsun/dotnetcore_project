﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class LocationDbService : BaseDatabaseService<LocationDto>, IDatabaseService<LocationDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Station> _stationRepository;
        private readonly DataHubContext _myContext;

        public LocationDbService(IMapper mapper, BaseRepository<Station> stationRepository, DataHubContext myContext)
        {
            _mapper = mapper;
            _stationRepository = stationRepository;
            _myContext = myContext;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(LocationDto locationDto)
        {
            var processingResultList = new List<DbProcessingResult>();
            // in order to get unique item in station table, you have to pass string. 
            if (_stationRepository.DoesEntityExist(locationDto.Location))
            {
                var result = _stationRepository.UpdateEntity(new Station());
                processingResultList.Add(result);
                return processingResultList;
            }

            // Adding data 
            // 1. Add ID for translation table. 
            var translation = _myContext.Translation.Add(new Translation());
            _myContext.SaveChanges();

            // 2. Add String And id, local to translation_string table.
            _myContext.TranslationStrings.Add(new TranslationStrings()
            {
                Strings = locationDto.Location,
                StringsKey = "en_GB",
                TranslationId = translation.Entity.Id
            });

            // 3. Add name_id to station table.
            var resultObject = _stationRepository.AddEntity(new Station()
            {
                NameId = translation.Entity.Id
            });
            _myContext.SaveChanges();

            processingResultList.Add(resultObject);
            return processingResultList;
        }
    }
}