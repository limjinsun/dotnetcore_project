﻿using System.Collections.Generic;
using AutoMapper;
using DataHubSheetImport.Dtos;
using DataHubSheetImport.Interfaces;
using DataHubSheetImport.Models;
using DataHubSheetImport.Repositories;

namespace DataHubSheetImport.Services.DatabaseServices
{
    public class OemDbService : BaseDatabaseService<OemDto>, IDatabaseService<OemDto>
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<Oemdetails> _oemDetailsRepository;
          
        public OemDbService(IMapper mapper, BaseRepository<Oemdetails> oemDetailsRepository)
        {
            _mapper = mapper;
            _oemDetailsRepository = oemDetailsRepository;
        }

        public override List<DbProcessingResult> InsertDtoToDbTable(OemDto oemDto)
        {
            var processingResultList = new List<DbProcessingResult>();

            var mappedOemdetail = _mapper.Map<Oemdetails>(oemDto);
            var result = _oemDetailsRepository.DoesEntityExist(mappedOemdetail) ? _oemDetailsRepository.UpdateEntity(mappedOemdetail) : _oemDetailsRepository.AddEntity(mappedOemdetail);

            processingResultList.Add(result);
            return processingResultList;
        }

        private long GetIdByName(string name)
        {
            return _oemDetailsRepository.GetIdByString(name);
        }
    }
}
